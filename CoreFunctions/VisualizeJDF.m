function [UsedDesigns]=VisualizeJDF(FileName,Shift,ShowCrosses,Design_GDS)
    [Job,ShotDesignTranslate,GlobalCrosses]=ReadJDF(FileName);
    %[ShotDesignTranslateS,Type]=StripFromType(ShotDesignTranslate);
    %ShotDesignTranslateFull=TranslateList(ShotDesignTranslateS,ShortDesignNames,DesignsNames);
    UsedDesigns=[];
    if(ShowCrosses)
        names=fieldnames(GlobalCrosses.GLMPOS);
        for i=1:numel(names)
           plot(GlobalCrosses.GLMPOS.(names{i})(1),GlobalCrosses.GLMPOS.(names{i})(2),'ok');hold on;
        end
    end
    for i=1:numel(Job)
        Layers=1:30;
        if(isequal(ShotDesignTranslate{i}(end),'C'));Layers=[2,3];end
        if(isequal(ShotDesignTranslate{i}(end),'G'));Layers=11;end
        for j=1:numel(Job(i).ARRAY)
            if(Job(i).ASSIGN(j)~=0)
            PlotGDS(ShiftGDS(FilterGDS(Design_GDS{Job(i).ASSIGN(j)},Layers),real(Job(i).ARRAY(j))+Shift(1),imag(Job(i).ARRAY(j))+Shift(2)));
            UsedDesigns=[UsedDesigns,Job(i).ASSIGN(j)];
            hold on;
            end
            if(ShowCrosses)
                names=fieldnames(Job(i).CHMPOS);
                for k=1:numel(names)
                    plot(real(Job(i).ARRAY(j))+Job(i).CHMPOS.(names{k})(1)+Shift(1),imag(Job(i).ARRAY(j))+Job(i).CHMPOS.(names{k})(2)+Shift(2),'*r');
                end
            end
        end
    end
    hold off;
end
function out=GetEffectiveDose(Freq,Current,Grid)
    out=1e-4/Grid.^2/Freq*Current*1e6;
end
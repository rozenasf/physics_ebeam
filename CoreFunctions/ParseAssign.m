function out=ParseAssign(in,Array)
    idx=findstr(in,'ASSIGN');
    in=in(idx+6:end);
    IDXI=findstr(in,'(');
    IDXF=findstr(in,')');
    out=[];
    DesignNumber=str2num(in(IDXI(1)+1:IDXF(1)-1));
    for i=3:numel(IDXI)
        SubText=in(IDXI(i)+1:IDXF(i-1)-1);
        CommaPosition=findstr(SubText,',');
        Xtext=SubText(1:CommaPosition-1);Ytext=SubText(CommaPosition+1:end);
        X=EbeamSyntexToVector(Xtext,size(Array,1));Y=EbeamSyntexToVector(Ytext,size(Array,2));
        [XX,YY]=meshgrid(X,Y);
        for j=1:numel(XX)
            out=[out;XX(j),YY(j),DesignNumber];
        end
    end
end
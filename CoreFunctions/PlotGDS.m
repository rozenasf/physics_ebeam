function PlotGDS(ForGDS)
    ActiveLayers=unique([ForGDS.layer]);
    Colors=GenerateColors(64);
    
    for i=1:numel(ForGDS)
        hold on;
        fill(ForGDS(i).points(1,:),ForGDS(i).points(2,:),Colors((ForGDS(i).layer==ActiveLayers),:));
    end
    hold off
end
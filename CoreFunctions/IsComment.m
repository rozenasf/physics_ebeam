function out=IsComment(in)
    out=0;
    IDX=findstr(in,';');
    if(numel(IDX)==0);return;end
    if(prod(ismember(in(1:IDX),' ;')));out=1;end
end
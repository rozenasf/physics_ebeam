function delta=GetShiftBetweenSquers(a,b)
    sq1=[min(a'),max(a')];
    sq2=[min(b'),max(b')];
    delta=sq1-sq2;
    if(sqrt((delta(1)-delta(3)).^2+(delta(2)-delta(4)).^2)>1)
       disp('couldnt match squers') 
    end
        delta=(delta(1:2)+delta(3:4))/2;
end
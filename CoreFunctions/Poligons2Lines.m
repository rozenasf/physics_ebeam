function out=Poligons2Lines(Design,varargin)
if(numel(varargin)==0);FilterLayers=[1:300]; else FilterLayers=varargin{1};end
out=[];
    for i=1:numel(Design)
        if(~ismember(Design(i).layer,FilterLayers));continue;end
        for j=1:(size(Design(i).points,2)-1)
            out=[out;Design(i).points(1,j),Design(i).points(2,j),Design(i).points(1,j+1),Design(i).points(2,j+1),Design(i).layer];
        end
    end
end
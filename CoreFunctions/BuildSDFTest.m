function Shifts=BuildSDFTest(Type,ProjectMark,DoseArray,DeltaX,SizeY)
    Input=[DeltaX*(-1).^(1:numel(DoseArray));round(linspace(-SizeY/2,SizeY/2,numel(DoseArray)));DoseArray];
    Input(1,1)=Input(1,1)*2;
    Input(1,end)=Input(1,end)*2;
    SDF_Text_Maker( Input', ['TEST_',Type,'_',ProjectMark], ['TEST_',Type,'_',ProjectMark] );
    Shifts=Input(1:2,:);
%     Layer=2;
%     hold off
%     for j=1:size(Shifts,2)
%         for i=1:numel(Design_GDS)
%            PlotGDS(ShiftGDS(FilterGDS(Design_GDS{i},Layer),Shifts(1,j),Shifts(2,j))); hold on
%         end
%     end
%     hold off
end
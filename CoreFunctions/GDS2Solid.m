function GDS2Solid(ForGDS,NameStruct)
%Work with Macro_draw_line.swp script to load
%Example: GDS2Solid('Standard_Regular.gds',struct('Contacts',2,'Gates',11,'Optical',39,'Etch',40,'Sketch6',41));
%Then run Macro_draw_line.swp in solidworks with the relevat layers to
%write. 
%IMPORTANT! in order for the script to work properly, go to tools->options->Sketch->Relations/Snaps
%keep only "enable snipping" and "Grid".  press "go to document grid setting and set the "major grid setting" to 0.001

    if(~isstruct(ForGDS))
       ForGDS=gds_load (ForGDS);
       ForGDS=[ForGDS.structures.elements.element];
    end
    %can get file name or ForGDS struct array
    LayersNames{300}=[];
    names=fieldnames(NameStruct);
    for i=1:numel(names)
        LayersNames{NameStruct.(names{i})}=names{i};
    end

    out=dir('ForSolid');if(numel(out)==0);mkdir('ForSolid');end;delete('ForSolid/*.GDSSLD');
        Layers=unique([ForGDS.layer]);
        for i=1:numel(Layers)
            if(isempty(LayersNames{Layers(i)})); disp(['no name for layer ',num2str(Layers(i))]);continue;end
            
            PointsList=[];
           for j=1:numel(ForGDS)
              if( ForGDS(j).layer~=Layers(i));continue;end
              points=ForGDS(j).points;
              
              for k=1:(size(points,2)-1)
                PointsList=[PointsList;points(1,k),points(2,k),points(1,k+1),points(2,k+1)];
              end
           end
              IDX=[];
              for k1=1:size(PointsList,1)
                  OK=1;
                  Val1=[PointsList(k1,1),PointsList(k1,2),PointsList(k1,3),PointsList(k1,4)];
                  for k2=1:size(PointsList,1)
                      if(k2==k1);continue;end
                      Val2=[PointsList(k2,1),PointsList(k2,2),PointsList(k2,3),PointsList(k2,4)];
                      Val3=[PointsList(k2,3),PointsList(k2,4),PointsList(k2,1),PointsList(k2,2)];
                      if(sum(abs(Val1-Val2))<1 || sum(abs(Val1-Val3))<1);OK=0;disp('removed');break;end
                  end
                  if(OK);IDX=[IDX,k1];end
              end
              PointsList=PointsList(IDX,:);
              temp=[];
              for k=1:(size(PointsList,1))
                temp=[temp,sprintf('%f,%f,%f,%f;',PointsList(k,1),PointsList(k,2),PointsList(k,3),PointsList(k,4))];
              end
              f=fopen(['ForSolid/',LayersNames{Layers(i)},'.GDSSLD'],'w');
           fprintf(f,temp);
           fclose(f);
           disp(['done layer ',num2str(Layers(i))]);
           
           
        end
end

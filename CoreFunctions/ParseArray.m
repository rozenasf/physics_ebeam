function out=ParseArray(Array)
    I=sqrt(-1);
    idx=findstr(Array,'ARRAY');
    Array=Array(idx+6:end);
    IDXI=findstr(Array,'(');
    IDXF=findstr(Array,')');
    ArrayX=str2num(Array(IDXI(1)+1:IDXF(1)-1));
    ArrayY=str2num(Array(IDXI(2)+1:IDXF(2)-1));
    ArrayX=ArrayX(1)+(0:(ArrayX(2)-1))*ArrayX(3);
    ArrayY=ArrayY(1)-(0:(ArrayY(2)-1))*ArrayY(3);
    [xx,yy]=meshgrid(ArrayX,ArrayY);
    out=xx+I*yy;
end
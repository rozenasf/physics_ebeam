function PlotGeometry(Geometry,Markers,WriteField)
    EbeamTypes=fieldnames(Geometry);
    colors='rgb';
    figure(1);
    for i=1:numel(EbeamTypes)
        Data=Geometry.(EbeamTypes{i});
        for j=1:numel(Data)
            h=fill(real(Data{j}),imag(Data{j}),colors(mod(i-1,3)+1));
            set(h,'facealpha',.5);set(h,'EdgeColor','None');
            hold on
        end
    end
    for i=1:size(Markers,1)
        PlotBox(Markers(i,:),'m')
    end
    PlotBox(WriteField,'k')
    hold off
end
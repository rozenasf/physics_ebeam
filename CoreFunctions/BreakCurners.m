function out=BreakCurners(in,Delta)

    out=zeros(size(in,1),(size(in,2)-2)*2+2);
    for j=1:size(in,1)
        out(j,1)=in(j,1);
        for i=1:(size(in,2)-2)
            out(j,2*i)=in(j,i+1)-(in(j,i+1)-in(j,i))*Delta/abs((in(j,i+1)-in(j,i)));
            out(j,2*i+1)=in(j,i+1)-(in(j,i+1)-in(j,i+2))*Delta/abs((in(j,i+1)-in(j,i+2)));
        end
        out(j,2*i+2)=in(j,end);
    end
    
end
function [out,Type]=StripFromType(in)
%works by the last one, assuming all have the same type
    for i=1:numel(in)
       out{i}=in{i}(1:end-1);
       Type=in{i}(end);
    end
end
function [Time]=GetWritingTimeGDS(ForGDS,Dose,Current,layers)
    EffectiveDose=GetEffectiveDose(10e6,2e-9,4e-9); %time it takes to get to a new point
    Sum=0;
    if(exist('layers'));ForGDS=FilterGDS(ForGDS,layers);end
    for i=1:numel(ForGDS)
       Sum=Sum+polyarea(ForGDS(i).points(1,:),ForGDS(i).points(2,:)) ;
    end
    
    Time=(Sum/1e12)*((Dose+EffectiveDose)/1e6*1e4)/(Current*1e-9);
end
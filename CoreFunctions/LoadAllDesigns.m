function [Design_GDS_Raw,Design_GDS]=LoadAllDesigns(Designs)
Design_GDS_Raw={};Design_GDS={};
for i=1:numel(Designs);Design_GDS_Raw{i}=gds_load([Designs{i},'.gds']);
    Design_GDS{i}=ExtractLayers(Design_GDS_Raw{i},'Die',1:300);
end
end
function ForGDS=ExtractLayers(GDS,Cell,Layers,varargin)
% This function will load from GDS file from specific cell all Layers. the
% extra argument is filter to remove large polygons.
% example: ForGDS=LoadLayer(loaded GDS,'full_wafer',39,100);

if(numel(varargin)==1);filter=varargin{1};else filter=1e8;end
    out=GDS;
    out2=gds_flat_structure(out.structures,Cell);
    %disp('Flattened cell')
    ForGDS=[];
    for i=1:numel(out2.plgs)
        if(ismember(out2.plgs(i).layer,Layers))
            if(size(out2.plgs(i).points,2)<filter)
                ForGDS(end+1).points=(out2.plgs(i).points)/1000;
                ForGDS(end).dataType=0;
                ForGDS(end).layer=out2.plgs(i).layer;
            end
        end
    end
    %disp('Done')
end
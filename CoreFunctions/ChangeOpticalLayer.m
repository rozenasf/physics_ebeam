function ChangeOpticalLayer(InputName,OutpuName,Template)
OriginOpticalLayer=39;
NewOpticalLayer=[117,119];
OriginalACrossLayer=91;
NewACrossLayer=202;

%% Test change optical layer
%InputName='Design_5_short_C.gds';
%OutpuName='Design_5_short_C.gds';


ForGDSRaw=gds_load([InputName,'.gds']);
Full=ExtractLayers(ForGDSRaw,'Die',1:300);
Across_Design=ExtractLayers(ForGDSRaw,'Die',OriginalACrossLayer);Across_Design=mean([max(Across_Design(1).points');min(Across_Design(1).points')]);
%% remove layers
LayerToRemove=OriginOpticalLayer;
Full2=Full;
IndexToRemove=[];
for i=1:numel(Full)
    if(Full(i).layer~=LayerToRemove)
       IndexToRemove=[IndexToRemove,i];
    end
end
Full2=Full(IndexToRemove);
%%
Across_Template=ExtractLayers(Template,'DIE',NewACrossLayer);Across_Template=mean([max(Across_Template(1).points');min(Across_Template(1).points')]);
ToAdd=ExtractLayers(Template,'DIE',NewOpticalLayer);
for i=1:numel(ToAdd)
   ToAdd(i).layer=LayerToRemove;
   ToAdd(i).points(1,:)=ToAdd(i).points(1,:)-Across_Template(1)+Across_Design(1);
   ToAdd(i).points(2,:)=ToAdd(i).points(2,:)-Across_Template(2)+Across_Design(2);
end
%%
Full2=[Full2,ToAdd];
%%
gdsFile = gds_create_library(OutpuName);
gds_start_structure(gdsFile, 'Die'); %cell will be called "Die"
gds_write_boundaries(gdsFile, Full2);
gds_close_structure(gdsFile);
gds_close_library(gdsFile);
end
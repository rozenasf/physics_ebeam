function out=GiveWidth(in,WidthVec)
out=[];I=sqrt(-1);
for i=1:numel(in)
    out(1,i)=[IntersectWithAngle(in(i)+WidthVec(i,1),imag(in(i)),WidthVec(i,2))];
    out(2,i)=[IntersectWithAngle(in(i)-WidthVec(i,1),imag(in(i)),WidthVec(i,2))];
end
end
function out=ParseShotDesign(in)
    out=[];
    out.DesignNumber=0;
    out.DesignName='';
    IDXI1=findstr(in,'(');
    IDXF1=findstr(in,')');
    IDX2=findstr(in,char(39));
    out.DesignNumber=str2num(in(IDXI1(1)+1:IDXF1(1)-1));
    out.DesignName=in(IDX2(1)+1:IDX2(2)-1);
end
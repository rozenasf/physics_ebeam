function CantileverList=GenerateCantileverList(DesignList)
    a=struct('Position',{},'Design',{});
    Letters='ABCDEFGHI';
    ToSkip={'A1','A2','B1','A9','A10','B10','H1','I1','I2','I3','H10','I8','I9','I10'};
    k=1;
    for i=1:9
        for j=1:10
            if(sum(ismember(ToSkip(:),[Letters(i),num2str(j)])));a(i,j).Design=0;continue;end
            a(i,j).Position=[Letters(i),num2str(j)];
            a(i,j).Design=DesignList(k,3);k=k+1;
        end
    end
    CantileverList=a;
    f=fopen('CantileverList.txt','wt');
    for i=unique([CantileverList.Design])
        if(i==0);continue;end
        fprintf(f,'%d:',i);
        I=find([CantileverList.Design]==i);
        ToPrint=sort({CantileverList(I(:)).Position});
        for j=1:numel(ToPrint)
            fprintf(f,'%s',ToPrint{j});
            if(j<numel(I));fprintf(f,',');else fprintf(f,'\n');end
        end
    end
    fclose(f);
end
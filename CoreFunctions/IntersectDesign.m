function out=IntersectDesign(ForGDS,Limits,Layers)
if(iscell(ForGDS))
    for i=1:numel(ForGDS)
        out{i}=IntersectDesign(ForGDS{i},Limits,Layers);
    end
else
    %Limits are X1,Y1,X2,Y2
    out=struct('points',{},'layer',{},'dataType',{});k=1;
    for i=1:numel(ForGDS)
       if(ismember(ForGDS(i).layer,Layers))
           
            [x,y]=polybool('intersection', ForGDS(i).points(1,:),ForGDS(i).points(2,:),[Limits(1),Limits(3),Limits(3),Limits(1),Limits(1)], [Limits(2),Limits(2),Limits(4),Limits(4),Limits(2)]);
            if(~isempty(x))
                out(k).points=[x;y];
                out(k).layer=ForGDS(i).layer;
                out(k).dataType=ForGDS(i).dataType;
                k=k+1;
            end
       else
          out(k)=ForGDS(i); 
          k=k+1;
       end
    end
end
end
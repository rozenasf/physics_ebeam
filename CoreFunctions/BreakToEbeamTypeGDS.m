function BreakToEbeamTypeGDS(filename)
    WritingTypes={'C','G'};
    WritingFieldLayer=[101,102];
    Design_GDS_Raw=gds_load(filename);Design_GDS=ExtractLayers(Design_GDS_Raw,'Die',1:300);
    for i=1:numel(WritingFieldLayer)
        WritingField=ExtractLayers(Design_GDS_Raw,'Die',WritingFieldLayer(i));
        points=WritingField.points;
        X=mean([min(min(points(1,:))),max(max(points(1,:)))]);
        Y=mean([min(min(points(2,:))),max(max(points(2,:)))]);
        
        
        IDX101=find([Design_GDS.layer]==101);
        IDXOther=find([Design_GDS.layer]==WritingFieldLayer(i));
        
        Design_GDS(IDX101).layer=110;
        Design_GDS(IDXOther).layer=101;
        
        gdsFile = gds_create_library([filename(1:end-4),'_',WritingTypes{i}]);
        gds_start_structure(gdsFile, 'Die'); %cell will be called "Die"
        gds_write_boundaries(gdsFile, ShiftGDS(Design_GDS,-X,-Y));
        gds_close_structure(gdsFile);
        gds_close_library(gdsFile);
    end
end
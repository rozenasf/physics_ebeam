function WriteField=InterpretWriteField(Lines,UseEbeam)
    areas=[1,1;-1,-1];
    WriteField=[];
    WriteField=[max(max(Lines.(UseEbeam)(:,[1,3]))),max(max(Lines.(UseEbeam)(:,[2,4]))),min(min(Lines.(UseEbeam)(:,[1,3]))),min(min(Lines.(UseEbeam)(:,[2,4])))];
%     for i=1:2
%         I=find((sign(Lines.(UseEbeam)(:,1))==areas(i,1)) .* (sign(Lines.(UseEbeam)(:,2))==areas(i,2) ));
%         [~,IDX]=sort( (Lines.(UseEbeam)(I,1)).^2+(Lines.(UseEbeam)(I,2)).^2);
%         WriteField=[WriteField,Lines.(UseEbeam)(I(IDX(end)),1),Lines.(UseEbeam)(I(IDX(end)),2)];
%     end
end
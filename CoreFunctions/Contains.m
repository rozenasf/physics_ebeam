function out=Contains(in1,in2)
    out=numel(findstr(in1,in2)>0);
end
function PlotGDSOutline(ForGDS)
    ActiveLayers=unique([ForGDS.layer]);
    Colors=GenerateColors(64);
    
    for i=1:numel(ForGDS)
        hold on;
        plot(ForGDS(i).points(1,:),ForGDS(i).points(2,:),'color',Colors((ForGDS(i).layer==ActiveLayers),:));
    end
    hold off
end
function [ForGDS,MidPoints]=GenerateTest(Designs,TestSpacingXY)
DeltaX=TestSpacingXY(1);
XVec=0:DeltaX:(numel(Designs)-1)*DeltaX;XVec=[XVec-2000-(numel(Designs))*DeltaX/2,XVec+2000+(numel(Designs))*DeltaX/2];
XVec=XVec-mean(XVec);
ForGDS=[];
PositionBox=[DeltaX,DeltaX]/2;
for i=1:numel(XVec)
    ForGDS(end+1).points=[XVec(i);0]*ones(1,5)+[-PositionBox(1),PositionBox(1),PositionBox(1),-PositionBox(1),-PositionBox(1);-PositionBox(2),-PositionBox(2),PositionBox(2),PositionBox(2),-PositionBox(2)];
    ForGDS(end).layer=200+mod(i-1,numel(Designs))+1;
    ForGDS(end).dataType=0;
end
MidPoints=[XVec',XVec'*0];
end

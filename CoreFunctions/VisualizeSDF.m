function VisualizeSDF(FileName,Holder,Window,Design_GDS,Layers)
    %FileName='TEST_C_040917.sdf';Holder='1';Window='3D';Design_GDS;
    [Job,MAGAZIN]=ReadSDF(FileName);
    Time=0;
    hold off
    for i=1:numel(Job)
       if(~isequal(Job(i).Holder,Holder));continue;end;
       if(~isequal(Job(i).Window,Window));continue;end;
       UsedDesigns=VisualizeJDF([Job(i).JDF,'.jdf'],Job(i).OFFSET,isequal(Job(i).CHIPAL,'4'),Design_GDS);
       hold on
       for j=1:numel(UsedDesigns)
           Time=Time+GetWritingTimeGDS(Design_GDS{UsedDesigns(j)},Job(i).RESIST,2,Layers);
       end
       
    end
    hold off
    title(['Holder ', Holder,', Window ',Window,', FileName ',FileName,' Time(~X2):',SecondsToTime(Time)],'Interpreter', 'none');
    
end
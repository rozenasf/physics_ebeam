function ForGDS=GeneratePatch(ForGDS,YCutList,Width)
%%
for YCut=YCutList
    Geometry.GDS=ForGDS;
i=1;
NewGDS=[];
delta=Width;extra=0.0;
Poly={};
%YCut=stitchY(i);

%YCut=480;

for j=1:numel(Geometry.GDS)
    [DoesCut,PolyOut1,PolyOut2]=CutPolygonAtY(Geometry.GDS(j).points,YCut+delta/2);
    if(DoesCut);
        [DoesCut2,PolyOut3,PolyOut4]=CutPolygonAtY(PolyOut1,YCut-delta/2);
        if(~DoesCut2)
            [DoesCut2,PolyOut3,PolyOut4]=CutPolygonAtY(PolyOut2,YCut-delta/2);
            Poly{1}=PolyOut1;Poly{2}=PolyOut3;Poly{3}=PolyOut4;
        else
            Poly{1}=PolyOut2;Poly{2}=PolyOut3;Poly{3}=PolyOut4;
        end
    end
    if(~DoesCut)
        NewGDS(end+1).points=Geometry.GDS(j).points;
        NewGDS(end).dataType=Geometry.GDS(j).dataType;
        NewGDS(end).layer=Geometry.GDS(j).layer;
    else
        for k=1:3
            Layer=Geometry.GDS(j).layer;
            if(prod((Poly{k}(2,:)==YCut+delta/2)+(Poly{k}(2,:)==YCut-delta/2)));
                Layer=Layer+1;
                Poly{k}(2,Poly{k}(2,:)==YCut+delta/2)=Poly{k}(2,Poly{k}(2,:)==YCut+delta/2)+extra;
                Poly{k}(2,Poly{k}(2,:)==YCut-delta/2)=Poly{k}(2,Poly{k}(2,:)==YCut-delta/2)-extra;
                Poly{k}(1,Poly{k}(2,:)==YCut+delta/2)=Poly{k}(1,Poly{k}(2,:)==YCut+delta/2)+extra;
                Poly{k}(1,Poly{k}(2,:)==YCut-delta/2)=Poly{k}(1,Poly{k}(2,:)==YCut-delta/2)-extra;
            end
            NewGDS(end+1).points=Poly{k};
            NewGDS(end).dataType=Geometry.GDS(j).dataType;
            NewGDS(end).layer=Layer;
        end
    end
end

ForGDS=NewGDS;


end
end


% %%
% colors=' rg       bm';
% for i=1:numel(NewGDS)
%    fill( NewGDS(i).points(1,:),NewGDS(i).points(2,:),colors(NewGDS(i).layer));
%    hold on
% end
% hold off
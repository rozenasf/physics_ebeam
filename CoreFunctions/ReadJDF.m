function [Job,ShotDesignTranslate,GlobalCrosses]=ReadJDF(FileName)
if(~isequal(FileName(end-2:end),'jdf'));error('Must be a jdf file!');end
%Path='../Main/AsafR43/';
Path=[];
%FileName='TEST_C_040917.jdf';
Path=[Path,FileName];
jdf=fopen(Path,'r');
GlobalCrosses=struct('GLMPOS','','GLMP','','GLMQRS','');
Job=struct('ARRAY',{},'CHMPOS',{},'ASSIGN',{});
ShotDesignTranslate={};
JobIDX=0;
while ~feof(jdf)
    line=fgets(jdf);
    %if(Contains(line,'END'));break;end
    if(IsComment(line));continue;end 
    if(Contains(line,'GLMPOS'));GlobalCrosses.GLMPOS=ParseGLMPOS(line);end
    if(Contains(line,'GLMP'));GlobalCrosses.GLMP=str2num(['[',line(findstr(line,'GLMP')+5:end),']']);end
    if(Contains(line,'GLMQRS'));GlobalCrosses.GLMQRS=str2num(['[',line(findstr(line,'GLMQRS')+6:end),']']);end
    
    if(Contains(line,'ARRAY'));JobIDX=JobIDX+1;Job(JobIDX).ARRAY=ParseArray(line);Job(JobIDX).ASSIGN=Job(JobIDX).ARRAY*0;end
    if(Contains(line,'CHMPOS'));Job(JobIDX).CHMPOS=ParseCHMPOS(line);end
    if(Contains(line,'ASSIGN'));
        Data=ParseAssign(line,Job(JobIDX).ARRAY);
        for k=1:size(Data,1)
            Job(JobIDX).ASSIGN(Data(k,2),Data(k,1))=Data(k,3);
        end
    end
    if(Contains(line,'P(') && ~Contains(line,'ASSIGN')); Data=ParseShotDesign(line);ShotDesignTranslate{Data.DesignNumber}=Data.DesignName(1:end-4);end
        
    %     if(Contains(line,'%'));Job(JobIDX).Window=KeepLettersNumbers(line);end
%     if(Contains(line,'JDF'));idx=findstr(line,char(39));Job(JobIDX).JDF=line(idx(1)+1:idx(2)-1);end
%     if(Contains(line,'GLMDET'));Job(JobIDX).GLMDET=KeepLettersNumbers(line(findstr(line,'GLMDET')+7:end));end
%     if(Contains(line,'CHIPAL'));Job(JobIDX).CHIPAL=KeepLettersNumbers(line(findstr(line,'CHIPAL')+7:end));end
%     if(Contains(line,'RESIST'));Job(JobIDX).RESIST=str2num(line(ismember(line,'0123456789')));end
%     if(Contains(line,'OFFSET'));Job(JobIDX).OFFSET=str2num(['[',line(findstr(line,'(')+1:findstr(line,')')-1),']']);end

end
fclose(jdf);
end
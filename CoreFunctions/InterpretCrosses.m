function Markers=InterpretCrosses(Lines)
    VerticalIndex=find(Lines.Crosses(:,1)==Lines.Crosses(:,3));
    Horizontalndex=find(Lines.Crosses(:,2)==Lines.Crosses(:,4));
    Centers=[];Boxes=[];Corners=[];areas=[1,1;-1,1;1,-1;-1,-1];
    for i=1:numel(VerticalIndex);
        VerticalX=Lines.Crosses(VerticalIndex(i),1);
        VerticalY=[Lines.Crosses(VerticalIndex(i),2),Lines.Crosses(VerticalIndex(i),4)];
        VerticalY=[min(VerticalY),max(VerticalY)];
        for j=1:numel(Horizontalndex)
            if(min(Lines.Crosses(Horizontalndex(j),[1:3]))< VerticalX && max(Lines.Crosses(Horizontalndex(j),[1:3]))>VerticalX)
                if(Lines.Crosses(Horizontalndex(j),2)>VerticalY(1) && Lines.Crosses(Horizontalndex(j),2)<VerticalY(2))
                    Centers=[Centers;[VerticalX,Lines.Crosses(Horizontalndex(j),2)]];
                    break;
                end
            end
        end
    end
    for i=1:4
        I=find((sign(Centers(:,1))==areas(i,1)) .* (sign(Centers(:,2))==areas(i,2) ));
        Corners(:,:,i)=Centers(I,:);
    end
    for i=1:4
        [~,Index]=sort(Corners(:,1,i).^2+Corners(:,2,i).^2);
        Corners(:,:,i)=Corners(Index,:,i);
    end
    for i=1:size(Corners,1)
        Boxes=[Boxes;Corners(i,1,1),Corners(i,2,1),Corners(i,1,4),Corners(i,2,4)];
    end
Markers=Boxes;
    %hold off
end
function Design2Solid(Name,Design)
%This function takes a design in ForGDS format, and generate a solidworks
%script from layers 2,3,11,12. This also copy a template and rename it
copyfile([GetCodeFolderPWD(),'Templates/Template_Cantilever.SLDPRT'],[Name,'.SLDPRT']) 
markers=Poligons2Lines(Design,[2,11]);
save([GetCodeFolderPWD(),'Templates/Points.txt'],'-ascii','markers');
end
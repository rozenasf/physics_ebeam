function out=EbeamSyntexToVector(in,MaxSize)
    if(isequal(in,'*'));out=1:MaxSize;return;end
    IDX=findstr(in,'-');if(numel(IDX)>0);out=str2num(in(1:IDX-1)):str2num(in(IDX+1:end));return;end
    out=str2num(in);
end
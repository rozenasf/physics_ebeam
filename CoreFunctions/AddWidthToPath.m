function out=AddWidthToPath(in,delta)
I=sqrt(-1);
TargetsShort3=in;
Points=[];
Points(1,1)=TargetsShort3(1)+(TargetsShort3(2)-TargetsShort3(1))*I/abs(TargetsShort3(2)-TargetsShort3(1))*delta;
Points(1,2)=TargetsShort3(1)-(TargetsShort3(2)-TargetsShort3(1))*I/abs(TargetsShort3(2)-TargetsShort3(1))*delta;
for i=2:(numel(TargetsShort3)-1)
    Points(i,1)=TargetsShort3(i)+(TargetsShort3(i+1)-TargetsShort3(i-1))*I/abs(TargetsShort3(i+1)-TargetsShort3(i-1))*delta;
    Points(i,2)=TargetsShort3(i)-(TargetsShort3(i+1)-TargetsShort3(i-1))*I/abs(TargetsShort3(i+1)-TargetsShort3(i-1))*delta;
end
Points(numel(TargetsShort3),1)=TargetsShort3(end)+(TargetsShort3(end)-TargetsShort3(end-1))*I/abs(TargetsShort3(end)-TargetsShort3(end-1))*delta;
Points(numel(TargetsShort3),2)=TargetsShort3(end)-(TargetsShort3(end)-TargetsShort3(end-1))*I/abs(TargetsShort3(end)-TargetsShort3(end-1))*delta;
out=Points;
end
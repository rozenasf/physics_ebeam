function out=IntersectWithAngle(in,Y0,angle)
    I=sqrt(-1);
    out=real(in)+I*(-sin(angle/180*pi)*abs(real(in))+Y0);
end
function ForGDS=Solid2GDS(Path,configuration,varargin)
% This function will convert a SLDMAT file to a gds file.
% In that file should be the writing layers + Ebeam + Crosses
% example: Solid2GDS('Standard.SLDMAT','Regular','Contacts',2,'Gates',11)
%% Load File
NoConf=0;
clear Lines;f=fopen([GetCodeFolderPWD(),'Templates/StandardOptical.SLDMAT'],'r');while(~feof(f));eval(fgets(f));end;fclose(f);
Lines_Raw=Lines;Lines=[];Lines.(configuration)=Lines_Raw.Regular;
f=fopen(Path,'r');while(~feof(f));eval(fgets(f));end;fclose(f);
if(numel(fieldnames(Lines))==1);NoConf=1;end
Lines=Lines.(configuration);

EbeamWrite=struct(varargin{:});
if(isfield(EbeamWrite,'Ebeam'));
    UseEbeam=EbeamWrite.Ebeam;
    if(isequal(UseEbeam,'EbeamContacts'))
        FileExtention='C';else FileExtention='G';
    end
    EbeamWrite=rmfield(EbeamWrite,'Ebeam');
    if(NoConf)
        filename=[Path(1:end-7),'_',FileExtention];
    else
        filename=[Path(1:end-7),'_',configuration,'_',FileExtention];
    end
else
    UseEbeam='Ebeam';
    if(NoConf)
        filename=[Path(1:end-7)];
    else
      filename=[Path(1:end-7),'_',configuration];  
    end
end
EbeamTypes=fieldnames(EbeamWrite);
%if(numel(filename)>24);error('Filename is too long, max 24 charectors for Ebeam');end
%% Build geometry
%convert lines to polygons. Each vertex must have only two lines connected
%to it, and all shapes must be closed. Solidworks checks this
Geometry=GeometryFromLines(Lines,EbeamTypes);
%% Get Markers field from Solidwroks
%Interpret the crosses in solidworks to rectangles the ebeam can
%understand. The first set in the most inner one.
Markers=InterpretCrosses(Lines);
%% Get write field from Solidwroks
%Interprete the writing field. Will only check global area, shape is not
%important
WriteField=InterpretWriteField(Lines,UseEbeam);
%WriteField=[Lines.Ebeam(1,1),Lines.Ebeam(1,2),Lines.Ebeam(1,3),Lines.Ebeam(2,4)]
%% plot
PlotGeometry(Geometry,Markers,WriteField)
%% Convert to format that gds can read
ForGDS=[];
for j=1:numel(EbeamTypes);
    for i=1:numel(Geometry.(EbeamTypes{j}))
        ForGDS(end+1).points=[real(Geometry.(EbeamTypes{j}){i});imag(Geometry.(EbeamTypes{j}){i})];
        ForGDS(end).dataType=0;
        ForGDS(end).layer=EbeamWrite.(EbeamTypes{j}); %write the actual geometry
    end
end
for i=1:size(Markers,1)
    ForGDS(end+1).points=[Markers(i,1),Markers(i,3),Markers(i,3),Markers(i,1),Markers(i,1);Markers(i,2),Markers(i,2),Markers(i,4),Markers(i,4),Markers(i,2)];
    ForGDS(end).dataType=0;
    ForGDS(end).layer=91+size(Markers,1)-i;  %write crosses boxes from 91 -> + starting from the most outer one
end

ForGDS(end+1).points=[WriteField(1),WriteField(3),WriteField(3),WriteField(1),WriteField(1);WriteField(2),WriteField(2),WriteField(4),WriteField(4),WriteField(2)];
ForGDS(end).dataType=0;
ForGDS(end).layer=101; %writing field box on layer 101
%% Generate patch
if(0)
if(~ismember(EbeamTypes,'Patch'))
PatchWidth=0.4;
I=find([ForGDS.layer]==101);WF=[min(ForGDS(I).points(1,:)),min(ForGDS(I).points(2,:)),max(ForGDS(I).points(1,:)),max(ForGDS(I).points(2,:))];
if(max(ForGDS(I).points(2,:))-min(ForGDS(I).points(2,:))>500)
    YCutList=min(ForGDS(I).points(2,:)):500:max(ForGDS(I).points(2,:));YCutList=YCutList(2:end-1);
    ForGDS=OverideGDS(ForGDS,GeneratePatch(FilterGDS(ForGDS,[2,3,11,12]),YCutList,PatchWidth));
end
end
end
%% Shift the origin to the center of the writing field:
Shift_x = mean([WriteField(1),WriteField(3)]);
Shift_y = mean([WriteField(2),WriteField(4)]);
%%
gdsFile = gds_create_library(filename);
gds_start_structure(gdsFile, 'Die'); %cell will be called "Die"
%gds_write_boundaries(gdsFile, ForGDS);
gds_write_boundaries(gdsFile, ShiftGDS(ForGDS,-Shift_x,-Shift_y));
gds_close_structure(gdsFile);
gds_close_library(gdsFile);
%%
%GenerateOpticalPath([filename,'.gds'],[filename,'.gds']);
end
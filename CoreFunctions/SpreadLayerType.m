function ForGDS=SpreadLayerType(ForGDS)
    Layers=unique([ForGDS.layer]);
    for i=1:numel(Layers)
       IDX=find([ForGDS.layer]==Layers(i));
       for j=1:numel(IDX)
           ForGDS(IDX(j)).dataType=j-1;
       end
    end
end
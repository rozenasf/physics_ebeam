function [Job,MAGAZIN]=ReadSDF(FileName)
if(~isequal(FileName(end-2:end),'sdf'));error('Must be a sdf file!');end
%Path='../Main/AsafR43/';
Path=[];
%FileName='TEST_C_040917.sdf';
Path=[Path,FileName];
sdf=fopen(Path,'r');
line=fgets(sdf);
idx=findstr(line,char(39));
MAGAZIN=line(idx(1)+1:idx(2)-1);
Job=struct('Holder',{},'Window',{},'JDF',{},'GLMDET',{},'CHIPAL',{},'RESIST',{},'OFFSET',{});
JobIDX=0;
while ~feof(sdf)
    line=fgets(sdf);
    if(Contains(line,'END'));break;end
    if(IsComment(line));continue;end
    
    if(Contains(line,'#'));JobIDX=JobIDX+1;Job(JobIDX).Holder=KeepLettersNumbers(line);end
    if(Contains(line,'%'));Job(JobIDX).Window=KeepLettersNumbers(line);end
    if(Contains(line,'JDF'));idx=findstr(line,char(39));Job(JobIDX).JDF=line(idx(1)+1:idx(2)-1);end
    if(Contains(line,'GLMDET'));Job(JobIDX).GLMDET=KeepLettersNumbers(line(findstr(line,'GLMDET')+7:end));end
    if(Contains(line,'CHIPAL'));Job(JobIDX).CHIPAL=KeepLettersNumbers(line(findstr(line,'CHIPAL')+7:end));end
    if(Contains(line,'RESIST'));Job(JobIDX).RESIST=str2num(line(ismember(line,'0123456789')));end
    if(Contains(line,'OFFSET'));Job(JobIDX).OFFSET=str2num(['[',line(findstr(line,'(')+1:findstr(line,')')-1),']']);end

end
fclose(sdf);
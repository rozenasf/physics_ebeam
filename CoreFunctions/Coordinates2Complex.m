function out=Coordinates2Complex(in)
    I=sqrt(-1);
    if(size(in,1)==2)
        out=in(1,:)+I*in(2,:);
    else
        out=in(:,1)+I*in(:,2);
    end
end
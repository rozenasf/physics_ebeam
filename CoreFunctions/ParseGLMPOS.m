function out=ParseGLMPOS(in)
    IDX=findstr(in,'=');
    IDXI=findstr(in,'(');
    IDXF=findstr(in,')');
    out=[];
    for i=1:numel(IDX)
        out.(in(IDX(i)-1))=str2num(['[',in(IDXI(i)+1:IDXF(i)-1),']']);
    end
end
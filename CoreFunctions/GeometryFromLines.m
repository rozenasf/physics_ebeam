function Geometry=GeometryFromLines(Lines,EbeamTypes)

VariationsShapes=[];
for j=1:numel(EbeamTypes)
    relevan=Lines.(EbeamTypes{j});
    I=sqrt(-1);
    data=[relevan(:,1)+I*relevan(:,2),relevan(:,3)+I*relevan(:,4)];
    Shape={};i=1;
    while(numel(data)>0)
        Shape{i}=[data(1,1),data(1,2)];LastPoint=data(1,1);CurrentPoint=data(1,2);data(1,:)=[];DoneShape=0;
        while(~DoneShape);
            IDX=find(CurrentPoint==data);
            if(numel(IDX)~=1);disp('found more then one or zero, each point should be connected to only 2 lines');end
            if(IDX>size(data,1));
                IDX=IDX-size(data,1);
                CurrentPoint=data(IDX,1);
            else
                CurrentPoint=data(IDX,2);
            end
            data(IDX,:)=[];if(CurrentPoint==LastPoint);DoneShape=1;end;Shape{i}=[Shape{i},CurrentPoint];
        end
        i=i+1;
    end
    VariationsShapes.(EbeamTypes{j})=Shape;
end
Geometry=VariationsShapes;
end
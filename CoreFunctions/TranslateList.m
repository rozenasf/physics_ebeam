function out=TranslateList(in,Ini,Fin)
    for i=1:numel(in)
       IDX=find(strcmp(in{i} ,Ini));
       if(numel(IDX)==1)
           out{i}=Fin{IDX};
       else
          out{i}=in{i}; 
       end
    end
end
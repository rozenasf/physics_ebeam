function String=SecondsToTime(Time)
    t_hms = Time;
    H=floor(t_hms/3600);
    t_hms=t_hms-H*3600;
    M=floor(t_hms/60);
    t_hms=t_hms-M*60;
    S=floor(t_hms);
    String=sprintf('%d:%d:%d',H,M,S);
end
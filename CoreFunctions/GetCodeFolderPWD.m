function Path=GetCodeFolderPWD()
    out=which ('GetCodeFolderPWD');
    IDX=findstr(out,'\');
    Path=out(1:IDX(end-1));
    Path=strrep(Path,'\','/');
end
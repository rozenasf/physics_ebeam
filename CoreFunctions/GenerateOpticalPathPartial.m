function out=GenerateOpticalPathPartial(InputName,OutputName,Template)
%EXAMPLE: first, make a gds file: 
%Solid2GDS('2SideSimpleBracket1Heating.SLDMAT','Large','Contacts',2,'Gates',11,'Optical',40,'Etch',41,'Ebeam','EbeamContacts');
%then just run this script
%%
%Template=gds_load([GetCodeFolderPWD(),'Templates/Full_Template.gds']);
%InputName='2SideSimpleBracket1Heating_Large_EbeamContacts.gds';
%OutputName='2SideSimpleBracket1Heating_Large_EbeamContacts_WithOptical.gds';
%InputName='2SideSimpleBracket1Heating_Large_EbeamContacts.gds';
%OutputName='2SideSimpleBracket1Heating_Large_EbeamContacts_WithOptical.gds';
delta=3; %of the shortening pads
LineWidth=1;
BracketHights=[300,350,480];
BracketHights=[480,510,550];
Spread=[41,80];

%BracketHights=[400,450,480];
deltaPatch=0.4;
LowestCrossYPosition=-1176.94; %keep constant
%%
I=sqrt(-1);
ForGDSRaw=gds_load(InputName);
Full=ExtractLayers(ForGDSRaw,'Die',1:300);
Contacts=ExtractLayers(ForGDSRaw,'Die',2);Contacts={Contacts.points};
ACrossSet=ExtractLayers(ForGDSRaw,'Die',91);
Shift=min(ACrossSet.points(2,:))-LowestCrossYPosition;
%Shift=400;
PatchAt=[-510;990]+Shift;
Lines=[];
for i=1:numel(Contacts)
    for j=1:(size(Contacts{i},2)-1)
        Lines=[Lines;[Contacts{i}(1,j),Contacts{i}(2,j),Contacts{i}(1,j+1),Contacts{i}(2,j+1)]];
    end
end
%%
ContactsLines=[Lines(:,1)+I*Lines(:,2),Lines(:,3)+I*Lines(:,4)];
HighestPoint=max(max(imag(ContactsLines)));
IDX=find(min(imag(ContactsLines)')==HighestPoint);
ContactsLines=ContactsLines(IDX,:);
for i=1:size(ContactsLines,1)
    if(real(ContactsLines(i,1))>real(ContactsLines(i,2)));ContactsLines(i,:)=[ContactsLines(i,2),ContactsLines(i,1)];end
end
[~,IDX]=sort(real(ContactsLines(:,1)));
ContactsLines=ContactsLines(IDX,:);
%%
OpticalTargetsRaw=ExtractLayers(Template,'DIE',39,100);
Etch=ExtractLayers(Template,'DIE',117,100);
%% Get target lines
Window=[-150+I*500,150+I*552];
Targets=[];
Polygons=GDS2Polygons(OpticalTargetsRaw);
for i=1:numel(Polygons)
    for j=1:(size(Polygons{i},2)-1)
        Line=Coordinates2Complex(Polygons{i}(:,[j:j+1]));
        if(InWindow(Line,Window))
            Targets=[Targets;sort(Line)];
        end
    end
end
for i=1:size(Targets,1)
    if(real(Targets(i,1))>real(Targets(i,2)));Targets(i,:)=[Targets(i,2),Targets(i,1)];end
end
[~,IDX]=sort(real(Targets(:,1)));
Targets=Targets(IDX,:);Targets(17,:)=[];
Targets=Targets+I*Shift;
%% Emmiting from contacts
DesignOutline=[];Addition=[];
for i=1:size(ContactsLines,1)
    Addition(i,:)=[mean(ContactsLines(i,:)),IntersectWithAngle(mean(ContactsLines(i,:)),BracketHights(1)+Shift,20)];
end
DesignOutline=[DesignOutline,Addition];
Addition=[];
Xlist1=linspace(-Spread(1),Spread(1),size(DesignOutline,1)+2);Xlist1=Xlist1(2:end-1);
Xlist2=linspace(-Spread(2),Spread(2),size(DesignOutline,1)+2);Xlist2=Xlist2(2:end-1);
for i=1:size(ContactsLines,1)
    Addition(i,:)=[IntersectWithAngle(Xlist1(i),BracketHights(2)+Shift,20),IntersectWithAngle(Xlist2(i),BracketHights(3)+Shift,20)];
end
DesignOutline=[DesignOutline,Addition];

if(mod(size(DesignOutline,1),2)==1)
    ToCut=(size(DesignOutline,1)-1)/2;
    TargetsCut=Targets( (17-ToCut):(17+ToCut),:);
else
    ToCut=(size(DesignOutline,1))/2;
    TargetsCut=Targets( (17-ToCut):(17+ToCut),:);   
    TargetsCut((size(TargetsCut,1)+1)/2,:)=[];
end
Addition=[];
for i=1:size(ContactsLines,1)
    Addition(i,1)=[mean(TargetsCut(i,:))];
end
DesignOutline=[DesignOutline,Addition];

%% Give Width
DesignOutline2=[];
Width=[];
Width.value=ones(size(ContactsLines,1),1)*[0.25,0.25,1,1,2];
Width.value(:,1)=real(ContactsLines(:,2)-ContactsLines(:,1))/2;
Width.value(:,2)=Width.value(:,1);
Width.value(:,3)=abs(TargetsCut(:,1)-TargetsCut(:,2))/4;
Width.value(:,4)=abs(TargetsCut(:,1)-TargetsCut(:,2))/4;
Width.value(:,5)=abs(TargetsCut(:,1)-TargetsCut(:,2))/4;
Width.angle=ones(size(ContactsLines,1),1)*[0,20,20,20,0];

rotate=(angle(TargetsCut(:,1)-TargetsCut(:,2)));
for i=1:size(DesignOutline,1)
    DesignOutline2=[DesignOutline2;GiveWidth(DesignOutline(i,:),[Width.value(i,:);Width.angle(i,:)]' )];
end
%%
DesignOutline2(:,end)=RotateLast(DesignOutline2(:,end),rotate);
%%
N=12;
DesignOutline3=zeros(size(DesignOutline2,1)/2,N+1);

for i=1:size(DesignOutline3,1)
Mid=(DesignOutline2(2*i-1,end)+DesignOutline2(2*i,end))/2;
delta=abs((DesignOutline2(2*i-1,end)-DesignOutline2(2*i,end)))*2;
%Angle=angle(TargetsCut(i,1)-TargetsCut(i,2));
Angle=-30*pi/180;
    for j=1:N
        DesignOutline3(i,j)=Mid+delta*exp(I*j/(N)*2*pi+I*(Angle));
    end
    DesignOutline3(i,N+1)=Mid+delta*exp(I*1/(N)*2*pi+I*(Angle));
end
for j=1:4
    DesignOutline2=BreakCurners(DesignOutline2,10/(2^j));
end
%% make main path polygon
main=[];
for i=1:size(DesignOutline2,1)/2
    Poly=[DesignOutline2(2*i-1,:),fliplr(DesignOutline2(2*i,:)),DesignOutline2(2*i-1,1)];
    [x,y]=polybool('union',real(Poly),imag(Poly),real(DesignOutline3(i,:)),imag(DesignOutline3(i,:)));
    main{i}=x+I*y;
   % main(i,:)=Poly;
   % main(i,:)=DesignOutline3(i,:);
end
%% Break To create Patches 
main2=struct('points',{},'layer',{},'dataType',{});
if(numel(PatchAt)>2)
    k=1;
    for i=1:numel(main)
        for j=1:(numel(PatchAt)-1)
            [x,y]=polybool('intersection', real(main{i})',imag(main{i})',[-250,250,250,-250,-250], [PatchAt(j)+deltaPatch/2,PatchAt(j)+deltaPatch/2,PatchAt(j+1)-deltaPatch/2,PatchAt(j+1)-deltaPatch/2,PatchAt(j)+deltaPatch/2]);
            main2(k).points=[x';y'];
            main2(k).layer=2;
            main2(k).dataType=0;
            k=k+1;
        end
    end
    for i=1:numel(main)
        for j=2:(numel(PatchAt)-1)
            [x,y]=polybool('intersection', real(main{i})',imag(main{i})',[-250,250,250,-250,-250], [PatchAt(j)+deltaPatch/2,PatchAt(j)+deltaPatch/2,PatchAt(j)-deltaPatch/2,PatchAt(j)-deltaPatch/2,PatchAt(j)+deltaPatch/2]);
            main2(k).points=[x';y'];
            main2(k).layer=3;
            main2(k).dataType=0;
            k=k+1;
        end
    end 
else
    k=1;
    for i=1:numel(main)

            
            main2(k).points=[real(main{i});imag(main{i})];
            main2(k).layer=2;
            main2(k).dataType=0;
            k=k+1;

    end
end

%% add sortening pads
TargetsShort=Targets( 2:(16-ToCut),:);
TargetsShort2=transpose(TargetsShort);TargetsShort2=transpose(TargetsShort2(:));
Points=AddWidthToPath(TargetsShort2,delta);
Points=[transpose(Points(:,1)),fliplr(transpose(Points(:,2))),Points(1,1)];
main2(k).points=[real(Points);imag(Points)];
main2(k).layer=2;
main2(k).dataType=0;
k=k+1;

TargetsShort=Targets( (18+ToCut):end-1,:);
TargetsShort2=transpose(TargetsShort);TargetsShort2=transpose(TargetsShort2(:));
Points=AddWidthToPath(TargetsShort2,delta);
Points=[transpose(Points(:,1)),fliplr(transpose(Points(:,2))),Points(1,1)];
main2(k).points=[real(Points);imag(Points)];
main2(k).layer=2;
main2(k).dataType=0;
k=k+1;

%%
%PlotGDS(main2);
%%
gdsFile = gds_create_library(OutputName(1:end-4));
gds_start_structure(gdsFile, 'Die'); %cell will be called "Die"
%gds_write_boundaries(gdsFile, ForGDS);
gds_write_boundaries(gdsFile, [Full,main2]);
gds_close_structure(gdsFile);
gds_close_library(gdsFile);
%%
out=[Full,main2];
end
function out=KeepLettersNumbers(in)
out=in(ismember(in,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'));
end
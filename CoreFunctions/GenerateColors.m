function Colors=GenerateColors(N)
ColorMap=jet(N);
a=[1,N];
while(numel(unique(a))==numel(a))
    Sa=sort(a);
    b=floor((Sa(2:end)+Sa(1:end-1))/2);
    a=[a,b];
end
[~,r]=unique(a);
a=a(sort(r));
Colors=ColorMap(a,:);
end
function DesignList=DistributeDesings(DesignList,RandomDevicePositions)
    [~,IDX]=unique(DesignList(:,1)+sqrt(-1)*DesignList(:,2));
    DesignList(:,2)=-DesignList(:,2);DesignList=fliplr(DesignList);DesignList=sortrows(DesignList(IDX,:));DesignList=fliplr(DesignList);DesignList(:,2)=-DesignList(:,2);

    DesignList(:,3)=RandomDevicePositions;

    CantileverList=GenerateCantileverList(DesignList);
end
function RandomDevicePositions=DistributDesigns(Amount,Weight)
    if(numel(dir('RandomDevicePositions.mat')))
        error('the file RandomDevicePositions.mat exists. you may load it or remove it');
    else
        List=[];
        k=1;
        Number=Weight*0;
        i=1;
        while i<=Amount
            if(Weight(k)~=0)
            if(sum(Number)==0 || Number(k)/sum(Number)<=Weight(k)/sum(Weight))
                List(i,1)=k;
                Number(k)=Number(k)+1;
                i=i+1;
            end
            end
            k=k+1;k=mod(k-1,numel(Weight))+1;
        end

        RandomDevicePositions=List(randperm(numel(List)));
        save('RandomDevicePositions.mat','RandomDevicePositions')
    end
end
function ForGDSTest=BuildFullGDS(WriteFieldType,Designs,Design_GDS,DesignList,Template,WriteOpticalLayer,TestSpacingXY)
PositionBox=[750,750];
gdsFile = gds_create_library(['Full_',WriteFieldType]);       %generate Fulll.gds
for i=1:numel(Designs)                      %create a cell for every design
    gds_start_structure(gdsFile, Designs{i});
    gds_write_boundaries(gdsFile, Design_GDS{i});
    gds_close_structure(gdsFile);
end

gds_start_structure(gdsFile, 'DeviceInformation');
for i=1:numel(Designs)
    gds_write_boundaries(gdsFile, ShiftGDS(str2boundaries ([num2str(i),' ',upper(Designs{i})], 'letterHeight',300),0,i*400));
end
gds_close_structure(gdsFile);

if(WriteOpticalLayer)
    gds_start_structure(gdsFile, 'Optical');
    OpticalLayer=ExtractLayers(Template,'full_wafer',39,100);
    gds_write_boundaries(gdsFile, OpticalLayer);
    gds_close_structure(gdsFile);
end

gds_start_structure(gdsFile, 'Wafer');      %create Wafer cell
if(WriteOpticalLayer);gds_write_sref (gdsFile, 'Optical', 'refPoint', [0;0]) ;end

ForGDS=[];
for i=1:size(DesignList,1)
    I=find([Design_GDS{DesignList(i,3)}.layer]==101);P=Design_GDS{DesignList(i,3)}(I).points;ShiftWritingField=[mean([max(P(1,:)),min(P(1,:))]),mean([max(P(2,:)),min(P(2,:))])];
    ForGDS(end+1).points=([ShiftWritingField']+[DesignList(i,1);DesignList(i,2)])*ones(1,5)+[-PositionBox(1),PositionBox(1),PositionBox(1),-PositionBox(1),-PositionBox(1);-PositionBox(2),-PositionBox(2),PositionBox(2),PositionBox(2),-PositionBox(2)];
    ForGDS(end).dataType=0;
    ForGDS(end).layer=200+DesignList(i,3);
    gds_write_sref (gdsFile, Designs{DesignList(i,3)}, 'refPoint', [DesignList(i,1);DesignList(i,2)]) %position relevante cells in correct positions, every design at its place
end
gds_write_boundaries(gdsFile, ForGDS); %place a rectangular with it center as offset of different position and layers as different designs.

gds_write_sref (gdsFile, 'DeviceInformation', 'refPoint', [max(DesignList(:,1));max(DesignList(:,2))])

gds_close_structure(gdsFile);

%Test part
gds_start_structure(gdsFile, 'Test');

ForGDSTest=[];

    [ForGDS,MidPoints]=GenerateTest(Designs,TestSpacingXY);
    ForGDSTest=[ForGDSTest,ForGDS];
    %gds_write_boundaries(gdsFile, Text);
    gds_write_sref (gdsFile, 'DeviceInformation', 'refPoint', [max(MidPoints(:,1))+300;0])

    for i=1:numel(ForGDS)
        gds_write_sref (gdsFile, Designs{ForGDS(i).layer-200}, 'refPoint', [MidPoints(i,1);MidPoints(i,2)+0])
    end

%gds_write_boundaries(gdsFile, SpreadLayerType(ForGDSTest)); %all on
%datatype 0
gds_write_boundaries(gdsFile, (ForGDSTest));
gds_close_structure(gdsFile);

gds_close_library(gdsFile);
end
function PlotComplex(Targets)
    for i=1:size(Targets,1)
        plot(real( Targets(i,:)),imag(Targets(i,:)))
        hold on
        plot(real( Targets(i,1)),imag(Targets(i,1)),'*')
    end
    hold off
end
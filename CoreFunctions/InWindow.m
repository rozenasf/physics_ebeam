function out=InWindow(in,window)
    %window is [X1+IY1,X2+IY2]
    %in is complex X+IY
    out=1;
    for i=1:numel(in)
        if(~(real(in(i))>=min(real(window)) && real(in(i))<=max(real(window)) && ...
            imag(in(i))>=min(imag(window)) && imag(in(i))<=max(imag(window)) ))
            out=0;
        end
    end
end
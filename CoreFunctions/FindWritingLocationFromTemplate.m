function DesignList=FindWritingLocationFromTemplate(Design_GDS_Raw,Template,RandomDevicePositions)
    WritingLocations=ExtractLayers(Template,'full_wafer',202,100); %crosses A from the template
    WritingLocationsDie=ExtractLayers(Design_GDS_Raw{1},'Die',91,100); %crosses A from a single design
    DesignList=zeros(numel(WritingLocations),3);
    for i=1:size(DesignList,1)
        DesignList(i,1:2)=GetShiftBetweenSquers(WritingLocations(i).points,WritingLocationsDie.points); %fit the crosses in template to the design
    end
    DesignList=DistributeDesings(DesignList,RandomDevicePositions);
end
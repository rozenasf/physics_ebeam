function PlotBox(Vector,color)
    before=ishold;
    plot([Vector(1),Vector(3)],[Vector(2),Vector(2)],color);
    hold on
    plot([Vector(1),Vector(3)],[Vector(4),Vector(4)],color);
    plot([Vector(1),Vector(1)],[Vector(2),Vector(4)],color);
    plot([Vector(3),Vector(3)],[Vector(2),Vector(4)],color);
    if(~before);hold off;end
end
function V30_Ftext_Editor( Strings )

%% Interchangable paths

Directory_Path = '.'; % Path to be used in all the files paths below.
%BeamerPath = 'C:\Program Files\BEAMER\v5.1.4_x86\BEAMER.exe';

%% PLEASE DO NOT CHANGE THE CODE IN THE FOLLOWING SECTIONS. CALL A SEPARATE .M FILE TO CREATE YOUR PARAMETERS.

% all must be in text format!
FileName = Strings.FileName;
Import_center_FileName = Strings.Import_center_FileName;
Extract_CellName = Strings.Extract_CellName;
ExtractLyr_Layer = Strings.ExtractLyr_Layer;
%Heal_TargetLayer = Strings.Heal_TargetLayer;
Grid_DatabaseGrid = Strings.Grid_DatabaseGrid;
PEC_PSFFileName = Strings.PEC_PSFFileName;
Exp_FileName = Strings.Exp_FileName;
FDA_Table = Strings.FDA_Table;
Region_Layer = Strings.RegionLayer;
Full_type=Strings.Full_type;
%% Editing the FText for V30

CD = cd;
% Loading the template file:
fid = fopen([GetCodeFolderPWD(),'Templates/V30_Template.ftxt'],'r');
i = 1;
tline = fgetl(fid);
A{i} = tline;
while ischar(tline)
    i = i+1;
    tline = fgetl(fid);
    if(numel(strfind(tline,'%%%V30Name%%%'))>0);tline=strrep(tline,'%%%V30Name%%%',FileName);end
    if(numel(strfind(tline,'%%%ExpName%%%'))>0);tline=strrep(tline,'%%%ExpName%%%',Exp_FileName);end
    A{i} = tline;
end
fclose(fid);
A = A(:);

% Adding the parameters:
A(1) = {['FLOW ',FileName]};
A(265) = {['FILE_NAME = .%5C',Directory_Path,'%5C',Import_center_FileName,'.gds']};
A(60) = {['CELLNAME = ',Extract_CellName]};
A(37) = {['LAYERSET = ',ExtractLyr_Layer]};
A(38) = {['REGIONLAYER = ',Region_Layer]};
%A(188) = {['TARGET_LAYER = ',Heal_TargetLayer]};
A(20) = {['DATABASE_GRID = ',Grid_DatabaseGrid]};
A(126) = {['PSF_FILENAME = .%5C',Directory_Path,'%5C',PEC_PSFFileName,'.lpsf']};
A(204) = {['FILE_NAME = .%5C',Directory_Path,'%5C',Exp_FileName,'.v30']};
A(169) = {['ASSIGNMENT = ',char(FDA_Table(1,1)),'%20%3A%20',char(FDA_Table(1,2))]};
for t = 2:size(FDA_Table,1)
    A = [A(1:167+t);{['ASSIGNMENT = ',char(FDA_Table(t,1)),'%20%3A%20',char(FDA_Table(t,2))]};A(168+t:end)];
end

% Saving new file:
fid = fopen([FileName,'.ftxt'], 'w');
for i = 1:numel(A)
    if A{i}==-1
        break
    end
    fprintf(fid,'%s\r\n', A{i});
end
fclose(fid);
clearvars fid A;

% Running Beamer:
%system(['"',BeamerPath,'" "',CD,'\',FileName,'.ftxt"']);

end
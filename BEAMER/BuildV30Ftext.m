function BuildV30Ftext(Full_type,Designs,ShortDesignNames)
    %Full_type - C/G
    %istest-0/1
    %Designs
    %ShortDesigns
    %Large Cross 'ABCDEF', Small Cross 'abcdef'

% switch Full_type
%     case 'C'
%         Cross = 'B'; % must be from 'ABCDEF'.
%         InnerCross = 'b'; % must be from 'abcdef'.
%     case 'G'
%         Cross = 'A'; % must be from 'ABCDEF'.
%         InnerCross = 'a'; % must be from 'abcdef'.
% end
%istest = 0; % either 1 for test or 0 for wafer.
clearvars V30_Strings JDF_Strings
Designs=AddDesignsExtention(Designs,Full_type);
% No extensions needed in filenames!
k=1; %for the test
for i=1:length(Designs)
    
%     Generate V30's:
    V30_Strings.FileName = char(Designs(i));
    V30_Strings.Import_center_FileName = ['Full_',Full_type];
    V30_Strings.Extract_CellName = [char(Designs(i))];
    V30_Strings.RegionLayer = '101';
    V30_Strings.Full_type = Full_type;
    
    if strcmp(Full_type,'C')
        V30_Strings.ExtractLyr_Layer = '2%2C3';
        %V30_Strings.Heal_TargetLayer = '2%2C3';
        V30_Strings.PEC_PSFFileName = 'Si_SiO2_150nm_PMMA_500nm_100kV_z125nm';
        V30_Strings.FDA_Table = [{'2'},{'1.0'}; {'3'},{'1.2'}]; 
    elseif strcmp(Full_type,'G')
        V30_Strings.ExtractLyr_Layer = '11';
        %V30_Strings.Heal_TargetLayer = '11';
        V30_Strings.PEC_PSFFileName = 'Si_SiO2_150nm_PMMA_120nm_100kV_z78nm';
        V30_Strings.FDA_Table = [{'11'},{'1.0'}]; 
    end
    V30_Strings.Grid_DatabaseGrid = '0.004000';
    V30_Strings.Exp_FileName = [char(ShortDesignNames(i)),Full_type];
    V30_Ftext_Editor( V30_Strings );
end
end

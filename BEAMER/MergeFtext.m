function MergeFtext(WriteFieldType,DesignsNames)
    Designs=AddDesignsExtention([DesignsNames,'jdf_Test','jdf_Wafer'],WriteFieldType);
    NL=char(13);
Header=['FLOW Full_',WriteFieldType,NL,'ENGINE_VERSION Layout_ENGINE_Revision_Number_5.04.001_,_Jul_26_2017',NL,'PROGRAM_VERSION BEAMER_Revision_Number_5.4.1_(23658),_Jul_26_2017',NL,'LIB_COMMENT',NL,'SHOW_LIB_COMMENT false',NL,' ()',NL];
Footer='ENDFLOW';
MaxID=0;
Shift=0;
XShift=200;

%List2=dir('*.ftxt');List2={List2.name};List={};
%for i=1:numel(List2);if(numel(findstr(List2{i},'Full'))==0);List{end+1}=List2{i};end;end
List=Designs;

Out=fopen(['Full_',WriteFieldType,'.ftxt'],'w');
fprintf(Out,'%s',Header);

for i=1:numel(List)
    if(exist([List{i},'.ftxt']))
    f=fopen([List{i},'.ftxt'],'r');
    Start=0;
    while(~feof(f))
        Line=fgets(f);
        if(numel(findstr(Line,'NODE'))~=0);Start=1;end
        if(numel(findstr(Line,'ENDFLOW'))~=0);break;end
        if(Start)
           if(numel(Line)>2 && isequal(Line(1:3),'ID '));
               CurrentID=str2num(Line(ismember(Line,'0123456789')));
               if(CurrentID>MaxID);MaxID=CurrentID;end;
               IDX1=find(Line=='=');IDX1=IDX1+1;
               Identity=num2str(str2num(Line(IDX1+1:end))+Shift);
               Line=[Line(1:IDX1),Identity,Line(end)];
           end
           if(numel(Line)>7 && isequal(Line(1:8),'POSITION'))
               IDX1=find(Line=='=');IDX1=IDX1+1;
               IDX2=find(Line==',');
               %XPos=num2str(str2num(Line(IDX1+1:IDX2-1))+(i-1)*XShift);
               XPos=num2str((i-1)*XShift);
               Line=[Line(1:IDX1),XPos,Line(IDX2:end)];
           end
           if(numel(Line)>6 && isequal(Line(1:7),'IN_PORT'))
               IDX1=find(Line=='=');IDX1=IDX1+1;
               IDX2=find(Line==',');IDX2=IDX2(1);
               Identity=num2str(str2num(Line(IDX1+1:IDX2-1))+Shift);
               Line=[Line(1:IDX1),Identity,Line(IDX2:end)];
           end
           if(numel(Line)>7 && isequal(Line(1:8),'OUT_PORT'))
               IDX1=find(Line=='=');IDX1=IDX1+1;
               IDX2=find(Line==',');IDX2=IDX2(1);
               Identity=num2str(str2num(Line(IDX1+1:IDX2-1))+Shift);
               Line=[Line(1:IDX1),Identity,Line(IDX2:end)];
           end           
           fprintf(Out,'%s',Line);
           
        end
    end
    Shift=Shift+MaxID;
    fclose(f);
    delete([List{i},'.ftxt']);
    end
end
fprintf(Out,'%s',Footer);
fclose (Out);
end
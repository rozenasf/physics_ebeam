function jdf_CHMPOS_Adder( InputFileName )
OutputFileName=InputFileName;
% 'InputFileName' is the name of the file to be modified.
% 'OutputFileName' is the name under which the final file is saved.

% Loading the template file:
fid = fopen([InputFileName,'.jdf'],'r');
i = 1;
tline = fgetl(fid);
A{i} = tline;
while ischar(tline)
    i = i+1;
    tline = fgetl(fid);
    A{i} = tline;
end
fclose(fid);
A = A(:);

for i = 1:length(A)-3
    if Contains(char(A(i)),'ARRAY') && ~Contains(char(A(i+1)),'CHMPOS')
        A = [A(1:i);...
             {[repmat(' ',1,strfind(char(A(i)),'ARRAY')-1),'CHMPOS M1=(0,0)']};...
             A(i+1:end)];
    end
end

% Saving new file:
fid = fopen([OutputFileName,'.jdf'], 'w');
for i = 1:numel(A)
    if A{i}==-1
        break
    end
    fprintf(fid,'%s\r\n', A{i});
end
fclose(fid);
clearvars fid A;

end

%% Editing the inputs

% all must be in text format!

V30_Strings.FileName = 'V30_Test'; % No extension needed!
V30_Strings.Import_center_FileName = 'TStandardFD'; % No extension needed!
V30_Strings.Extract_CellName = 'TStandardF';
V30_Strings.ExtractLyr_Layer = '2';
V30_Strings.Heal_TargetLayer = '2';
V30_Strings.Grid_DatabaseGrid = '0.004000';
V30_Strings.PEC_PSFFileName = 'Si_SiO2_150nm_PMMA_500nm_100kV_z125nm'; % No extension needed!
V30_Strings.Exp_FileName = 'TStandardFD1'; % No extension needed!
V30_Strings.FDA_Table = [{'2(3)'},{'1.0'}; {'2(3)'},{'1.1'}]; % For each row: first value is layers, second value is relative dose.


JDF_Strings.FileName = 'jdf_Test'; % No extension needed!
JDF_Strings.InDDS_FileName = 'Mask'; % No extension needed!
JDF_Strings.Extract_CellName = 'Mask';
JDF_Strings.OutJobdeck_FileName = 'TStandardFD1'; % No extension needed!
JDF_Strings.OutJobdeck_JobdeckHeader = ['JOB/W  ''NAME'',2\n\n'...
                                        'GLMPOS    P=(0,400),Q=(0,-400)\n'...
                                        'GLMP      1.1,100.0 \n'...
                                        'GLMQRS    1.0,100.0\n'];
JDF_Strings.OutJobdeck_JobdeckFooter = ['     OBJAPT  3                          ; 60um\n'...
                                        '        STDCUR  1.1                        ; 2nA\n'...
                                        '        RESTYP  POSI,''PMMA''\n'..
                                        '        PWIDTH  0.5                        ; 0.50 um\n'];
% OutJobdeck_JobdeckHeader = ['JOB%2FW%20%20%27NAME%27%2C3%0A%0AGLMPOS%20%20%20%20P%3D(0%2C400)%2CQ%3D(0%2C-400)%0AGLMP%20%20%20%20%20%20'...
%                             '1.1%2C100.0%20%0AGLMQRS%20%20%20%201.0%2C100.0%0A'];
% OutJobdeck_JobdeckFooter = ['%20%20%20%20%20OBJAPT%20%203%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20'...
%                             '%3B%2060um%0A%20%20%20%20%20%20%20%20STDCUR%20%201.1%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20'...
%                             '%3B%202nA%0A%20%20%20%20%20%20%20%20RESTYP%20%20POSI%2C%27PMMA%27%0A%20%20%20%20%20%20%20%20PWIDTH%20%20'...
%                             '0.5%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%3B%200.50%20um%0A'];
JDF_Strings.OutJobdeck_Table = [{'201(0)'},{'TStandardFD1'},{'1'}; {'202(0)'},{'TStandardFD1'},{'1'}; {'203(0)'},{'TStandardFD1'},{'1'};...
                                {'204(0)'},{'TStandardFD1'},{'1'}; {'205(0)'},{'TStandardFD1'},{'1'}; {'206(0)'},{'TStandardFD1'},{'1'};...
                                {'207(0)'},{'TStandardFD1'},{'1'}];
                   % For each row: first value is layers, second value is V30 filename, third value is relative dose.

                   
%% Calling the functions

V30_Ftext_Editor( V30_Strings );
jdf_Ftext_Editor( JDF_Strings );

clearvars V30_Strings JDF_Strings;
function SDF_Text_Maker( Input, FileName, JDFName )

% Input must be a [3Xn] matrix for n copies of the device.
% First column is X offset, second column is Y offset, and third column is
% resist dosage.
% 'FileName' is the name under which the final file is saved.
% 'JDFName' is the name of the JDF file.

% Wrinting texts:
A(1) = {'MAGAZIN ''ASTEST'' '};
A(2) = {' '};
for i = 1:size(Input,1)
    A(end+1) = {';********************************** '}; %#ok<*AGROW>
    A(end+1) = {'#1 '};
    A(end+1) = {'%3D '};
    A(end+1) = {['JDF	''',JDFName,''',1 ']};
    A(end+1) = {'ACC 100 '};
    A(end+1) = {'CALPRM ''Weiz_2na'' '};
    A(end+1) = {'DEFMODE 2 '};
    A(end+1) = {'HSWITCH OFF,ON '};
    A(end+1) = {'GLMDET C '};
    A(end+1) = {'CHIPAL V1 '};
    A(end+1) = {'CHMDET A,1,9 '};
    A(end+1) = {'LBC ON '};
    A(end+1) = {['RESIST ',num2str(Input(i,3)),' ']};
    A(end+1) = {'SHOT A,4 '};
    A(end+1) = {['OFFSET (',num2str(Input(i,1)),',',num2str(Input(i,2)),') ']};
    A(end+1) = {';*********************************** '};
end
A(end+1) = {'END'};
A(end+1) = {-1};

% Saving new file:
fid = fopen([FileName,'.sdf'], 'w');
for i = 1:numel(A)
    if A{i}==-1
        break
    end
    fprintf(fid,'%s\r\n', A{i});
end
fclose(fid);
clearvars fid A;

end
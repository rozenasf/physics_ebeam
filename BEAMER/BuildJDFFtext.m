function BuildJDFFtext(Full_type,Designs,ShortDesignNames,ProjectMark,istest,Cross,InnerCross)
    %Full_type - C/G
    %istest-0/1
    %Designs
    %ShortDesigns
    %Large Cross 'ABCDEF', Small Cross 'abcdef'
    
% switch Full_type
%     case 'C'
%         Cross = 'B'; % must be from 'ABCDEF'.
%         InnerCross = 'b'; % must be from 'abcdef'.
%     case 'G'
%         Cross = 'A'; % must be from 'ABCDEF'.
%         InnerCross = 'a'; % must be from 'abcdef'.
% end
%istest = 0; % either 1 for test or 0 for wafer.
clearvars V30_Strings JDF_Strings
Designs=AddDesignsExtention(Designs,Full_type);
% No extensions needed in filenames!
k=1; %for the test

for i=1:length(Designs);
%   Generate JDF:
    if istest
%         for j = 1:length(RelativeDoses)
%             Num = j+(i-1)*length(RelativeDoses);
%             for l=1:numel(find([ForGDSTest.layer]==200+Num))
%                 JDF_Strings.OutJobdeck_Table(k,:) = [{[num2str(200+Num),'(',num2str(l-1),')']},{[char(ShortDesignNames(i))]},{num2str(RelativeDoses(j))}];
%                 k=k+1;
%             end
%         end
        JDF_Strings.OutJobdeck_Table(i,:) = [{[num2str(200+i),'(0)']},{[char(ShortDesignNames(i)),Full_type]},{'1'}];
        
        JDF_Strings.MarkerLayer = '99';
        JDF_Strings.FileName = ['jdf_Test_',Full_type];
        JDF_Strings.Extract_CellName = 'Test';
        JDF_Strings.OutJobdeck_FileName = ['TEST_',Full_type,'_',ProjectMark];
    else
        JDF_Strings.OutJobdeck_Table(i,:) = [{[num2str(200+i),'(0)']},{[char(ShortDesignNames(i)),Full_type]},{'1'}];
        JDF_Strings.MarkerLayer = num2str(double(upper(InnerCross))+26);
        JDF_Strings.FileName = ['jdf_Wafer_',Full_type];
        JDF_Strings.Extract_CellName = 'Wafer';
        JDF_Strings.OutJobdeck_FileName = ['WAFER_',Full_type,'_',ProjectMark];
    end
end
JDF_Strings.InDDS_FileName = ['Full_',Full_type];


switch Cross % Outer crosses positions:
    case 'A'
        P = [-20000,2950];
        Q = [20000,2950];
    case 'B'
        P = [-20000,1750];
        Q = [20000,1750];
    case 'C'
        P = [-20000,550];
        Q = [20000,550];
    case 'D'
        P = [-20000,-650];
        Q = [20000,-650];
    case 'E'
        P = [-20000,-1850];
        Q = [20000,-1850];
    case 'F'
        P = [-20000,-3050];
        Q = [20000,-3050];
end

JDF_Strings.OutJobdeck_JobdeckHeader = ['JOB/W  ''NAME'',',num2str(istest+1),'\n\n'...
                                        'GLMPOS    P=(',num2str(P(1)),',',num2str(P(2)),')'...
                                        ',Q=(',num2str(Q(1)),',',num2str(Q(2)),')\n'...
                                        'GLMP      4,900.0 \n'...
                                        'GLMQRS    4,900.0\n'];
JDF_Strings.OutJobdeck_JobdeckFooter = ['     OBJAPT  3                          ; 60um\n'...
                                        '        STDCUR  1.1                        ; 2nA\n'...
                                        '        RESTYP  POSI,''PMMA''\n'...
                                        '        PWIDTH  0.5                        ; 0.50 um\n'];

jdf_Ftext_Editor( JDF_Strings );
end

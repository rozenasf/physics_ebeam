function  BuildSDFWafer(Type,ProjectMark,Dose)
A(1) = {'MAGAZIN ''ASTEST'' '};
A(2) = {' '};

    A(end+1) = {';********************************** '}; %#ok<*AGROW>
    A(end+1) = {'#1 '};
    A(end+1) = {'%2A '};
    A(end+1) = {['JDF	''',['WAFER_',Type,'_',ProjectMark],''',1 ']};
    A(end+1) = {'ACC 100 '};
    A(end+1) = {'CALPRM ''Weiz_2na'' '};
    A(end+1) = {'DEFMODE 2 '};
    A(end+1) = {'HSWITCH OFF,ON '};
    A(end+1) = {'GLMDET S '};
    A(end+1) = {'CHIPAL 4 '};
    A(end+1) = {'CHMDET A,1,9 '};
    A(end+1) = {'LBC ON '};
    A(end+1) = {['RESIST ',num2str(Dose),' ']};
    A(end+1) = {'SHOT A,4 '};
    A(end+1) = {['OFFSET (',num2str(332),',',num2str(-3226),') ']};
    A(end+1) = {';*********************************** '};

A(end+1) = {'END'};
A(end+1) = {-1};

% Saving new file:
fid = fopen([['WAFER_',Type,'_',ProjectMark],'.sdf'], 'w');
for i = 1:numel(A)
    if A{i}==-1
        break
    end
    fprintf(fid,'%s\r\n', A{i});
end
fclose(fid);
clearvars fid A;
end

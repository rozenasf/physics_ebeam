function jdf_Ftext_Editor( Strings )

%% Interchangable paths

Directory_Path = '.'; % Path to be used in all the files paths below.
BeamerPath = 'C:\Program Files\BEAMER\v5.1.4_x86\BEAMER.exe';

%% PLEASE DO NOT CHANGE THE CODE IN THE FOLLOWING SECTIONS. CALL A SEPARATE .M FILE TO CREATE YOUR PARAMETERS.

% all must be in text format!
FileName = Strings.FileName;
InDDS_FileName = Strings.InDDS_FileName;
Extract_CellName = Strings.Extract_CellName;
OutJobdeck_FileName = Strings.OutJobdeck_FileName;
OutJobdeck_JobdeckHeader = Strings.OutJobdeck_JobdeckHeader;
OutJobdeck_JobdeckFooter = Strings.OutJobdeck_JobdeckFooter;
OutJobdeck_Table = Strings.OutJobdeck_Table;

%% Editing the FText for jdf

CD = cd;

TextChanges = [{' '},{'%20'}; {'/'},{'%2F'}; {''''},{'%27'}; {'\n'},{'%0A'};...
               {'='},{'%3D'}; {','},{'%2C'}; {';'},{'%3B'}];
for t = 1:size(TextChanges,1)
    HeaderPos = strfind(OutJobdeck_JobdeckHeader,char(TextChanges(t,1)));
    FooterPos = strfind(OutJobdeck_JobdeckFooter,char(TextChanges(t,1)));
    for idx = fliplr(1:length(HeaderPos))
        OutJobdeck_JobdeckHeader = [OutJobdeck_JobdeckHeader(1:HeaderPos(idx)-1),char(TextChanges(t,2)),...
                                    OutJobdeck_JobdeckHeader(HeaderPos(idx)+length(char(TextChanges(t,1))):end)];
    end
    for idx = fliplr(1:length(FooterPos))
        OutJobdeck_JobdeckFooter = [OutJobdeck_JobdeckFooter(1:FooterPos(idx)-1),char(TextChanges(t,2)),...
                                    OutJobdeck_JobdeckFooter(FooterPos(idx)+length(char(TextChanges(t,1))):end)];
    end   
end

% Loading the template file:
fid = fopen([GetCodeFolderPWD(),'Templates/jdf_Template.ftxt'],'r');
i = 1;
tline = fgetl(fid);
A{i} = tline;
while ischar(tline)
    i = i+1;
    tline = fgetl(fid);
    A{i} = tline;
end
fclose(fid);
A = A(:);

% Adding the parameters:
A(1) = {['FLOW ',FileName]};
A(72) = {['FILE_NAME = .%5C',Directory_Path,'%5C',InDDS_FileName,'.gds']};
A(53) = {['CELLNAME = ',Extract_CellName]};
A(18) = {['FILE_NAME = .%5C',Directory_Path,'%5C',OutJobdeck_FileName,'.jdf']};
A(21) = {['JobdeckHeader = ',OutJobdeck_JobdeckHeader]};
A(22) = {['JobdeckFooter = ',OutJobdeck_JobdeckFooter]};
A(23) = {['MarkerLayer = ', Strings.MarkerLayer]};
A(37) = {['JOBDECKLAYERINFO = ',char(OutJobdeck_Table(1,1)),';.%5C',Directory_Path,'%5C',char(OutJobdeck_Table(1,2)),'.v30;;',char(OutJobdeck_Table(1,3))]};
for t = 2:size(OutJobdeck_Table,1)
    A = [A(1:35+t);...
         {['JOBDECKLAYERINFO = ',char(OutJobdeck_Table(t,1)),';.%5C',Directory_Path,'%5C',char(OutJobdeck_Table(t,2)),'.v30;;',char(OutJobdeck_Table(t,3))]};...
         A(36+t:end)];
end

% Saving new file:
fid = fopen([FileName,'.ftxt'], 'w');
for i = 1:numel(A)
    if A{i}==-1
        break
    end
    fprintf(fid,'%s\r\n', A{i});
end
fclose(fid);
clearvars fid A;

% Running Beamer:
%system(['"',BeamerPath,'" "',CD,'\',FileName,'.ftxt"']);

end
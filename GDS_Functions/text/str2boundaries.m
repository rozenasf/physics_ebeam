function textSegments = str2boundaries (text, varargin)

p = inputParser;

p.addRequired('text', @ischar);
p.addParamValue('fontName', 'sixteen segments', @(x)ischar(x) && size(x,1) == 1 && (strcmp(x, 'sixteen segments') || strcmp(x, '5x7 pixels') ));
p.addParamValue('letterHeight', 10.0, @(x)isreal(x) && x > 0);
p.addParamValue('letterSpacing', 0.2, @(x)isreal(x) && x > 0);
p.addParamValue('heightToWidthRatio', 1.5, @(x)isreal(x) && x > 0);
p.addParamValue('segmentWidth', 0.2, @(x)isreal(x) && x > 0);
p.addParamValue('shearAngle', 10.0, @(x)isreal(x) && x > 0);
p.addParamValue('segmentsGap', 0.04, @(x)isreal(x) && x > 0);
p.addParamValue('layer', 200, @(x)isscalar(x) && isreal(x) && mod(x,1) == 0 && x >= 0 && x < 256);
p.addParamValue('dataType', 0, @(x)isscalar(x) && isreal(x) && mod(x,1) == 0 && x >= 0 && x < 256);
p.addParamValue('portion', 'full', @(x)ischar(x) && size(x,1) == 1 && (strcmp(x, 'full') || strcmp(x, 'upperRight') || strcmp(x, 'lowerLeft') ));
p.addParamValue('hAlign', 'none', @(x)ischar(x) && size(x,1) == 1 && (strcmp(x, 'none') || strcmp(x, 'left') || strcmp(x, 'center') || strcmp(x, 'right') ));
p.addParamValue('vAlign', 'none', @(x)ischar(x) && size(x,1) == 1 && (strcmp(x, 'none') || strcmp(x, 'bottom') || strcmp(x, 'center') || strcmp(x, 'top') ));

p.parse(text, varargin{:});

p = p.Results;

lh = p.letterHeight;
ls = p.letterSpacing * p.letterHeight;
w2h = p.heightToWidthRatio;
lw = p.segmentWidth;
shr = tan(p.shearAngle/180*pi);
g2w = p.segmentsGap/lw;

if strcmp(p.fontName, 'sixteen segments')
    PR = 1:7;
    if strcmp(p.portion, 'upperRight')
        PR = [1:4 1];
    else
        if strcmp(p.portion, 'lowerLeft')
            PR = [4:7 4];
        end
    end
else
    PR = 1:5;
    if strcmp(p.portion, 'upperRight')
        PR = [1:3 1];
    else
        if strcmp(p.portion, 'lowerLeft')
            PR = [3:5 3];
        end
    end
end
s2 = sqrt(2);

lp = lh / w2h + ls;
lnp = lh + ls*w2h;

textSegments = [];

letterColumn = 0;
letterLine = 0;

for letterIndex = 1:length(text)

    letter = text(letterIndex);
    
    if letter == 10
        
        letterLine = letterLine + 1;
        letterColumn = 0;
    else

        % generating the letter segments (START)
        letterParams = getLetterParams(letter, p.fontName);
    
        if strcmp(p.fontName, 'sixteen segments')

            if ~isempty(letterParams)
            
                letterSegments = [];
                segments = letterParams.segments;
                
                M = [1 shr; 0 1];
                
                r = [0 lw/2 lw/2 0 -lw/2 -lw/2 0; ...
                    g2w*lw g2w*lw+lw/2 w2h-g2w*lw-lw/2 w2h-g2w*lw w2h-g2w*lw-lw/2 g2w*lw+lw/2 g2w*lw];
                
                if segments(1)
                    R = M*(r+repmat([0 0]',1,7));
                    letterSegments = [letterSegments struct('points', R(:,PR))];
                end
                
                if segments(2)
                    R = M*(r+repmat([1 0]',1,7));
                    letterSegments = [letterSegments struct('points', R(:,PR))];
                end
                
                if segments(3)
                    R = M*(r+repmat([2 0]',1,7));
                    letterSegments = [letterSegments struct('points', R(:,PR))];
                end
                
                if segments(4)
                    R = M*(r+repmat([0 w2h]',1,7));
                    letterSegments = [letterSegments struct('points', R(:,PR))];
                end
                
                if segments(5)
                    R = M*(r+repmat([1 w2h]',1,7));
                    letterSegments = [letterSegments struct('points', R(:,PR))];
                end
                
                if segments(6)
                    R = M*(r+repmat([2 w2h]',1,7));
                    letterSegments = [letterSegments struct('points', R(:,PR))];
                end
                
                r = [0 lw/2 lw/2 0 -lw/2 -lw/2 0; ...
                    g2w*lw g2w*lw+lw/2 1-g2w*lw-lw/2 1-g2w*lw 1-g2w*lw-lw/2 g2w*lw+lw/2 g2w*lw];
                r = [0 1; 1 0]*r;
                
                if segments(7)
                    R = M*(r+repmat([0 0]',1,7));
                    letterSegments = [letterSegments struct('points', R(:,PR))];
                end
                
                if segments(8)
                    R = M*(r+repmat([1 0]',1,7));
                    letterSegments = [letterSegments struct('points', R(:,PR))];
                end
                
                if segments(9)
                    R = M*(r+repmat([0 w2h]',1,7));
                    letterSegments = [letterSegments struct('points', R(:,PR))];
                end
                
                if segments(10)
                    R = M*(r+repmat([1 w2h]',1,7));
                    letterSegments = [letterSegments struct('points', R(:,PR))];
                end
                
                if segments(11)
                    R = M*(r+repmat([0 2*w2h]',1,7));
                    letterSegments = [letterSegments struct('points', R(:,PR))];
                end
                
                if segments(12)
                    R = M*(r+repmat([1 2*w2h]',1,7));
                    letterSegments = [letterSegments struct('points', R(:,PR))];
                end
                
                a = w2h-2*(g2w*lw+lw/2);
                b = 1-2*(g2w*lw+lw/2);
                
                dx = norm([a,b])/a*lw/2;
                dy = norm([a,b])/b*lw/2;
                
                r = [g2w*lw+lw/2 g2w*lw+lw/2+dx 1-(g2w*lw+lw/2) 1-(g2w*lw+lw/2) 1-(g2w*lw+lw/2)-dx g2w*lw+lw/2 g2w*lw+lw/2; ...
                    g2w*lw+lw/2 g2w*lw+lw/2 w2h-(g2w*lw+lw/2)-dy w2h-(g2w*lw+lw/2) w2h-(g2w*lw+lw/2) g2w*lw+lw/2+dy g2w*lw+lw/2];
                r = fliplr(r);
                
                if segments(13)
                    R = M*(r+repmat([0 0]',1,7));
                    letterSegments = [letterSegments struct('points', R(:,PR))];
                end
                
                if segments(14)
                    R = M*(r+repmat([1 w2h]',1,7));
                    letterSegments = [letterSegments struct('points', R(:,PR))];
                end
                
                r(1,:) = 1-r(1,:);
                
                if segments(15)
                    R = M*(r+repmat([1 0]',1,7));
                    letterSegments = [letterSegments struct('points', R(:,PR))];
                end
                
                if segments(16)
                    R = M*(r+repmat([0 w2h]',1,7));
                    letterSegments = [letterSegments struct('points', R(:,PR))];
                end
                
                transformStr = sprintf('translate(%f,%f) scale(%f)', ...
                    lp*(letterColumn), -lnp*letterLine, lh/w2h/2);
                
                if ~isempty(letterSegments)
                    [letterSegments.layer] = deal(p.layer);
                    [letterSegments.dataType] = deal(p.dataType);
                    
                    textSegments = [applyTransform(letterSegments, transformStr) textSegments];
                end
                
                letterColumn = letterColumn + 1;
            end
        else
            if ~isempty(letterParams)
            
                M = [1 shr; 0 1];
                pixelRect = struct('points', [-0.5 0.5 0.5 -0.5 -0.5 ; 0.5 0.5 -0.5 -0.5 0.5 ]);
                pixelRect = applyTransform(pixelRect, ...
                    sprintf('scale(%f %f) translate(0.5 0.5) scale(%f)', ...
                    lh/w2h/5, lh/7, 1-g2w));

                pixelRect.points = pixelRect.points(:, PR);
                
                letterSegments = [];
                
                for L = 1:7
                    for C = 1:5
                        if letterParams.bitmap(L,C)
                            
                            letterSegments = [letterSegments applyTransform(pixelRect, ...
                                [1 0 lh/w2h/5*(C-1); 0 1 lh/7*(L-1); 0 0 1])];
                        end
                    end
                end
                
                transformStr = sprintf('translate(%f,%f) skewx(%f)', ...
                    lp*(letterColumn), -lnp*letterLine, p.shearAngle);
                
                if ~isempty(letterSegments)
                    [letterSegments.layer] = deal(p.layer);
                    [letterSegments.dataType] = deal(0);

                    textSegments = [applyTransform(letterSegments, str2transformMatrix(transformStr)) textSegments];
                end
                
                letterColumn = letterColumn + 1;
            end            
        end
        
    end
end

if ~strcmp(p.hAlign, 'none') || ~strcmp(p.hAlign, 'none')
    
    bbox = boundingBox(textSegments);
    
    dx = 0;
    dy = 0;
    
   
    if ~strcmp(p.hAlign, 'none')
       
        if strcmp(p.hAlign, 'left')
            
            dx = -bbox(1);
        else
            if strcmp(p.hAlign, 'center')
                
                dx = -(bbox(1)+bbox(3))/2;
            else
                
                dx = -bbox(3);
            end
        end
    end
    
    if ~strcmp(p.vAlign, 'none')
       
        if strcmp(p.vAlign, 'bottom')
            
            dy = -bbox(2);
        else
            if strcmp(p.vAlign, 'center')
                
                dy = -(bbox(2)+bbox(4))/2;
            else
                
                dy = -bbox(4);
            end
        end
    end
    
    textSegments = applyTransform(textSegments, [1 0 dx; 0 1 dy; 0 0 1]);
end




function letterParams = getLetterParams(letter, fontName)

persistent ascii_tab;
persistent pixel_tab;

if isempty(ascii_tab)
    ascii_tab{1} = struct('char', '.', 'segments', [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 ]);
    ascii_tab{' '-0} = struct('char', ' ', 'segments', [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ]);
    ascii_tab{'#'-0} = struct('char', '#', 'segments', [0 1 1 0 1 1 1 1 1 1 0 0 0 0 0 0 ]);
    ascii_tab{','-0} = struct('char', ',', 'segments', [0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 ]);
    ascii_tab{'+'-0} = struct('char', '+', 'segments', [0 1 0 0 1 0 0 0 1 1 0 0 0 0 0 0 ]);
    ascii_tab{'-'-0} = struct('char', '-', 'segments', [0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 ]);
    ascii_tab{'.'-0} = struct('char', '.', 'segments', [0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 ]);
    ascii_tab{'0'-0} = struct('char', '0', 'segments', [1 0 1 1 0 1 1 1 0 0 1 1 1 1 0 0 ]);
    ascii_tab{'1'-0} = struct('char', '1', 'segments', [0 0 1 0 0 1 0 0 0 0 0 0 0 0 0 0 ]);
    ascii_tab{'2'-0} = struct('char', '2', 'segments', [1 0 0 0 0 1 1 1 1 1 1 1 0 0 0 0 ]);
    ascii_tab{'3'-0} = struct('char', '3', 'segments', [0 0 1 0 0 1 1 1 1 1 1 1 0 0 0 0 ]);
    ascii_tab{'4'-0} = struct('char', '4', 'segments', [0 0 1 1 0 1 0 0 1 1 0 0 0 0 0 0 ]);
    ascii_tab{'5'-0} = struct('char', '5', 'segments', [0 0 0 1 0 0 1 1 1 0 1 1 0 0 1 0 ]);
    ascii_tab{'6'-0} = struct('char', '6', 'segments', [1 0 1 1 0 0 1 1 1 1 1 1 0 0 0 0 ]);
    ascii_tab{'7'-0} = struct('char', '7', 'segments', [0 0 1 0 0 1 0 0 0 0 1 1 0 0 0 0 ]);
    ascii_tab{'8'-0} = struct('char', '8', 'segments', [1 0 1 1 0 1 1 1 1 1 1 1 0 0 0 0 ]);
    ascii_tab{'9'-0} = struct('char', '9', 'segments', [0 0 1 1 0 1 1 1 1 1 1 1 0 0 0 0 ]);
    ascii_tab{'='-0} = struct('char', '=', 'segments', [0 0 0 0 0 0 1 1 1 1 0 0 0 0 0 0 ]);
    ascii_tab{'A'-0} = struct('char', 'A', 'segments', [1 0 1 1 0 1 0 0 1 1 1 1 0 0 0 0 ]);
    ascii_tab{'B'-0} = struct('char', 'B', 'segments', [0 1 1 0 1 1 1 1 1 1 1 1 0 0 0 0 ]);
    ascii_tab{'C'-0} = struct('char', 'C', 'segments', [1 0 0 1 0 0 1 1 0 0 1 1 0 0 0 0 ]);
    ascii_tab{'D'-0} = struct('char', 'D', 'segments', [0 1 1 0 1 1 1 1 0 0 1 1 0 0 0 0 ]);
    ascii_tab{'E'-0} = struct('char', 'E', 'segments', [1 0 0 1 0 0 1 1 1 1 1 1 0 0 0 0 ]);
    ascii_tab{'F'-0} = struct('char', 'F', 'segments', [1 0 0 1 0 0 0 0 1 1 1 1 0 0 0 0 ]);
    ascii_tab{'G'-0} = struct('char', 'G', 'segments', [1 0 1 1 0 0 1 1 0 1 1 1 0 0 0 0 ]);
    ascii_tab{'H'-0} = struct('char', 'H', 'segments', [1 0 1 1 0 1 0 0 1 1 0 0 0 0 0 0 ]);
    ascii_tab{'I'-0} = struct('char', 'I', 'segments', [0 1 0 0 1 0 1 1 0 0 1 1 0 0 0 0 ]);
    ascii_tab{'J'-0} = struct('char', 'J', 'segments', [1 0 1 0 0 1 1 1 0 0 0 0 0 0 0 0 ]);
    ascii_tab{'K'-0} = struct('char', 'K', 'segments', [1 0 0 1 0 0 0 0 1 0 0 0 0 1 1 0 ]);
    ascii_tab{'L'-0} = struct('char', 'L', 'segments', [1 0 0 1 0 0 1 1 0 0 0 0 0 0 0 0 ]);
    ascii_tab{'M'-0} = struct('char', 'M', 'segments', [1 0 1 1 0 1 0 0 0 0 0 0 0 1 0 1 ]);
    ascii_tab{'N'-0} = struct('char', 'N', 'segments', [1 0 1 1 0 1 0 0 0 0 0 0 0 0 1 1 ]);
    ascii_tab{'O'-0} = struct('char', 'O', 'segments', [1 0 1 1 0 1 1 1 0 0 1 1 0 0 0 0 ]);
    ascii_tab{'P'-0} = struct('char', 'P', 'segments', [1 0 0 1 0 1 0 0 1 1 1 1 0 0 0 0 ]);
    ascii_tab{'Q'-0} = struct('char', 'Q', 'segments', [1 0 1 1 0 1 1 1 0 0 1 1 0 0 1 0 ]);
    ascii_tab{'R'-0} = struct('char', 'R', 'segments', [1 0 0 1 0 1 0 0 1 1 1 1 0 0 1 0 ]);
    ascii_tab{'S'-0} = struct('char', 'S', 'segments', [0 0 1 1 0 0 1 1 1 1 1 1 0 0 0 0 ]);
    ascii_tab{'T'-0} = struct('char', 'T', 'segments', [0 1 0 0 1 0 0 0 0 0 1 1 0 0 0 0 ]);
    ascii_tab{'U'-0} = struct('char', 'U', 'segments', [1 0 1 1 0 1 1 1 0 0 0 0 0 0 0 0 ]);
    ascii_tab{'V'-0} = struct('char', 'V', 'segments', [1 0 0 1 0 0 0 0 0 0 0 0 1 1 0 0 ]);
    ascii_tab{'W'-0} = struct('char', 'W', 'segments', [1 0 1 1 0 1 0 0 0 0 0 0 1 0 1 0 ]);
    ascii_tab{'X'-0} = struct('char', 'X', 'segments', [0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 ]);
    ascii_tab{'Y'-0} = struct('char', 'Y', 'segments', [0 1 0 1 0 1 0 0 1 1 0 0 0 0 0 0 ]);
    ascii_tab{'Z'-0} = struct('char', 'Z', 'segments', [0 0 0 0 0 0 1 1 0 0 1 1 1 1 0 0 ]);
    ascii_tab{'['-0} = struct('char', '[', 'segments', [1 0 0 1 0 0 1 0 0 0 1 0 0 0 0 0 ]);
    ascii_tab{']'-0} = struct('char', ']', 'segments', [0 0 1 0 0 1 0 1 0 0 0 1 0 0 0 0 ]);
    ascii_tab{'_'-0} = struct('char', '_', 'segments', [0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 ]);
    ascii_tab{'/'-0} = struct('char', '/', 'segments', [0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 ]);
end

if isempty(pixel_tab)
    for asciiIndex = [32:127]
       pixel_tab{asciiIndex} = struct('char', char(asciiIndex), ...
           'bitmap', flipud(imread(['Small Fonts\' num2str(asciiIndex) '.bmp'])));
    end
    
    pixel_tab{' '}.bitmap = repmat(false, 7, 5);
end
   
if strcmp(fontName, 'sixteen segments')
    if letter > length(ascii_tab)
        letterParams = [];
    else
        letterParams = ascii_tab{letter};
    end
else
    if letter > length(pixel_tab)
        letterParams = [];
    else
        letterParams = pixel_tab{letter};
    end
end
    


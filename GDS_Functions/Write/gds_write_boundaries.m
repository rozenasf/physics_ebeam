function gds_write_boundaries (gdsFile, boundaries)

libCreated = false;

if ischar(gdsFile)
    
    gdsFileName = gdsFile;
    gdsFile = gds_create_library(gdsFileName);
    gds_start_structure(gdsFile, gdsFileName);
    
    libCreated = true;
end
    


for boundary = boundaries
    
    if boundary.points(1,1) ~= boundary.points(1,end) || boundary.points(2,1) ~= boundary.points(2,end)
        gds_write_path(gdsFile, boundary.layer, boundary.dataType, ...
            struct('x', boundary.points(1,:), 'y', boundary.points(2,:)), 0);
    else
        gds_write_boundary(gdsFile, boundary.layer, boundary.dataType, ...
            struct('x', boundary.points(1,:), 'y', boundary.points(2,:)));
    end
end

if libCreated
    
    gds_close_structure(gdsFile);
    gds_close_library(gdsFile);
end
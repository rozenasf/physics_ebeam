function gdsFile = gds_create_library (libName, refLibs)

gdsFile = fopen([libName '.gds'], 'w', 'ieee-be');

% HEADER record
fwrite(gdsFile, 6, 'uint16');
fwrite(gdsFile, 0, 'uint8');
fwrite(gdsFile, 2, 'uint8');

fwrite(gdsFile, 6, 'int16');

% BGNLIB record
fwrite(gdsFile, 28, 'uint16');
fwrite(gdsFile, 1, 'uint8');
fwrite(gdsFile, 2, 'uint8');

thisMoment = clock;
fwrite(gdsFile, thisMoment, 'int16');
fwrite(gdsFile, thisMoment, 'int16');

% REFLIBS record
if nargin == 2 && iscell(refLibs)

    fwrite(gdsFile, 4+44*(length(refLibs)), 'uint16');
%    fwrite(gdsFile, 4+44*(length(refLibs)+1), 'uint16');
    fwrite(gdsFile, 31, 'uint8');
    fwrite(gdsFile, 6, 'uint8');

    for refLibIndex = 1:length(refLibs)
       
        % each library name is supposed to be padded to 44
        % characters
        
        refLibName = [refLibs{refLibIndex} zeros(1, 44)];
        refLibName = refLibName(1:44);
        
        fwrite(gdsFile, refLibName, 'uint8');
    end

%    libName = zeros(1,44);
%    fwrite(gdsFile, libName, 'uint8');
end
    
% LIBNAME record
if mod(length(libName),2) == 1
    libName = [libName 0];
end

fwrite(gdsFile, 4+length(libName), 'uint16');
fwrite(gdsFile, 2, 'uint8');
fwrite(gdsFile, 6, 'uint8');

fwrite(gdsFile, libName, 'uint8');

    
% UNITS record

fwrite(gdsFile, 20, 'uint16');
fwrite(gdsFile, 3, 'uint8');
fwrite(gdsFile, 5, 'uint8');

gds_write_real8(gdsFile, 1e-3);
gds_write_real8(gdsFile, 1e-9);

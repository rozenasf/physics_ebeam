function gds_write_boundary (gdsFile, layer, dataType, boundary)

% BOUNDAY record

fwrite(gdsFile, 4, 'uint16');
fwrite(gdsFile, 8, 'uint8');
fwrite(gdsFile, 0, 'uint8');

% LAYER record

fwrite(gdsFile, 6, 'uint16');
fwrite(gdsFile, 13, 'uint8');
fwrite(gdsFile, 2, 'uint8');

fwrite(gdsFile, layer, 'int16');

% DATATYPE record

fwrite(gdsFile, 6, 'uint16');
fwrite(gdsFile, 14, 'uint8');
fwrite(gdsFile, 2, 'uint8');

fwrite(gdsFile, dataType, 'int16');

% XY record

xy = reshape([boundary.x; boundary.y], 1, []);

fwrite(gdsFile, 4+4*length(xy), 'uint16');
fwrite(gdsFile, 16, 'uint8');
fwrite(gdsFile, 3, 'uint8');

fwrite(gdsFile, round(1000*xy), 'int32');

% ENDEL record

fwrite(gdsFile, 4, 'uint16');
fwrite(gdsFile, 17, 'uint8');
fwrite(gdsFile, 0, 'uint8');


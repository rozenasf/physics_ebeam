function gds_start_structure (gdsFile, strName)

% BGNSTR record
fwrite(gdsFile, 28, 'uint16');
fwrite(gdsFile, 5, 'uint8');
fwrite(gdsFile, 2, 'uint8');

thisMoment = clock;
fwrite(gdsFile, thisMoment, 'int16');
fwrite(gdsFile, thisMoment, 'int16');

% STRNAME record
if mod(length(strName),2) == 1
    strName = [strName 0];
end

fwrite(gdsFile, 4+length(strName), 'uint16');
fwrite(gdsFile, 6, 'uint8');
fwrite(gdsFile, 6, 'uint8');

fwrite(gdsFile, strName, 'uint8');

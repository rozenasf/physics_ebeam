function gds_write_sref (gdsFile, srefName, varargin)

p = inputParser;

p.addRequired('gdsFile', @(x)isempty(ferror(x)));
p.addRequired('srefName', @ischar);
p.addParamValue('refPoint', [0;0], @(x)isreal(x) && size(x,1) == 2 && size(x,2) == 1);
p.addParamValue('reflection', false, @(x)isscalar(x) && islogical(x));
p.addParamValue('absMagnification', false, @(x)isscalar(x) && islogical(x));
p.addParamValue('absAngle', false, @(x)isscalar(x) && islogical(x));
p.addParamValue('magnification', 1.0, @(x)isreal(x) && x > 0);
p.addParamValue('angle', 0.0, @(x)isreal(x) && x >= 0 && x < 360.0);

p.parse(gdsFile, srefName, varargin{:});

p = p.Results;

% SREF record

fwrite(gdsFile, 4, 'uint16');
fwrite(gdsFile, 10, 'uint8');
fwrite(gdsFile, 0, 'uint8');

% SNAME record
if mod(length(p.srefName),2) == 1
    p.srefName = [p.srefName 0];
end

fwrite(gdsFile, 4+length(p.srefName), 'uint16');
fwrite(gdsFile, 18, 'uint8');
fwrite(gdsFile, 6, 'uint8');

fwrite(gdsFile, p.srefName, 'uint8');

if any([p.reflection p.absMagnification p.absAngle p.magnification~=1.0 p.angle~=0.0])

    % STRANS record
    
    strans = 0;
    
    if p.reflection
        strans = bitor(strans, 2^15);
    end
    
    if p.absMagnification
        strans = bitor(strans, 2^2);
    end
    
    if p.absAngle
        strans = bitor(strans, 2^1);
    end

    fwrite(gdsFile, 6, 'uint16');
    fwrite(gdsFile, 26, 'uint8');
    fwrite(gdsFile, 1, 'uint8');

    fwrite(gdsFile, strans, 'uint16');

    
    if p.magnification ~= 1.0
        
        % MAG record

        fwrite(gdsFile, 12, 'uint16');
        fwrite(gdsFile, 27, 'uint8');
        fwrite(gdsFile, 5, 'uint8');
        
        gds_write_real8(gdsFile, p.magnification);
    end
    
    if p.angle ~= 0.0
        
        % ANGLE record

        fwrite(gdsFile, 12, 'uint16');
        fwrite(gdsFile, 28, 'uint8');
        fwrite(gdsFile, 5, 'uint8');
        
        gds_write_real8(gdsFile, p.angle);
    end
end

% XY record

p.refPoint = reshape(p.refPoint, 1, []);

fwrite(gdsFile, 4+4*length(p.refPoint), 'uint16');
fwrite(gdsFile, 16, 'uint8');
fwrite(gdsFile, 3, 'uint8');

fwrite(gdsFile, round(1000*p.refPoint), 'int32');

% ENDEL record

fwrite(gdsFile, 4, 'uint16');
fwrite(gdsFile, 17, 'uint8');
fwrite(gdsFile, 0, 'uint8');

    
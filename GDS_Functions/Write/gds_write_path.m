function gds_write_path (gdsFile, layer, dataType, path, width)

% PATH record

fwrite(gdsFile, 4, 'uint16');
fwrite(gdsFile, 9, 'uint8');
fwrite(gdsFile, 0, 'uint8');

% LAYER record

fwrite(gdsFile, 6, 'uint16');
fwrite(gdsFile, 13, 'uint8');
fwrite(gdsFile, 2, 'uint8');

fwrite(gdsFile, layer, 'int16');

% DATATYPE record

fwrite(gdsFile, 6, 'uint16');
fwrite(gdsFile, 14, 'uint8');
fwrite(gdsFile, 2, 'uint8');

fwrite(gdsFile, dataType, 'int16');

% WIDTH record

fwrite(gdsFile, 8, 'uint16');
fwrite(gdsFile, 15, 'uint8');
fwrite(gdsFile, 3, 'uint8');

fwrite(gdsFile, round(1000*width), 'int32');

% XY record

xy = reshape([path.x; path.y], 1, []);

fwrite(gdsFile, 4+4*length(xy), 'uint16');
fwrite(gdsFile, 16, 'uint8');
fwrite(gdsFile, 3, 'uint8');

fwrite(gdsFile, round(1000*xy), 'int32');

% ENDEL record

fwrite(gdsFile, 4, 'uint16');
fwrite(gdsFile, 17, 'uint8');
fwrite(gdsFile, 0, 'uint8');


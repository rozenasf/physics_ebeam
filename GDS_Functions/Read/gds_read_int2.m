function Int2 = gds_read_int2(fid)

    headerLength = fread(fid, 2, 'uint8');
    vInt2 = headerLength(1)*256+headerLength(2);
    
    if vInt2 >= 2^15
        vInt2 = 2^16-vInt2;
    end
end

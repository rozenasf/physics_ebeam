function vRecord = gds_read_record(fid, fout, fileOffset)

    vRecord.length  = fread(fid, 1, 'uint16');
    vRecord.recKind = fread(fid, 1, 'uint8');
    vRecord.valKind = fread(fid, 1, 'uint8');
    vRecord.offset = fileOffset;

    [vRecord.content, vRecord.recName] = gds_read_record_content(fid, vRecord, fout, fileOffset);
    
function [vContent, recordName] = gds_read_record_content(fid, header, fout, fileOffset);

switch header.recKind

    case 0    % HEADER

        recordName = 'HEADER';
        
        version = fread(fid, 1, 'uint16');
        if version < 10
            vContent.version = sprintf('v%d.0', version);
        else
            vContent.version = sprintf('v%d.%d.%d', ...
                                        floor(version/100), ...
                                        floor(mod(version, 100)/10), ...
                                        mod(version, 10));
        end
        
        fprintf(fout, [' + HEADER [0x%08x]\r\n' ...
                 '   - version : [' vContent.version ']\r\n'], fileOffset);

    case 1    % BGNLIB
        
        recordName = 'BGNLIB';

        vContent.libCreated = readDate(fid);
        vContent.libUpdated = readDate(fid);
        
        fprintf(fout, [' + BGNLIB [0x%08x]\r\n' ...
                 '   - created : ' vContent.libCreated.text '\r\n' ...
                 '   - updated : ' vContent.libUpdated.text '\r\n'], fileOffset);
                 
    case 2    % LIBNAME

        recordName = 'LIBNAME';

        vContent.libName = gds_read_string(fid, header.length-4);
        
        fprintf(fout, [' + LIBNAME [0x%08x]\r\n' ...
                 '   - libName [' vContent.libName ']\r\n'], fileOffset);
        
    case 3    % UNITS

        recordName = 'UNITS';

        vContent.userUnit = gds_read_real8(fid);
        vContent.libUnit = gds_read_real8(fid);
        
        strout = sprintf([' + UNITS [0x%08x]\r\n' ...
                          '   - user unit : %1.1e\r\n' ...
                          '   - library unit : %1.1em\r\n'], fileOffset, vContent.userUnit, vContent.libUnit);
                      
        for n = 1:6, strout = strrep(strout, 'e-0', 'e-'); end;
        fprintf(fout, strout);
        
    case 4    % ENDLIB
        
        recordName = 'ENDLIB';

        fprintf(fout, ' + ENDLIB [0x%08x]\r\n', fileOffset);
        vContent = [];
        

    case 5    % BGNSTR
        
        recordName = 'BGNSTR';

        vContent.structureCreated = readDate(fid);
        vContent.structureUpdated = readDate(fid);
        
        fprintf(fout, [' + BGNSTR [0x%08x]\r\n' ...
                 '   - created : ' vContent.structureCreated.text '\r\n' ...
                 '   - updated : ' vContent.structureUpdated.text '\r\n'], fileOffset);
    
    case 6    % STRNAME
        
        recordName = 'STRNAME';

        vContent.structName = gds_read_string(fid, header.length-4);
        
        fprintf(fout, [' + STRNAME [0x%08x]\r\n' ...
                 '   - structName [' vContent.structName ']\r\n'], fileOffset);

    case 7    % ENDSTR
                
        recordName = 'ENDSTR';

        fprintf(fout, [' + ENDSTR [0x%08x]\r\n'], fileOffset);
        vContent = [];
                 
    case 8    % BOUNDARY
                
        recordName = 'BOUNDARY';

        fprintf(fout, [' + BOUNDARY [0x%08x]\r\n'], fileOffset);
        vContent = [];
                 
    case 9    % PATH
                
        recordName = 'PATH';

        fprintf(fout, [' + PATH [0x%08x]\r\n'], fileOffset);
        vContent = [];
                 
    case 10   % SREF
                
        recordName = 'SREF';

        fprintf(fout, [' + SREF [0x%08x]\r\n'], fileOffset);
        vContent = [];
                 
    case 11   % AREF
                
        recordName = 'AREF';

        fprintf(fout, [' + AREF [0x%08x]\r\n'], fileOffset);
        vContent = [];
                 
    case 13   % LAYER
        
        recordName = 'LAYER';

        vContent.layer = fread(fid, 1, 'int16');
        
        fprintf(fout, [' + LAYER [0x%08x]\r\n' ...
                 '   - layer : %d\r\n'], fileOffset, vContent.layer);
    case 14   % DATATYPE
        
        recordName = 'DATATYPE';

        vContent.datatype = fread(fid, 1, 'int16');
        
        fprintf(fout, [' + DATATYPE [0x%08x]\r\n' ...
                 '   - datatype : %d\r\n'], fileOffset, vContent.datatype);

    case 15   % WIDTH

        recordName = 'WIDTH';

        vContent.width = fread(fid, 1, 'int32');
        
        strout = sprintf([' + WIDTH [0x%08x]\r\n' ...
                          '   - width : %d\r\n'], fileOffset, vContent.width);
                      
        fprintf(fout, strout);
        
    case 16   % XY

        recordName = 'XY';

        xy = fread(fid, (header.length-4)/4, 'int32');
        vContent.xy = [xy(1:2:end) xy(2:2:end)]; 
      
        fprintf(fout, [' + XY [0x%08x]\r\n' ...
                 '   - (%d,%d)'], fileOffset, xy(1:2));
        if length(xy) > 2     
            fprintf(fout, ' -> (%d,%d)', xy(3:end));
        end
        
        fprintf(fout, '\r\n');
        
    case 17   % ENDEL
                
        recordName = 'ENDEL';

        fprintf(fout, [' + ENDEL [0x%08x]\r\n'], fileOffset);
        vContent = [];
                 
    case 18   % SNAME
        
        recordName = 'SNAME';

        vContent.srefName = gds_read_string(fid, header.length-4);
        
        fprintf(fout, [' + SNAME [0x%08x]\r\n' ...
                 '   - srefName [' vContent.srefName ']\r\n'], fileOffset);


    case 19   % COLROW
        
        recordName = 'COLROW';

        vContent.columns = fread(fid, 1, 'int16');
        vContent.rows = fread(fid, 1, 'int16');

        fprintf(fout, [' + COLROW [0x%08x]\r\n' ...
                 '   - columns : %d, rows : %d\r\n'], fileOffset, vContent.columns, vContent.rows);

        
    case 26   % STRANS
        
        recordName = 'STRANS';

        bitArray = fread(fid, 1, '*uint16');
        vContent.reflection = (bitand(bitArray, 2^15) ~= 0);
        vContent.absMagnification = (bitand(bitArray, 2^2) ~= 0);
        vContent.absAngle = (bitand(bitArray, 2^1) ~= 0);
        
        fprintf(fout, [' + STRANS [0x%08x]\r\n'], fileOffset);
        if vContent.reflection
            fprintf(fout, '   - reflected : true\r\n');
        else
            fprintf(fout, '   - reflected : false\r\n');
        end
        
        if vContent.absMagnification
            fprintf(fout, '   - absolute magnification : true\r\n');
        else
            fprintf(fout, '   - absolute magnification : false\r\n');
        end
        
        if vContent.absAngle
            fprintf(fout, '   - absolute angle : true\r\n');
        else
            fprintf(fout, '   - absolute angle : false\r\n');
        end

    case 27   % MAG

        recordName = 'MAG';

        vContent.magnification = gds_read_real8(fid);

        strout = sprintf([' + MAG [0x%08x]\r\n' ...
                          '   - magnification : %1.1e\r\n'], fileOffset, vContent.magnification);
                      
        for n = 1:2, strout = strrep(strout, 'e-0', 'e-'); end;
        for n = 1:2, strout = strrep(strout, 'e+0', 'e+'); end;
        fprintf(fout, strout);
        
    case 28   % ANGLE

        recordName = 'ANGLE';

        vContent.angle = gds_read_real8(fid);

        strout = sprintf([' + ANGLE [0x%08x]\r\n' ...
                          '   - angle : %1.1e\r\n'], fileOffset, vContent.angle);
                      
        for n = 1:2, strout = strrep(strout, 'e-0', 'e-'); end;
        for n = 1:2, strout = strrep(strout, 'e+0', 'e+'); end;
        fprintf(fout, strout);
        
    case 31   % REFLIBS
        
        recordName = 'REFLIBS';

        vContent.refLibs = {};

        strout = sprintf(' + REFLIBS [0x%08x]\r\n', fileOffset);
        
        for refLibIndex = 1:(header.length-4)/44
        
            refLib = gds_read_string(fid, 44);

            strout = [strout ...
                sprintf('   - lib name : %s\r\n', refLib)];
            
            if ~isempty(refLib)
                vContent.refLibs = {vContent.refLibs{:} refLib};
            end
        end
        
        fprintf(fout, strout);
        
    case 33   % PATHTYPE
        
        recordName = 'PATHTYPE';

        pathtype = fread(fid, 1, 'int16');
        
        switch pathtype
            
            case 0
                
                vContent.pathType = 'square ended path';
            case 1
                
                vContent.pathType = 'round ended';
            case 2
                
                vContent.pathType = 'square ended, extended 1/2 width';
            case 4
                
                vContent.pathType = 'variable length extensions, CustomPlus';
            otherwise
                
                vContent.pathType = 'unknown path type';
        end
        
        fprintf(fout, [' + PATHTYPE [0x%08x]\r\n' ...
                 '   - pathtype : %s\r\n'], fileOffset, vContent.pathType);

        
    case 45   % STRTYPE
                
        recordName = 'STRTYPE';

        fprintf(fout, [' + STRTYPE [0x%08x]\r\n'], fileOffset);
        vContent = [];
                 
    case 46   % BOXTYPE
        
        recordName = 'BOXTYPE';

        vContent.boxtype = fread(fid, 1, 'int16');
        
        fprintf(fout, [' + BOXTYPE [0x%08x]\r\n' ...
                 '   - boxtype : %d\r\n'], fileOffset, vContent.boxtype);

    otherwise
        
        recordName = 'UNHANDLED';

        fprintf(fout, '>+ record %d, length %d [0x%08x]\r\n', header.recKind, header.length, fileOffset);
        fread(fid, header.length-4, 'uchar');
        vContent = [];
end

function date = readDate(fid)

MONTHS = {'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'};

numbers = fread(fid, 6, 'int16');

date.year = numbers(1);
date.month = numbers(2);
date.day = numbers(3);
date.hour = numbers(4);
date.minute = numbers(5);
date.second = numbers(6);

date.text = sprintf('%2d %s %d %2d:%02d:%02d', date.day, MONTHS{date.month}, date.year, date.hour, date.minute, date.second);


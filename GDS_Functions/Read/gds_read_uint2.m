function vUInt2 = gds_read_uint2(fid)

    headerLength = fread(fid, 2, 'uint8');
    vUInt2 = headerLength(1)*256+headerLength(2);

end

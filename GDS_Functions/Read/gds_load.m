function gdsLibData = gds_load(gdsFileName, logFileName)
% gdsLibData = gds_load([gdsFileName [, logFileName]])
%
%   this function loads a GDSII file. It can output the results as text to
%   an opened file (fout), and outputs the resutls as a structure.
%   
%   gdsFileName  - [OPTIONAL] GDSII file name, if not specified, a dialog
%                  for choosing the file is opened
%   logFileName  - [OPTIONAL] file name for logging, default is 
%                            "gds_load_results.txt"
%
%   (S) - a single structure, (A) - structures array
%
%   The output contains a description of all records (in the 'records' 
%   field), exactly in the order it is placed in the file. It also holds
%   the offset from the begining of the file if needed.
%
%   Now it contains general details regarding the library. libUnit means
%   the resolution of the points. userUnit is the inverse of "how much is
%   one in resolution units". Usually we talk in units of microns and store
%   the data in units of nanometers. Hence, libUnit is 1e-9 and userUnit is
%   1e-3.
%
%   The third part is the structures. For each structure, the offset and    
%   length is recorded so it can be read directly and maybe writen as a
%   whole in another library. The elements are recorded also. 
%
%   The handled element kinds are GDS_BOUNDARY, GDS_PATH, GDS_SREF & 
%   GDS_AREF. Each kind has different parameters. 
%
%   gdsLibData(S) -+-- records(A) -+-- length
%                  |               +-- recKind
%                  |               +-- valKind
%                  |               +-- offset
%                  |               +-- content
%                  |               +-- recName
%                  |                 
%                  +-- libDetails(S) -+-- version
%                  |                  +-- created(S)
%                  |                  +-- updated(S)
%                  |                  +-- name
%                  |                  +-- unit(S) -+-- userUnit
%                  |                               +-- libUnit
%                  |                   
%                  +-- structures(A) -+-- offset
%                                     +-- length            
%                                     +-- created(S)
%                                     +-- updated(S)
%                                     +-- name
%                                     +-- elements(A) -+-- kind
%                                                      +-- element
%
%   kind is one of {GDS_BOUNDARY(1),GDS_PATH(2),GDS_SREF(4),GDS_AREF(5)},
%   each has its on element fields. All coordinates and widths values are 
%   integers in units of libUnit.

persistent gdsPathName;

if nargin == 0
    
    if isempty(gdsPathName);
        [gdsFileName, gdsPathName] = uigetfile(['*.gds'], 'Select GDSII file to load');
    else
        [gdsFileName, gdsPathName] = uigetfile([gdsPathName '*.gds'], 'Select GDSII file to load');
    end
        
    if isnumeric(gdsFileName)
        msgbox('next time choose something...', 'What''s wrong with you?');
        return;
    end
    
    gdsFileName = [gdsPathName gdsFileName];
end

[fin,msg] = fopen(gdsFileName, 'r', 'ieee-be');

if fin == -1
    fprintf(['*** could not open your file [' gdsFileName ']\n' ...
             '  >>> ' msg ' <<<\n']);
         
    return;              
end

fileOpened = false;
if nargin < 2
    logFileName = 'gds_load_results.txt';
end
   
fout = fopen(logFileName, 'w');

fileOffset = 0;
gdsLibData.records = [];

recordsBuffer = gds_read_record(fin, fout, fileOffset);

while recordsBuffer(end).recKind ~= 4

    fileOffset = fileOffset + recordsBuffer(end).length;

    if length(recordsBuffer) == 1000
        gdsLibData.records = [gdsLibData.records recordsBuffer];
        recordsBuffer = gds_read_record(fin, fout, fileOffset);
        
    else
        recordsBuffer = [recordsBuffer gds_read_record(fin, fout, fileOffset)];
    end
end
gdsLibData.records = [gdsLibData.records recordsBuffer];

fclose(fin);
fclose(fout);

recordNames = {gdsLibData.records.recName};

HEADER = gdsLibData.records(strmatch('HEADER', recordNames, 'exact'));
gdsLibData.libDetails.version = HEADER.content.version;

BGNLIB = gdsLibData.records(strmatch('BGNLIB', recordNames, 'exact'));
gdsLibData.libDetails.created = BGNLIB.content.libCreated;
gdsLibData.libDetails.updated = BGNLIB.content.libUpdated;

LIBNAME = gdsLibData.records(strmatch('LIBNAME', recordNames, 'exact'));
gdsLibData.libDetails.name = LIBNAME.content.libName;

UNITS = gdsLibData.records(strmatch('UNITS', recordNames, 'exact'));
gdsLibData.libDetails.units = UNITS.content;

BGNSTR_INDS = strmatch('BGNSTR', recordNames, 'exact')';
ENDSTR_INDS = strmatch('ENDSTR', recordNames, 'exact')';

for strIndex = 1:length(BGNSTR_INDS)
   
    thisStr = [];
    
    strRecords = gdsLibData.records(BGNSTR_INDS(strIndex):ENDSTR_INDS(strIndex));
    strRecordNames = {strRecords.recName};
    
    BGNSTR = strRecords(strmatch('BGNSTR', strRecordNames, 'exact'));
    ENDSTR = strRecords(end);
    
    thisStr.offset = BGNSTR.offset;
    thisStr.length = ENDSTR.offset+4-BGNSTR.offset;
    
    thisStr.created = BGNSTR.content.structureCreated;
    thisStr.updated = BGNSTR.content.structureUpdated;
    
    STRNAME = strRecords(strmatch('STRNAME', strRecordNames, 'exact'));
    thisStr.name = STRNAME.content.structName;

    thisStr.elements = [];
    
    ENDEL_INDS = strmatch('ENDEL', strRecordNames, 'exact')';
    
    % now I collect all the boundary elements
    ELEMENT_INDS = strmatch('BOUNDARY', strRecordNames, 'exact')';
    for elementIndex = ELEMENT_INDS

        elementRecords = strRecords(elementIndex:ENDEL_INDS(min(find(ENDEL_INDS > elementIndex))));
        elementNames = {elementRecords.recName};

        LAYER = elementRecords(strmatch('LAYER', elementNames, 'exact'));
        DATATYPE = elementRecords(strmatch('DATATYPE', elementNames, 'exact'));
        XY = elementRecords(strmatch('XY', elementNames, 'exact'));
        
        element = struct('layer', LAYER.content.layer, ...
                         'dataType', DATATYPE.content.datatype, ...
                         'points', XY.content.xy');
        
        thisStr.elements = [thisStr.elements ...
            struct('kind', GDS_BOUNDARY, 'element', element)];
    end
    
    % now I collect all the path elements
    ELEMENT_INDS = strmatch('PATH', strRecordNames, 'exact')';
    for elementIndex = ELEMENT_INDS

        elementRecords = strRecords(elementIndex:ENDEL_INDS(min(find(ENDEL_INDS > elementIndex))));
        elementNames = {elementRecords.recName};

        LAYER = elementRecords(strmatch('LAYER', elementNames, 'exact'));
        DATATYPE = elementRecords(strmatch('DATATYPE', elementNames, 'exact'));
        XY = elementRecords(strmatch('XY', elementNames, 'exact'));
        
        width = 0;
        WIDTH_IND = strmatch('WIDTH', elementNames, 'exact');
        if ~isempty(WIDTH_IND)
            
            WIDTH = elementRecords(WIDTH_IND);
            width = WIDTH.content.width;
        end
        
        element =  struct('layer', LAYER.content.layer, ...
                          'dataType', DATATYPE.content.datatype, ...
                          'points', XY.content.xy', ...
                          'width', width);
        
        thisStr.elements = [thisStr.elements ...
            struct('kind', GDS_PATH, 'element', element)];
        
    end
    
    % now I collect all the single reference elements
    ELEMENT_INDS = strmatch('SREF', strRecordNames, 'exact')';
    for elementIndex = ELEMENT_INDS

        elementRecords = strRecords(elementIndex:ENDEL_INDS(min(find(ENDEL_INDS > elementIndex))));
        elementNames = {elementRecords.recName};

        SNAME = elementRecords(strmatch('SNAME', elementNames, 'exact'));
        XY = elementRecords(strmatch('XY', elementNames, 'exact'));

        element = struct( ...
            'sName', SNAME.content.srefName, ...
            'refPoint', XY.content.xy', ...
            'reflection', false, ...
            'absMagnification', false, ...
            'absAngle', false, ...
            'magnification', 1.0, ...
            'angle', 0.0);
        
        STRANS_IND = strmatch('STRANS', elementNames, 'exact');
        if ~isempty(STRANS_IND)
            
            STRANS = elementRecords(STRANS_IND);

            element.reflection = STRANS.content.reflection;
            element.absMagnification = STRANS.content.absMagnification;
            element.absAngle = STRANS.content.absAngle;
            
            MAG_IND = strmatch('MAG', elementNames, 'exact');
            if ~isempty(MAG_IND)
               
                MAG = elementRecords(MAG_IND);
                element.magnification = MAG.content.magnification;                
            end

            ANGLE_IND = strmatch('ANGLE', elementNames, 'exact');
            if ~isempty(ANGLE_IND)
                
                ANGLE = elementRecords(ANGLE_IND);
                element.angle = ANGLE.content.angle;
            end
        end
        
        thisStr.elements = [thisStr.elements ...
            struct('kind', GDS_SREF, ...
                   'element', element)];
        
    end
    
    % now I collect all the array reference elements
    ELEMENT_INDS = strmatch('AREF', strRecordNames, 'exact')';
    for elementIndex = ELEMENT_INDS

        elementRecords = strRecords(elementIndex:ENDEL_INDS(min(find(ENDEL_INDS > elementIndex))));
        elementNames = {elementRecords.recName};

        SNAME = elementRecords(strmatch('SNAME', elementNames, 'exact'));
        COLROW = elementRecords(strmatch('COLROW', elementNames, 'exact'));
        XY = elementRecords(strmatch('XY', elementNames, 'exact'));

        element = struct( ...
            'sName', SNAME.content.srefName, ...
            'columns', COLROW.content.columns, ...
            'rows', COLROW.content.rows, ...
            'refPoint', XY.content.xy(1,:)', ...
            'interColumnsSpacing', (XY.content.xy(2,:)-XY.content.xy(1,:))'/COLROW.content.columns, ...
            'interRowsSpacing', (XY.content.xy(3,:)-XY.content.xy(1,:))'/COLROW.content.rows, ...
            'reflection', false, ...
            'absMagnification', false, ...
            'absAngle', false, ...
            'magnification', 1.0, ...
            'angle', 0.0);
        
        STRANS_IND = strmatch('STRANS', elementNames, 'exact');
        if ~isempty(STRANS_IND)
            
            STRANS = elementRecords(STRANS_IND);

            element.reflection = STRANS.content.reflection;
            element.absMagnification = STRANS.content.absMagnification;
            element.absAngle = STRANS.content.absAngle;
            
            MAG_IND = strmatch('MAG', elementNames, 'exact');
            if ~isempty(MAG_IND)
               
                MAG = elementRecords(MAG_IND);
                element.magnification = MAG.content.magnification;                
            end

            ANGLE_IND = strmatch('ANGLE', elementNames, 'exact');
            if ~isempty(ANGLE_IND)
                
                ANGLE = elementRecords(ANGLE_IND);
                element.angle = ANGLE.content.angle;
            end
        end
        
        thisStr.elements = [thisStr.elements ...
            struct('kind', GDS_AREF, ...
                   'element', element)];
        
    end
    
    gdsLibData.structures(strIndex) = thisStr;
end




function vStr = gds_read_string(fid, length)

vStr = fread(fid, length, '*char')';
        
if vStr(end) == 0
    
    vStr = vStr(1:(min(find(vStr == 0))-1));
end


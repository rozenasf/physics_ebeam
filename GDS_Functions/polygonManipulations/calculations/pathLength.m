function [totalLength, allLengths] = pathLength(points)

if isstruct(points)
    points = points.points;
end

dists = points(:,2:end)-points(:,1:end-1);
lagsLengths = sqrt(dists(1,:).^2+dists(2,:).^2);

lengths = cumsum([0 lagsLengths]);

totalLength = lengths(end);
if nargout == 2
    allLengths = lengths;
end
    
    
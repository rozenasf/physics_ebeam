function ForGDS=OverideGDS(Original,NewData)
    Layers=unique([NewData.layer]);
    ForGDS=[NewData,Original(find(~ismember([Original.layer],Layers)))];
end
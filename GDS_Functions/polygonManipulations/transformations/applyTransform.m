function pOut = applyTransform(pIn, transform)

if isempty(pIn)
    pOut = [];
    return;
end

if ischar(transform)
    
    transform = str2transformMatrix(transform);
end

if isstruct(pIn)

    pOut = pIn;

    for cellInd = 1:prod(size(pOut))

        pOut(cellInd).points = ...
           transform * [pOut(cellInd).points; ones(1,size(pOut(cellInd).points,2))];
        pOut(cellInd).points = pOut(cellInd).points(1:2,:);
    end
else

    pOut = transform * [pIn; ones(1,size(pIn,2))];
    pOut = pOut(1:2,:);
end





function out=ShiftGDS(in,x,y)
    out=in;
    for i=1:numel(in)
        out(i).points=in(i).points+[x;y]*ones(1,size(in(i).points,2));
    end
end
function m = trSkewX(ang)

t = tan(ang/180*pi);

m = [ 1 t 0 ; 0 1 0 ; 0 0 1 ];
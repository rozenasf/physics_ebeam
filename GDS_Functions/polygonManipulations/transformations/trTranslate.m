function m = trTranslate(dx, dy)

if nargin == 1
    if numel(dx) == 2
        dy = dx(2);
        dx = dx(1);
    end
end

m = [ 1 0 dx ; 0 1 dy; 0 0 1 ];
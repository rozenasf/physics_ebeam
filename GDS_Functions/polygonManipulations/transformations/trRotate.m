function m = trRotate(ang, cx, cy)

if nargin < 3
    cx = 0; 
    cy = 0;
end

switch mod(ang, 360.0)
    
    case 0.0
        
        c = 1.0;
        s = 0.0;
        
    case 90.0
        
        c = 0.0;
        s = 1.0;
        
    case 180.0
        
        c = -1.0;
        s = 0.0;
        
    case 270.0
        
        c = 0.0;
        s = -1.0;
        
    otherwise
        
        c = cos(ang/180*pi);
        s = sin(ang/180*pi);
end

m = trTranslate(cx, cy)*[ c -s 0 ; s c 0 ; 0 0 1 ]*trTranslate(-cx, -cy);

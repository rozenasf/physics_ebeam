%% Add path
addpath(genpath('C:\Users\asafr\Documents\physics_ebeam'));
%% Generate designs
%will generage a gds file from file "Standard.SLDMAT", configuration "Regular" extracting Contacts and gates on layers 2,11
%will also take care of writing field and crosses.
%See GDS2Solid function to generate intial solidworks from GDS

%% Old system
generic_cnt_AsafR('Simple_5g_Regular_Contact',1);
generic_cnt_AsafR('Simple_5g_Split_Contact',1);

%% load general template
%Template=gds_load('../../Templates/Full_Template.gds');
Solid2GDS('2LargeSide2Nav4Probe.SLDMAT','Large','Contacts',2,'Gates',11,'Optical',40,'Etch',41,'Ebeam','EbeamGates')
Solid2GDS('2LargeSide2Nav4Probe.SLDMAT','Large','Contacts',2,'Gates',11,'Optical',40,'Etch',41,'Ebeam','EbeamContacts')
%%
GenerateOpticalPath('Lior1.gds','Lior1.gds');
GenerateOpticalPath('Lior2.gds','Lior2.gds');
%%
BreakToEbeamTypeGDS('Lior1.gds');
BreakToEbeamTypeGDS('Lior2.gds');
%%
Template=gds_load([GetCodeFolderPWD(),'Templates/Full_Template.gds']);
%% ---------------------------------------------------------------------------------------------------%%
%% Job
DesignsNames={'2LargeSide2Nav4Probe','2LargeSide2Nav_S','Lior1','Lior2','IlanitV4','IlanitV1b2','Wedge'}; %these will be the designs in the final wafer 
ShortDesignNames={'2LS2N4P','2LS2N4S','L1','L2','I4','Ib2','W'};
RelativeDoses=[0.7,0.9,1,1.1,1.3];TestSpacingXY=[300,550];WriteOpticalLayer=0;
ProjectMark='060917';
%% Build full GDS for Contacts
    WriteFieldType='C';
    Designs=AddDesignsExtention(DesignsNames,WriteFieldType);
    [Design_GDS_Raw,Design_GDS]=LoadAllDesigns(Designs); %Load all designs
    DesignList=FindWritingLocationFromTemplate(Design_GDS_Raw,Template,RandomDevicePositions); %Will use the first design to find writing locations. will distribute as well
    BuildFullGDS(WriteFieldType,Designs,Design_GDS,DesignList,Template,WriteOpticalLayer,TestSpacingXY); %build full GDS

%% Build Ftexts
BuildV30Ftext('C',DesignsNames,ShortDesignNames);
BuildJDFFtext('C',DesignsNames,ShortDesignNames,ProjectMark,0,'B','b'); %Contacts Wafer
BuildJDFFtext('C',DesignsNames,ShortDesignNames,ProjectMark,1,'B','b'); %Contacts Test

%%
MergeFtext('C',DesignsNames);
%%

%% Add lines to jdf text
jdf_CHMPOS_Adder( ['TEST_C_',ProjectMark] )


%%
BuildSDFTest('C',ProjectMark,round(1700*[0.65,0.8,1,1.2,1.5]),200,5000);

%%
VisualizeSDF('TEST_C_060917.sdf','1','3D',Design_GDS,2)
%%
VisualizeSDF('WAFER_C_040917.sdf','1','2A',Design_GDS,2)
%%
% 
% BeamerPath = 'C:\Program Files\BEAMER\v5.1.4_x86\BEAMER.exe';
% CD=cd;
% a=tic;
% for i=1:numel(Designs);
%     system(['"',BeamerPath,'" "',CD,'\',Designs{i},'_Ebeam',Full_type,'.ftxt"']);
%     disp([num2str(i),'/',num2str(numel(Designs)),',Time:',num2str(toc(a)/i*(numel(Designs)-i)),':',Designs{i}])
% end
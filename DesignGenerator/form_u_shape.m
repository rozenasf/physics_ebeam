function u_shape = form_u_shape(x,y,layer)
u_shape = struct('points', [x(1) x(1) x(4) x(4) x(3) x(3) x(2) x(2) x(1); y(1) y(3) y(3) y(1) y(1) y(2) y(2) y(1) y(1)], 'dataType', 3, 'layer', layer);
end


function rect = form_trap(x,y,layer)
% forms a trapezoid with 2 x points and 4 y points
rect = struct('points', [x(1) x(1) x(2) x(2) x(1); y(1) y(2) y(3) y(4) y(1)], 'dataType', 3, 'layer', layer);
end


function [seg_out, center] = advanced_taper_segment(segment, taper_lines, ybeg, yend, yc1, yc2, widths, spacings, xwindow, center)
    xval_min = 100;
    xval_max = -100;
%    xwindow = 5;
    for l = taper_lines,
        [segment(l).points, xvals, ~] = break_segment(segment(l).points, ybeg);
        xval_min = min(min(xvals), xval_min);
        xval_max = max(max(xvals), xval_max);
        segment(l).points = break_segment(segment(l).points, yend);        
    end;

    if nargin==9
        center=(xval_min + xval_max) / 2;
        middle_origin = (xval_min + xval_max - sum(widths) - sum(spacings)) / 2;
    else

        middle_origin=center-(sum(widths) + sum(spacings))/2;
    end
    width_steps = kron([0 cumsum(widths)], [1,1]);
    space_steps = kron([0 cumsum(spacings)], [1,1]);

    xvalues = middle_origin + width_steps(2:end-1) + space_steps;
    xvalues = reshape(xvalues, 2, []);
    xvalues = xvalues(:,end:-1:1);
    for k = 1:length(taper_lines),
        l = taper_lines(k);
        segment(l).points = taper_segment_adv(segment(l).points, yc1, xvalues(:,k),center-xwindow,center+xwindow);
        segment(l).points = taper_segment_adv(segment(l).points, yc2, xvalues(:,k),center-xwindow,center+xwindow);
        %segment(l).points = heal_segment(segment(l).points, [yc1 yc2]);
    end;
    seg_out = segment;
end

function segp_out = heal_segment(segpoints, yrange)
    segp_out = [];
    epsilon = 0.01;
    for k = 1:size(segpoints,2),
        if ((segpoints(2, k) > (max(yrange) - epsilon)) + (segpoints(2, k) < (min(yrange) + epsilon))) > 0,
            segp_out = [segp_out, segpoints(:,k)];
        end
    end;
end

function [segp_out, xvals, indices] = break_segment(segpoints, y,x1,x2)
    segp_out = segpoints(:,1);
    xvals = [];
    indices = [];
    for k = 2:size(segpoints,2),
        if nargin==4
            if segpoints(1,k)>x1 && segpoints(1,k)<x2
                if point_in_line(y, [segpoints(2, k-1), segpoints(2, k)]),
                    x_val = segpoints(1, k) + (y - segpoints(2, k))*(segpoints(1, k) ...
                        - segpoints(1, k-1) ) / (segpoints(2, k) - segpoints(2, k-1));
                    segp_out = [segp_out, [x_val;y]];
                    xvals = [xvals, x_val];
                    indices = [indices, size(segp_out, 2)];
                end
            end
        else
            if point_in_line(y, [segpoints(2, k-1), segpoints(2, k)]),
                x_val = segpoints(1, k) + (y - segpoints(2, k))*(segpoints(1, k) ...
                    - segpoints(1, k-1) ) / (segpoints(2, k) - segpoints(2, k-1));
                segp_out = [segp_out, [x_val;y]];
                xvals = [xvals, x_val];
                indices = [indices, size(segp_out, 2)];
            end            
        end
        segp_out = [segp_out, segpoints(:, k)];
    end
    
    %if no point in line
    if isempty(xvals)
        if min(segpoints(2,:))>=y
            if nargin==4
                [xvals, indices]=min_segpoints(segpoints, x1, x2);
            else
                [xvals, indices]=min_segpoints(segpoints);
            end
        elseif max(segpoints(2,:))<=y
            if nargin==4
                [xvals, indices]=max_segpoints(segpoints, x1, x2);
            else
                [xvals, indices]=max_segpoints(segpoints);
            end
        end
    end        
end

function [xvals, indices]=min_segpoints(segpoints, x1, x2)
    xvals=[];
    indices=[];
    v=min(segpoints(2,:));
    if nargin==3
        for i=1:size(segpoints(2,:),2)
            if segpoints(1,i)>x1 && segpoints(1,i)<x2 && segpoints(2,i)==v
                xvals=[xvals segpoints(1,i)];
                indices=[indices i];
            end
        end
    else
       for i=1:size(segpoints(2,:),2)
            if segpoints(2,i)==v
                xvals=[xvals segpoints(1,i)];
                indices=[indices i];
            end
        end 
    end
end

function [xvals, indices]=max_segpoints(segpoints, x1, x2)
    xvals=[];
    indices=[];
    v=max(segpoints(2,:));
    if nargin==3
        for i=1:size(segpoints(2,:),2)
            if segpoints(1,i)>x1 && segpoints(1,i)<x2 && segpoints(2,i)==v
                xvals=[xvals segpoints(1,i)];
                indices=[indices i];
            end
        end
    else
        for i=1:size(segpoints(2,:),2)
            if segpoints(2,i)==v
                xvals=[xvals segpoints(1,i)];
                indices=[indices i];
            end
        end 
    end
end

function segp_out = taper_segment_adv(segpoints, y, xvals,x1,x2)
    if nargin==3
        [segp_out, xvals_seg, indices] = break_segment(segpoints, y);
    elseif nargin==5
        [segp_out, xvals_seg, indices] = break_segment(segpoints, y,x1,x2);
    end
    
    for i=1:length(xvals_seg)
        if xvals_seg(i)<max(xvals_seg)
            segp_out(1,indices(i))=min(xvals);
        else
            segp_out(1,indices(i))=max(xvals);
        end
    end
%     xvals = sort(xvals);
%     if xvals_seg(1) > xvals_seg(2),
%         indices = indices(end:-1:1);
%     end;
%     segp_out(1, indices(1)) = xvals(1);
%     segp_out(1, indices(2)) = xvals(2);
end


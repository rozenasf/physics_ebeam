function alphas = distributeOnEllipse(alpha1,alpha2,n,rx_by_ry)

N = 1000;
alph_cont = linspace(alpha1,alpha2,1000); % "continuous" alpha for calculating circumference

dl = sqrt(rx_by_ry^2*cos(alph_cont).^2 + sin(alph_cont).^2);
circ  = cumsum(dl);
circ = circ/circ(end); % normalize circumference

circ = mod(circ,1/(n-1))-1/2/(n-1);

is=find(circ(1:end-1)>0 & circ(2:end)<0); % find crossing points

alphas = [alpha1,alph_cont(is)];

end
function fan = form_w_fan(i,x,y,layer,buttonXY)


fan = struct('points', [[x(2);y(2)] [x(1);y(1)] [buttonXY(:,[2,1],i)] [x(2);y(2)]], 'dataType', 3, 'layer', layer);
end


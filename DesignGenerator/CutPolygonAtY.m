function [DoesCut,PolyOut1,PolyOut2]=CutPolygonAtY(PolyIn,YCut)
    DoesCut=0;PolyOut1=[];PolyOut2=[];
    geo=PolyIn;
    if(numel(find(abs(geo(2,:)-YCut)<1e-3))==0)
    CutLocations=find(diff(geo(2,:)>YCut)~=0);
    if(numel(CutLocations)~=2);return;end
    Y1=geo(2,CutLocations(1));Y2=geo(2,CutLocations(1)+1);
    Y3=geo(2,CutLocations(2));Y4=geo(2,CutLocations(2)+1);
    X1=geo(1,CutLocations(1));X2=geo(1,CutLocations(1)+1);
    X3=geo(1,CutLocations(2));X4=geo(1,CutLocations(2)+1);
    geo2=[ geo(:,1:CutLocations(1)) , [(YCut-Y1)./(Y2-Y1)*(X2-X1)+X1;YCut],geo(:,CutLocations(1)+1:CutLocations(2)),[(YCut-Y3)./(Y4-Y3)*(X4-X3)+X3;YCut],geo(:,CutLocations(2)+1:end)];
    else
       geo2= geo;
    end
    NewSlicedLocations=find(abs(geo2(2,:)-YCut)<1e-3);
    PolyOut1=[geo2(:,1:NewSlicedLocations(1)),geo2(:,NewSlicedLocations(2):end)];
    PolyOut2=[geo2(:,NewSlicedLocations(1):NewSlicedLocations(2)),geo2(:,NewSlicedLocations(1))];
    DoesCut=1;
end
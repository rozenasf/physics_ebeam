function l = layer(lstr)

switch(lstr)
    case('gates')
        l = 11;
    case('contacts')
        l = 2;
    case('etch')
        l = 7;
    case('thin_gates')
        l = 13;
    case('delta') % difference between stitching layers (e.g. gate layers are layer('gates')+[0:num-1]*layer('delta'))
        l = 1; % DON'T CHANGE. This affects layers of crosses, marks, and stitching patches
    case('label')
        l = 1; 
    case('title')
        l = 1; 
    case('optical')
        l = 39;
    case('border')
        l = 200;
    case('field')
        l = 201;
    case('mark')
        % For automatic jdf creation by Nissim, all designs carry a mark in
        % layer('mark').
        % For automatic jdf creation by BEAMER. e.g. design #3 will bare a mark in layer('mark')+3*layer('delta')
        l = 40; % design #1 marked in layer 50+delta, and so on.
    case('top') % same as mark, for top-edge instances (using upper neighbour's crosses)
        l = 60; 
    case('bottom') % same as mark, for bottom-edge instances (using lower neighbour's crosses)
        l = 80;
    case('left') % same as mark, for left-edge instances (using right neighbour's crosses)
        l = 160;
    case('right') % same as mark, for right-edge instances (using left neighbour's crosses)
        l = 180;
    case('crosses')
        l = 201; % first layer would be 201+delta, and so on.
    otherwise
        error(['Unknown layer (',lstr,')']);
end
        
end
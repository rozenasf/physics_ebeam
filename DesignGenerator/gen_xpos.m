function [xpos, widthOut, spacingOut] = gen_xpos(lineWidth,spacing,total,nlines)
% XPOS = GEN_XPOS(lineWidth,SPACING) generates a list of line x positions
% (size(XPOS) = [2,size(lineWidth)])of given lineWidth (1 per active line)
% and given SPACING (size(spacing) = size(lineWidth)-1).
%
% XPOS = GEN_XPOS(lineWidth,SPACING,TOTAL) allows automatic spacing using
% TOTAL cantilever lineWidth. Spacing is calculated automatically for
% individual lines with SPACING(i) = -1 or for all is SPACING = -1. TOTAL
% must be larger than SUM(lineWidth)+SUM(SPACING(SPACING > 0)).
%
% XPOS = GEN_XPOS(lineWidth,SPACING,TOTAL,NLINES) should be used when
% WIDTH, SPACING, and TOTAL are scalars. In that case NLINES is taken as
% the number of lines.
%
% [XPOS, WIDTH, SPACING] = GEN_XPOS(~) returns the calculated width and
% spacing.
%
% For example GEN_XPOS([10 5 5],[10 -1],100) leaves 70 microns
% between the 2nd and 3rd lines.Alternatively,
% GEN_XPOS([10 5 5],-1,100) will give equal distribution (40 micron
% spacing).

if((length(spacing) ~= length(lineWidth)-1) && (length(spacing) ~= 1 || spacing ~= -1))
    error('SPACING and lineWidth do not match in size. length(SPACING) should equal length(lineWidth)-1 unless SPACING = -1.');
end

if(nargin < 4)
    nlines = -1; % deduce number of lines from other arrays
end

if nlines < 0 % number of lines is automatic
    if length(lineWidth)==1
        error('Number of lines cannot be infered');
    else
        nlines = length(lineWidth);
    end
else
    if length(lineWidth)==1
        lineWidth=lineWidth*ones(1,nlines);
    else
        if nlines~=length(lineWidth)
            error('Number of lines different the # of elements in lineWidth');
        end
    end
end
        
% disp(int2str(nlines));
% disp(num2str(lineWidth,3));

if(all(spacing > 0))
    if(exist('total','var'))
        % total lineWidth is over-defined. If given, verify
        % consistency
        if(total ~= sum([lineWidth spacing]) && total > 0)
            error(sprintf('TOTAL lineWidth does not match given lineWidth and spacing %d ~= %d.',total,sum([lineWidth spacing])))
        else
            disp(sprintf('TOTAL width is %d microns',sum([lineWidth spacing])));
        end
    else
        disp(sprintf('TOTAL width is %d microns',sum([lineWidth spacing])));
    end
else
    % automatic spacing required
    if(~exist('total','var') || total < 0)
        error('TOTAL lineWidth must be given with SPACING = -1')
    end
    
    remaining = total - sum(lineWidth) - sum(spacing(spacing > 0));
    if (remaining < 0)
        error('TOTAL lineWidth is too small to allow for spacing.')
    end
    
    if (length(spacing) == 1)
        % all spacings are automatically generated        
        disp(sprintf('Automatic spacing is %d um',remaining/(nlines-1)));
        spacing = ones(1,nlines-1)*remaining/(nlines-1);
    else
        % some spacings are given. fill in where needed (where < 0).
        disp(sprintf('Automatic spacing is %d um',remaining/sum(spacing<0)));
        spacing(spacing<0) = remaining/sum(spacing<0);
    end
end

xpos = zeros(length(lineWidth),2);

cumspacing = [0 cumsum(spacing)];
cumwidth = [0 cumsum(lineWidth)];

for i = 1:length(lineWidth)
    xpos(i,1) = cumspacing(i)+cumwidth(i);
    xpos(i,2) = cumspacing(i)+cumwidth(i+1);
end

% center xpos at 0
xpos = xpos-xpos(end)/2;

if nargout > 1
    widthOut = lineWidth;
end

if nargout > 2
    spacingOut = spacing;
end

end

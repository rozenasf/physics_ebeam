function button = form_button(i, layer, buttonXY)
button = struct('points', [buttonXY(:,:,i) [buttonXY(1:2,1,i)]], 'dataType', 3, 'layer', layer);
end


function button = short_buttons(i1, i2, layer, buttonXY, side1, side2)

if strcmp(side1,'left')
    ind1 = [2,3];
else
    ind1 = [1,4];
end

if strcmp(side2,'left')
    ind2 = [3,2];
else
    ind2 = [4,1];
end

button = struct('points', [buttonXY(:,ind1,i1) buttonXY(:,ind2,i2) ...
    buttonXY(:,ind1(1),i1)], 'dataType', 3, 'layer', layer);

end


function b = point_in_line(point, line)
    line = sort(line);
    b = (point > line(1))*(point < line(2));
end
function polyOut = split_lines(polys,ylim,dlayer)
% shift all points above ylim to a new layer (layer += dlayer)
% split polygons if necessary

polyOut = [];
for ip = 1:numel(polys)
    poly = polys(ip);
    if all(poly.points(2,:)<ylim) % whole poly below limit - do nothing
        polyOut = [polyOut poly];
    elseif all(poly.points(2,:)>ylim) % while poly above limit - shift layer
        poly.layer = poly.layer+dlayer;
        polyOut = [polyOut poly];
    else % split polygon in two
        [topPoints, bottomPoints] = split_poly(poly.points,ylim,2); % split by y value (dim=2)
        topPoly = poly;
        topPoly.points = topPoints;
        topPoly.layer = poly.layer + dlayer;
        bottomPoly = poly;
        bottomPoly.points = bottomPoints;
        polyOut = [polyOut topPoly bottomPoly];
    end
end

    function [top,bot]=split_poly(points,lim,dim)
        top=[];
        bot=[];
        old = 0; % keeps whether previous point is above or below limit
        for i = 1:size(points,2)
           if (points(dim,i)-lim)*old < 0 % split required
               intr = intersect(points(:,i-1:i),lim,dim);
               top = [top intr];
               bot = [bot intr];
           end
           old = sign(points(dim,i)-lim);
           if old < 0 % add to bottom
               bot = [bot points(:,i)];
           else % add to top
               top = [top points(:,i)];
           end
        end
        % close the one open polygon
        if old > 0 % closing point missing in bottom
            bot = [bot bot(:,1)];
        else
            top = [top top(:,1)];
        end
        
    end

    function point = intersect(points,lim,dim)
        if (dim == 2) % transpose
            points = flipud(points);
        end
        point = [lim;(lim-points(1,1))/(points(1,2)-points(1,1))... % *
            *(points(2,2)-points(2,1)) + points(2,1)];
        if (dim == 2) % transpose
            point = flipud(point);
        end
end

end
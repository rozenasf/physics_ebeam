function button = form_wide_button(i, layer, buttonXY, stubSegX,...
    stubSegY, buttonExtraWidth, buttonExtraLength)

XY = [buttonXY(:,2:4,i), buttonXY(:,1,i)];
if buttonExtraLength % extend button along wide fan
    p5 = lineSegment(buttonXY(:,1,i),[stubSegX(1);stubSegY(1)], buttonExtraLength);
    p6 = lineSegment(buttonXY(:,2,i),[stubSegX(2);stubSegY(2)], buttonExtraLength);
    XY = [XY p5 p6];
end
XY = [XY, XY(:,1)]; % close polygon

%{
% simple, naive, non-general broadening
if buttonExtraLength
    iright = [1,2,6,7];
    ileft = [3,4,5];
else
    iright = [1,2,5];
    ileft = [3,4];
end
XY(1,ileft) = XY(1,ileft) - buttonExtraWidth;
XY(1,iright) = XY(1,iright) + buttonExtraWidth;
XY(2,[2,3]) = XY(2,[2,3]) + buttonExtraWidth;
%}
XY = inflatePoly(XY, buttonExtraWidth, -1);

button = struct('points', XY, 'dataType', 3, 'layer', layer);

    function xy = lineSegment(xy1,xy2,dl)
        l = sqrt((xy1(1)-xy2(1))^2+(xy1(2)-xy2(2))^2);
        xy(2,1) = xy1(2)+ dl/l*(xy2(2)-xy1(2));
        xy(1) = xy1(1) + dl/l*(xy2(1)-xy1(1));
    end

end
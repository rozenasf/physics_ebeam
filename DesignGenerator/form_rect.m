function rect = form_rect(x,y,layer)
rect = struct('points', [x(1) x(1) x(2) x(2) x(1); y(1) y(2) y(2) y(1) y(1)], 'dataType', 3, 'layer', layer);
end


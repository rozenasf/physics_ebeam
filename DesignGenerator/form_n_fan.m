function fan = form_n_fan(x1,y1,x2,y2,layer)
fan = struct('points', [x1(1) x2(1) x2(2) x1(2) x1(1); y1 y2(1) y2(2) y1 y1], 'dataType', 3, 'layer', layer);
end


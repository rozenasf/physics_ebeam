function polyOut = patch_lines(polys,y0,dy,dlayer)
% shift all points in range y=(y0-dy,y0+dy) to a new layer
% (layer += dlayer) split polygons if necessary

polyTop = split_lines(polys,y0-dy,layer('delta')); % move ploygons beyond y0-d0 to layer+delta
polyOut = split_lines(polyTop,y0+dy,-layer('delta')); % move polygons beyond y0+d0 back to original layer

end
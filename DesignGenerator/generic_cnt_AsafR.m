function generic_cnt_AsafR(filename,PerformWriteFieldCut)

%% read the cnt file
    fid = fopen([filename '.cnt'],'r');
    while 1
        tline = fgetl(fid);
        if ~ischar(tline), break, end
        eval(tline);
    end
%% PARAMETERS

maxlines = 33; % maximal number of lines (determined by optical layer)

%The draw options below should be 1 unless ...
drawCantileverSegment=1;
drawStubFanSegment=1;
drawStubSegment=1;
drawButtons=1;
    
gateLayer = layer('gates');
thingateLayer = layer('thin_gates');
contactLayer = layer('contacts');
etchLayer = layer('etch');
borderLayer = 101;
fieldLayer = layer('field');
LAYER_TRANSITION_BUTTONS = 3;

%% SET UNUSED TOGGLES OFF
if ~exist('drawEtch','var')
    drawEtch = 0;
end

if ~exist('Tapers', 'var')
    Tapers = 1;
end;

if ~exist('drawCurls','var')
    drawCurls = 0;
end
if ~exist('drawStoppers','var')
    drawStoppers = 0;
end
if ~exist('drawAdditionalStoppers','var')
    drawAdditionalStoppers = 0;
end

if ~exist('drawcurlyStoppers','var')
    drawcurlyStoppers = 0;
end

if ~exist('drawExtensions','var')
    drawExtensions = 0;
end
if ~exist('drawCurrentLoop','var')
    drawCurrentLoop = 0;
end

if ~exist('drawHeatingLoop','var')
    drawHeatingLoop = 0;
end

if ~exist('stopperWidthReduce','var')
    stopperWidthReduce = 0;
end

if ~exist('doTaper','var')
    doTaper = 0;
end

if ~exist('advancedTaper','var')
    advancedTaper = 0;
end

%% SET DEFAULT PARAMETERS (for backward compatability)
if ~exist('buttonExtraWidth','var')
    buttonExtraWidth = 0;
end

if ~exist('buttonExtraLength','var')
    buttonExtraLength = 0;
end

%% RELEASE RESERVED WORD 'CURL'
if exist('curl','var')
    curly = curl;
    clear('curl');
end

%% LINE TYPES

if maxlines > length(scheme)
    error('line number mismatch (scheme string is shorter than %d)',maxlines)
elseif maxlines < length(scheme)
    error('line number mismatch (scheme string is longer than %d)',maxlines)
end

contacts = find(scheme == 'c');
gates = find(scheme == 'g');
lines = sort([gates contacts]);
nlines = length(lines); % number of used lines

%translates the physical index (appearing in shceme) to the line index
%(used in width and length arrays)
lineind = zeros(maxlines);
lineind(lines) = 1:length(lines);


if drawCurls
    leftcurl = find(curly == 'l');
    rightcurl = find(curly == 'r');
    gate2Offset = find(curly == 'o');
    if ~exist('curl_g2c_Scheme','var')
        g2c = [];
    else
        g2c = find(curl_g2c_Scheme == 'x'); %Contact 2 gate extension (Avishai)
    end
    
     if ~exist('curl_c2g_Scheme','var')
        c2g = [];
    else
        c2g = find(curl_c2g_Scheme == 'x'); %Contact 2 gate extension (Avishai)
    end
end

if drawStoppers
    stoppers =  find(stopperScheme == 's');
    longStoppers = find(stopperScheme == 'S');
end

if drawExtensions
    if ~exist('extensionScheme','var')
        error('extensionScheme missing');
    end
    extensions = find(extensionScheme == 'c'); %extensions for contact, should add for gate
end


%% BUTTON COORDINATES  (matched to optical layer, chreated with a similar piece of code)

% % % THE PARAMETERS BELOW MUST MATCH THOSE USED FOR OPTICAL LAYER % % %
 % %
  %
buttonMaxY = 552;
buttonMinY = 502;
buttonMaxX = 120;
buttonWidth = 3+1; % +1 to increase overlap
maxRx = (buttonMaxX^2+(buttonMaxY-buttonMinY)^2)/2/(buttonMaxY-buttonMinY);
buttonRx = 130; % should not exceed circular limit maxRx=(MaxX^2+MaxY-MinY)^2)/2/(MaxY-MinY)
  %
 % %
% % % THE PARAMETERS ABOVE MUST MATCH THOSE USED FOR OPTICAL LAYER % % %

% solve for Ry
buttonDY = buttonMaxY-buttonMinY;
R=roots([buttonMaxX^2/buttonRx^2,-2*buttonDY,buttonDY^2]);
buttonRy=max(R);
buttonCtrY = buttonMaxY-buttonRy;
buttonCtrX = 0;
buttonAngle = atan(buttonMaxX/(buttonMinY-buttonCtrY));

% distribute buttons "evenly" on ellipse
alphas = distributeOnEllipse(pi/2-buttonAngle,pi/2+buttonAngle,maxlines,buttonRx/buttonRy);
for i = 1:maxlines
    alph_i = alphas(i);
    r_i = buttonRx*buttonRy/sqrt(cos(alph_i)^2*buttonRy^2+sin(alph_i)^2*buttonRx^2);
    buttonDelAngle = buttonWidth/r_i/2;
    dalph = buttonDelAngle*[1 -1 -1 1];
    dr = [-1 -1 +1 +1]*buttonWidth;
    for j = 1:4 % draw button around (alph_i, r_i)
        alph_ij = alph_i + dalph(j);
        r_ij = dr(j) + buttonRx*buttonRy/sqrt(cos(alph_ij)^2*buttonRy^2+sin(alph_ij)^2*buttonRx^2);
        buttonXY(:,j,i) = r_ij*[cos(alph_ij);sin(alph_ij)] + [buttonCtrX;buttonCtrY];
    end
end

%% CALCULATE LINE MASK

% % stub segment

% calculate stub parameters (distribute lines evenly)
stubY = 400;
cantiY = -50; % not 0 due to an offset in the mask... :(
stubSegWidth = min(1,stubWidth/(3*length(find(scheme=='c'|scheme=='g'))-2)); % 50% duty cycle, but avoiding too wide (>1um) lines
stubSegSpacing = -1; % -1 Automatic
stubSegTotal = stubWidth;
stubSegLength = stubY-cantiY-stubFanLength; %

% disp('Calculating stub segment')
if (length(stubSegWidth) ~= nlines && length(stubSegWidth) ~= 1)
    error ('STUBSEGWIDTH array does not match number of used lines in SCHEME.')
end

if length(stubSegSpacing)>=length(stubSegWidth)
    stubSegSpacing = stubSegSpacing(1:max(1,length(stubSegWidth)-1));
end

if (length(stubSegWidth) == 1) % width is scalar (uniform)
    stubSegWidth = ones(1,nlines)*stubSegWidth; % convert to array
end

if (length(stubSegSpacing) == 1) % width is scalar (uniform)
    stubSegSpacing = ones(1,nlines-1)*stubSegSpacing; % convert to array
end

[stubSegX,stubSegWidth,stubSegSpacing] = gen_xpos(stubSegWidth,stubSegSpacing,stubSegTotal);
stubSegX = -stubSegX;

if (length(stubSegLength) ~= 1) % length is scalar (uniform)
    error('non-uniform length in stub segment is not allowed')
end

% fanY is top end of stub where fan must end. below this point lines must be
% straight (actual stub ends at stubY).
fanY = buttonCtrY;

% spread y at interface with fan to avoid shorts/breaks
for i = 1:nlines
    dy(2*i-1) = stubSegWidth(i)*tan(atan((buttonXY(1,4,lines(i))-mean(stubSegX(i,:)))/(buttonXY(2,4,lines(i))-buttonCtrY))/2);
    if (i~=length(lines))
        dy(2*i) = stubSegSpacing(i)*tan(atan((buttonXY(1,3,lines(i+1))+buttonXY(1,4,lines(i))-stubSegX(i+1,1)-stubSegX(i,2))/(buttonXY(2,4,lines(i))-buttonCtrY))/2);
    end
end

y0 = max(-sum(dy),0);
y = [0 cumsum(dy)]+y0+fanY;

for i = 1:nlines
   stubSegY(i,:) = [y(2*i-1) stubY-stubSegLength*[1 1] y(2*i)];
end

% % cantilever segment

if length(lineWidth) ~= nlines
    error ('LINE_WIDTH array does not match number of used lines in SCHEME.')
end
% disp('Calculating lines')
if length(lineSpacing)>=length(lineWidth)
    lineSpacing = lineSpacing(1:length(lineWidth)-1);
end
lineX = -gen_xpos(lineWidth,lineSpacing,lineTotal);
wideX = lineX; % DEADBEEF
if (length(lineLength) == 1) 
    lineLength = ones(1,nlines)*lineLength;
end

% spread y at interface with fan to avoid shorts/breaks
for i = 1:nlines
    dy(2*i-1) = lineWidth(i)*(mean(stubSegX(i,:))-mean(lineX(i,:)))/(stubSegY(i,2)-cantiY)/2;
    if (i~=length(lines))
        dy(2*i) = lineSpacing(i)*(mean(stubSegX(i,:))-mean(lineX(i,:)))/(stubSegY(i,2)-cantiY)/2;
    end
end

y0 = max(-sum(dy),0);
y = [0 cumsum(dy)]+y0+cantiY;

for i = 1:nlines
   lineY(i,:) = [y(2*i-1) (-lineLength(i)+cantiY)*[1 1] y(2*i)];
end

wideY = lineY; % DEADBEEF

if (min(lineY(:,2)) < -cantiLength+cantiY)
    error('Some lines are longer than the cantilever!')
else
    disp(sprintf('Unused y portion of catilevesr is %.2f microns',cantiLength-cantiY+min(lineY(:,2))));
end

%ADDED - ILANIT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(drawCurls)
    
    % check list
    if  (~isempty(leftcurl))
        if (topMinLength+max(lineLength(lineind(leftcurl)))>cantiLength)
           disp(('Some curly lines are longer than the cantilever!'))
        end
        if (~isempty(find( scheme(leftcurl)=='-', 1 )))
            error('Left curly line is defined for a line that is not defined')
        end
        if (~isempty(rightcurl))
            if ( max(leftcurl) > min(rightcurl) )
                error('All left curles must be to the left of all right curles')
            end  
        end        
    end
    if (~isempty(rightcurl))
        if (topMinLength+max(lineLength(lineind(rightcurl)))>cantiLength)  %%%%% FIX!!! FOR THE CASE THE INDEX OF WIDELENGTH IS NOT THE SAME AS SCHEME!
            disp(('Some curly lines are longer than the cantilever!'))
        end
        if (~isempty(find( scheme(rightcurl)=='-', 1 )))
            error('Right curly line is defined for a line that is not defined')
        end
    end
    if (isempty(gate2Offset))
        error('User must specify reference straight line that determines positions of curly lines!')
    end
    if (length(gate2Offset)>1)
        error('User must specify only one reference straight line that determines positions of curly lines!')
    end    
    if (~isempty(find( scheme(gate2Offset)=='-', 1 )))
        error('Offset reference straight line is defined for a line that is not defined')
    end
    
    disp('Calculating curly lines')
    
    if isempty(topWidth) %%Added by Avishai in case of chosen topWidth different then lineWidth
        if isempty(rightcurl)
            topWidth=fliplr(lineWidth(lineind(leftcurl)));
        else
            if (isempty(leftcurl))
                topWidth=fliplr(lineWidth(lineind(rightcurl)));
            else
                % The flipping below is done because the most inner left
                % curly line becomes the most outer line on the top, and
                % similarly for the right curly lines
                topWidth=[fliplr(lineWidth(lineind(leftcurl))), fliplr(lineWidth(lineind(rightcurl))) ];
            end
        end
        disp(['Add errors for wrong topWidth (Avishai)']);
    end
    
    topX = -gen_xpos(topWidth,topSpacing,sum([topWidth topSpacing]));
    topX = topX - topX(1,1) +lineX(lineind(gate2Offset),1) - curlLineOffset ;
    
    if (~isempty(rightcurl) )
        if ( topX(end,2) < lineX(min(lineind(rightcurl)),1) )
            error('Curly lines ill defined - top lines should all be in between left and right curly lines in the bottom')
        end
    end

    n_left=length(leftcurl);
    n_right=length(rightcurl);
    
    n_g2c=length(g2c);
    i_g2c=1;
    g2c_Y = zeros(n_g2c,2);
    g2c_X = zeros(n_g2c,2);

    n_c2g=length(g2c);
    i_c2g=1;
    c2g_Y = zeros(n_g2c,2);
    c2g_X = zeros(n_g2c,2);

    
    if n_left %non-zero
        leftuY = zeros(n_left,3);
        % The flipping is done because the most inner left curly line
        % becomes the most outer line on the top, and similarly for the
        % right curly lines
        leftuY(:,1) = fliplr(-lineLength(lineind(leftcurl)) + cantiY)';
        leftuX = zeros(n_left,4);
        
        for i=1:n_left
            
            ind_g2c=find(g2c==leftcurl(n_left+1-i));
            ind_c2g=find(c2g==leftcurl(n_left+1-i));
            
            
            %find points between the inner parts of the u-shape
            [a_midlines, b_midlines] = find( topX(i,1) <= lineX  &  lineX  <  lineX(lineind(leftcurl(n_left+1-i)),2) ); 
            midlines = [a_midlines,b_midlines];
            un_midlines = unique(midlines(:,1));
            % find highest gate in the inner part of the u-shape (lineY is negative) 
            y_min = min(min(lineY(un_midlines,:),[],2));

            if i==1
                if (~isempty(y_min))
                    leftuY(i,2) = min( y_min - topMinDist , leftuY(i,1) - topMinLength);
                else
                    leftuY(i,2) = leftuY(i,1) - topMinLength;
                end
            else
                if (~isempty(y_min))
                    %leftuY(i,2) = min( [ y_min - topMinDist , leftuY(i,1) - topMinLength ,  leftuY(i-1,3) - topMinDist ] );
                    leftuY(i,2) = min( [ y_min - topMinDist ,  leftuY(i-1,3) - topMinDist ] );
                else
                    %leftuY(i,2) = min( [ leftuY(i,1) - topMinLength ,  leftuY(i-1,3) - topMinDist ] );
                    leftuY(i,2) = min( [leftuY(i-1,3) - topMinDist ] );
                end
            end
            leftuY(i,3) = leftuY(i,2) - lineWidth(lineind(leftcurl(n_left+1-i)));

            if ind_g2c
                g2cY(i_g2c,:) = -[max(lineLength) + g2c_Overlap, g2c_Length(lineind(g2c(ind_g2c)))]  + cantiY ;
                %leftuY(i,1) = - g2c_Length(lineind(g2c(ind_g2c))) + g2c_Overlap + cantiY;
                
                g2cX(i_g2c,:) = topX(i,:);
                i_g2c=i_g2c+1;
            end
            
            if ind_c2g
                c2gY(i_c2g,:) = -[max(lineLength) + c2g_Overlap, c2g_Length(lineind(c2g(ind_c2g)))]  + cantiY ;
                %leftuY(i,1) = - g2c_Length(lineind(g2c(ind_g2c))) + g2c_Overlap + cantiY;
                
                c2gX(i_c2g,:) = topX(i,:);
                i_c2g=i_c2g+1;
            end
            
            
            max_line = un_midlines(end);
            if ( lineX(max_line,2) <= topX(i,1) )
                if (  leftuY(i,1) >= min(lineY(max_line,:),[],2) )
                    %error(['Collision between curly line no. ', ...
                    %    num2str(lineind(leftcurl(n_left+1-i))) ,' and line no. ', num2str(max_line)])
                end
            end

            j = 1;
            while (j <= length(lineLength) - max_line)
                if ( lineX(max_line+j,1) >= topX(i,2) )
                    if (  leftuY(i,1) >= min(lineY(max_line+j,:),[],2) )
                        error(['Collision between curly line no. ', ...
                            num2str(lineind(leftcurl(n_left+1-i))) ,' and line no. ', num2str(max_line+1)])
                    end
                    j = j+1;
                else
                    break;
                end
            end

            leftuX(i,1:2) = lineX(lineind(leftcurl(n_left+1-i)),:);
            leftuX(i,3:4) =  topX(i,:);
            
        end

        if abs(min(min(leftuY))) >= (cantiLength - cantiY)
            error('Some curly lines are longer than the cantilever!')
        end
        
    end
    
    if n_right % non-zero
        rightuY = zeros(n_right,3);
        rightuY(:,1) = (-lineLength(lineind(rightcurl)) + cantiY)'; %DEADBEEF
        rightuX = zeros(n_right,4);        
         
        for i=1:n_right
            %find points between the inner parts of the u-shape
            [a_midlines, b_midlines] = find( topX(end+1-i, 2) > wideX  &  wideX  >=  wideX(lineind(rightcurl(i)),1) );
            midlines = [a_midlines,b_midlines];
            un_midlines = unique(midlines(:,1));
            % find highest gate in the inner part of the u-shape (wideY is negative) 
            y_min = min(min(wideY(un_midlines,:),[],2)); 

            if i==1
                if (~isempty(y_min))
                    rightuY(i,2) = min( y_min - topMinDist , rightuY(i,1) - topMinLength);
                else
                    rightuY(i,2) = rightuY(i,1) - topMinLength;
                end
            else
                if (~isempty(y_min))
                    %rightuY(i,2) = min( [ y_min - topMinDist , rightuY(i,1) - topMinLength ,  rightuY(i-1,3) - topMinDist ] );
                    rightuY(i,2) = min( [ y_min - topMinDist , rightuY(i-1,3) - topMinDist ] );
                else
                    %rightuY(i,2) = min( [ rightuY(i,1) - topMinLength ,  rightuY(i-1,3) - topMinDist ] );
                    rightuY(i,2) = min( [ rightuY(i-1,3) - topMinDist ] );
                end                
            end

            rightuY(i,3) = rightuY(i,2) - lineWidth(lineind(rightcurl(i)));
            max_line = un_midlines(1);

            if ( wideX(max_line,1) >= topX(end+1-i,2) )
                if (  rightuY(i,1) >= min(wideY(max_line,:),[],2) )
                    error(['Collision between curly line no. ', ...
                        num2str(lineind(rightcurl(i))) ,' and line no. ', num2str(max_line)]);
                end
            end

            j = 1;
            while (j <= max_line - 1)
                if ( wideX(max_line-j,2) <= topX(end+1-i,1) )
                    if (  rightuY(i,1) >= min(wideY(max_line-j,:),[],2) )
                        error(['Collision between curly line no. ', ...
                            num2str(lineind(rightcurl(i))) ,' and line no. ', num2str(max_line+1)]);
                    end
                    j = j+1;
                else
                    break;
                end
            end

            rightuX(i,1:2) = topX(end+1-i,:);
            rightuX(i,3:4) = lineX(lineind(rightcurl(i)),:);  

                    %for gate 2 contact (Avishai)
%             i
%             rightcurl(end+1-i)
            ind_g2c=find(g2c==rightcurl(i));
            if ind_g2c
                g2cY(i_g2c,:) = -[max(lineLength) + g2c_Overlap, g2c_Length(ind_g2c)] + cantiY;
                g2cX(i_g2c,:) = topX(end+1-i,:);
                i_g2c=i_g2c+1;
            end
            
            ind_c2g=find(c2g==rightcurl(i));
            if ind_c2g
                %c2gY(i_c2g,:) = -[max(lineLength) + c2g_Overlap, c2g_Length(ind_c2g)] + cantiY;
                
                c2gY(i_c2g,:) = -[max(lineLength) + c2g_Overlap, c2g_Length((lineind(c2g(ind_c2g))))] + cantiY;
                c2gX(i_c2g,:) = topX(end+1-i,:);
                i_c2g=i_c2g+1;
            end
        end

        if ( abs(min(min(rightuY))) >= cantiLength - cantiY) % DEADBEEF
            error('Some curly lines are longer than the cantilever!')
        end

    end    

end

if (drawStoppers)
    
    % check list
    if (isempty(stoppers))
        error('No stoppers were specified.')
    end
    if ( length(stopperStart) ~= length(stoppers) )
        error('No. of stopper beginning positions does not match no. of stoppers.')
    end
    if ( length(stopperEnd ) ~= length(stoppers) )
        error('No. of stopper end positions does not match no. of stoppers.')
    end
    if ( length(stopperStart) ~= length(stopperEnd) )
        error('No. of stopper beginning positions does not match no. of stopper end possitions.')
    end
    if (~isempty(find( scheme(stoppers)=='-', 1 )))
            error('Stopper is defined for a line that is not defined')
    end
    stopperLayer = contactLayer;
    if (~isempty(find( scheme(stoppers)=='c', 1 )))
        if (~isempty(find( scheme(stoppers)=='g', 1 )))
            error('Stopper be only on contacts or only on gates')
        end;
        stopperLayer = gateLayer;
    end
    if( ~isempty( find( (stopperEnd - stopperStart >= stopperMinLength)==0, 1 ) ))
        error('Stopper length is smaller than the minimum stopper length')
    end
    if (max(stopperEnd) > cantiLength - cantiY)
        error('stopper is longer than the cantilever!')
    end
    
    if(~isempty(longStoppers))
        stopperBegin = zeros(size(longStoppers));
        for i=1:length(longStoppers)
            stopperBeginInd = [];
            if i==1
                stopperBeginInd = find(stoppers < longStoppers(i));
            else
                stopperBeginInd = find( (stopperBegin(i-1) < stoppers)  & (stoppers < longStoppers(i)));
            end
            
            if (isempty(stopperBeginInd))
                error('A wide stopper is defined with an incorrect beginning')
            end
            
            stopperBegin(i) = max(stoppers(stopperBeginInd));            
            
            % ADD SOMETHING THAT CHECKS THAT THE LINES IN THE MIDDLE ARE
            % GATES AND NOT CURLY
            %{
            if (~isempty(find( scheme(longStoppers)=='-', 1 )))
                 error('Stopper is defined for a line that is not defined')
            end
            %}
            
        end           
            if (~isempty(find( scheme(longStoppers)=='-', 1 )))
                error('Stopper is defined for a line that is not defined')
            end
            if (~isempty(find( scheme(longStoppers)=='c', 1 )))
                    error('Stopper can only be defined on a gate')
            end
    end
    
    if (length(lineLength) > 1 ) % non-uniform line length
        if(  ~isempty( find( (lineLength(lineind(stoppers)) <  (stopperEnd + cantiY) )==1, 1 ) ) )            
            error('stopper is longer than the gate.')
        end
    else % uniform line length
            if (max(stopperEnd) > lineLength )
                error('stopper is longer than the gate.')
            end
    end
    stopperX = zeros(length(stoppers),2);
    stopperY = zeros(length(stoppers),2);
    
    j=1;
    for i=1:length(stoppers)
        if  (isempty(longStoppers))
            stopperX(i,:) = lineX(lineind(stoppers(i)),1:2) + stopperWidthReduce*[-1 1];
        else
            stopperBeginInd = [];
            stopperBeginInd = find(stopperBegin==stoppers(i));
            if (isempty(stopperBeginInd))
                stopperX(i,:) = lineX(lineind(stoppers(i)),1:2);
            else
                stopperX(i,:) = [lineX(lineind(stopperBegin(j)),1), lineX(lineind(longStoppers(j)),2)];
                j=j+1;
            end
        end
        stopperY(i,:) = -[stopperEnd(i) + cantiOffset, stopperStart(i) + cantiOffset];
    end

    
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (drawAdditionalStoppers),
    
    % check list
    if (isempty(additionalStoppers))
        error('No additionalStoppers were specified.')
    end
    if ( length(additionalStopperStart) ~= length(additionalStoppers) )
        error('No. of additionalStopper beginning positions does not match no. of additionalStoppers.')
    end
    if ( length(additionalStopperEnd ) ~= length(additionalStoppers) )
        error('No. of additionalStopper end positions does not match no. of additionalStoppers.')
    end
    if ( length(additionalStopperStart) ~= length(additionalStopperEnd) )
        error('No. of additionalStopper beginning positions does not match no. of additionalStopper end possitions.')
    end
    if (~isempty(find( scheme(additionalStoppers)=='-', 1 )))
            error('additionalStopper is defined for a line that is not defined')
    end
    if (~isempty(find( scheme(additionalStoppers)=='c', 1 )))
            error('additionalStopper can only be defined on a gate')
    end
    if( ~isempty( find( (additionalStopperEnd - additionalStopperStart >= stopperMinLength)==0, 1 ) ))
        error('additionalStopper length is smaller than the minimum additionalStopper length')
    end
    if( max(additionalStopperEnd) > cantiLength - cantiY)
        error('additionalStopper is longer than the cantilever!')
    end

    % DEADBEEF
    %if (length(lineLength) > 1 ) % non-uniform line length
    %    if(  ~isempty( find( (lineLength(lineind(additionalStoppers)) <  (additionalStopperEnd + cantiY) )==1, 1 ) ) )            
    %        error('additionalStopper is longer than the gate.')
    %    end
    %else % uniform line length
    %        if (max(additionalStopperEnd) > lineLength )
    %            error('additionalStopper is longer than the gate.')
    %        end
    %end
    additionalStopperX = zeros(length(additionalStoppers),2);
    additionalStopperY = zeros(length(additionalStoppers),2);
    
    j=1;
    for i=1:length(additionalStoppers)
        additionalStopperX(i,:) = lineX(lineind(additionalStoppers(i)),1:2)  + stopperWidthReduce*[-1 1];
        additionalStopperY(i,:) = -[additionalStopperEnd(i), additionalStopperStart(i)] + cantiY;
    end

    
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



if (drawcurlyStoppers),
    
    % check list
    if (isempty(curlyStoppers))
        error('No curlyStoppers were specified.')
    end
    if ( length(curlyStopperStart) ~= length(curlyStoppers) )
        error('No. of curlyStopper beginning positions does not match no. of curlyStoppers.')
    end
    if ( length(curlyStopperEnd ) ~= length(curlyStoppers) )
        error('No. of curlyStopper end positions does not match no. of curlyStoppers.')
    end
    if ( length(curlyStopperStart) ~= length(curlyStopperEnd) )
        error('No. of curlyStopper beginning positions does not match no. of curlyStopper end possitions.')
    end

    curlyStopperX = zeros(length(curlyStoppers),2);
    curlyStopperY = zeros(length(curlyStoppers),2);
    
    j=1;
    for i=1:length(curlyStoppers)
        curlyStopperX(i,:) = topX(curlyStoppers(i),1:2)  + stopperWidthReduce*[-1 1] ;
        curlyStopperY(i,:) = -[curlyStopperEnd(i), curlyStopperStart(i)] + cantiY;
    end

    
    
end


if (exist('drawExtensions','var') && drawExtensions)
    
    if (isempty(extensions))
        error('No contact extensions were specified.')
    end
  
    extensionX = zeros(length(extensions),2);
    extensionY = zeros(length(extensions),2);

    for i=1:length(extensions)
        extensionX(i,:) = lineX(lineind(extensions(i)),1:2)+[extensionwidthL(i) extensionwidthR(i)];
        extensionY(i,:) = -[lineLength(lineind(extensions(i))), lineLength(lineind(extensions(i)))+extensionLength(i)];
    end  
end


%% CALCULATE ETCH MASK (Y is flipped as last stage)

if (drawEtch) %if advanced_taper_only exist only for that segment
    if (~exist('extraEtchBeyond','var'))
        warning('etch parameters missing. using defaults');
        extraEtchBeyond = 10;
        extraEtchBefore = 30;
        etchWidth = 30;
        etchSpacing = 1.3;
    end
    
    if exist('advanced_taper_descriptor','var')
        totalWidth=sum(advanced_taper_widths)+sum(advanced_taper_spacings);
        totalLength=max(lineLength);
    else
        totalWidth=sum(lineWidth)+sum(lineSpacing);
        totalLength=max(lineLength);
    end
    
    etchX = [etchWidth/2,                 etchWidth/2,                 totalWidth/2+etchSpacing,    totalWidth/2+etchSpacing];
    etchY = [totalLength+extraEtchBeyond, totalLength-extraEtchBefore, totalLength-extraEtchBefore, totalLength+etchSpacing];
    
    etchX = [etchX -etchX(end:-1:1),etchX(1)];
    etchY = -[etchY, etchY(end:-1:1) etchY(1)]+cantiY;

end % drawEtch

%% CREATE CURRENT-LOOP
if drawCurrentLoop
    warning('No fault detection is implemented in current-loop feature. Verify manually.')
    currentLoop.x = [lineX(1,1) lineX(end,2)];
    currentLoop.y = lineY(end,2)+[-1 1]*diff(lineX(1,:))/2;
end
%% CREATE CURRENT-LOOP
if drawHeatingLoop
    warning('No fault detection is implemented in current-loop feature. Verify manually.')

end
%% CREATE BORDER (multiple of field size, containing entire writing area, but no longer centered at zero)

fieldSize = 500; % formal maximal size of field
fieldEdge = 10; % unused portion of field (um)
distFromFan = 10; % distance of stitch points from fans
borderX = [-0.5 0.5]*fieldSize;
% round y extent to fieldSize halves

%{ 
% used to be centered around origin (0,0)
DY = ceil(2*max(buttonMaxY/fieldSize,(cantiLength-cantiY)/fieldSize))*fieldSize/2;
borderY = [-DY DY];
%}

DY = ceil((2*fieldEdge+buttonMaxY+cantiLength-cantiY)/fieldSize)*fieldSize;
borderY = [DY 0] - cantiLength + cantiY - fieldEdge;

border = form_rect(borderX,borderY,borderLayer);

stitchY = linspace(borderY(1),borderY(2),DY/fieldSize+1); % calculate field edges
stitchY = stitchY(2:end-1); % stitching is required only between two fields, not at edges

% Shift the origin to the center of the writing field:
Shift_x = -mean(borderX);
Shift_y = -mean(borderY);

%% CREATE FIELD BOXES

% old - for manual fields (not working with multipass)
%{
fieldX = [-0.5 0.5]*fieldSize;
topStubStitchY = stubY-distFromFan; % mandatory stitch between big fan and stub segment
bottomStubStitchY = stubFanLength+distFromFan+cantiY; % mandatory stitch between narrow fan and stub segment
fieldY1 = [buttonMaxY+buttonWidth topStubStitchY];
fieldY2 = [topStubStitchY bottomStubStitchY];
field = [form_rect(fieldX,fieldY1,fieldLayer) form_rect(fieldX,fieldY2,fieldLayer)];

% next field(s) are for cantilever
maxY = bottomStubStitchY;
minY = -cantiLength+cantiY;
dY = maxY-minY;
if (dY < fieldSize-2*fieldEdge)
    fieldY = [maxY minY];
    field = [field form_rect(fieldX,fieldY,fieldLayer)];
    stitchY = nan; % no additional stitch
elseif dY > 2*(fieldSize-2*fieldEdge)
    error('Cantilever line length require more than one stitch beyond mandatory stiches (multi-stitching not implemented)');
else
    stitchY = min(60,minY+fieldSize-2*fieldEdge);
    fieldY = [maxY stitchY];
    field = [field form_rect(fieldX,fieldY,fieldLayer)];
    fieldY = [stitchY minY];
    field = [field form_rect(fieldX,fieldY,fieldLayer)];
end
%}

field = []; % fields are generated automatically within the extracted border


%% WRITE MASK

gdsFile = gds_create_library(filename);

gds_start_structure(gdsFile, 'Die');

cantileverSegment = []; % init narrow straight segment
stubFanSegment = []; % init narrow fan segment
stubSegment = []; % init wide straight segment
wfanSegment = []; % init wide fan segment
buttons = []; % init buttons
curlyLineSegment = []; % init curly lines u-shape segment
stopperSegment = []; % init stepper
g2cSegment = []; %curly gate 2 contact (Avishai)
c2gSegment = []; 

extensionSegment = [];

for i = 1:nlines
    if (find(gates == lines(i)))
        lineLayer = gateLayer;
    else
        lineLayer = contactLayer;
    end
    
    buttons = [buttons form_button(lines(i), contactLayer, buttonXY)];
    
    cantileverSegment = [cantileverSegment form_trap(lineX(i,:),lineY(i,:),lineLayer)];
    stubFanSegment =  [stubFanSegment form_n_fan(stubSegX(i,:),stubSegY(i,2),lineX(i,:),lineY(i,[1,4]),lineLayer)];
    stubSegment =  [stubSegment form_trap(stubSegX(i,:),stubSegY(i,:),contactLayer)];
    if (lineLayer == gateLayer),
        % add buttons for layer transition
        fan_segpoints = form_n_fan(stubSegX(i,:),stubSegY(i,2),lineX(i,:),lineY(i,[1,4]),contactLayer);
        fan_segpoints.points = cut_segment(fan_segpoints.points, stubSegY(i,2) - LAYER_TRANSITION_BUTTONS);
        stubSegment =  [stubSegment fan_segpoints];    
    end;
    wfanSegment = [wfanSegment form_w_fan(lines(i),stubSegX(i,:),stubSegY(i,[1,4]), contactLayer, buttonXY)];
    
    if (drawCurls)        
        if (n_left)
            ind_l = find(leftcurl == lines(i));
            if (~isempty(ind_l) )
                curlyLineSegment = [curlyLineSegment  form_u_shape(leftuX(n_left+1-ind_l,:), leftuY(n_left+1-ind_l,:), lineLayer)] ;
            end
        end
        if (n_right)
            ind_r = find(rightcurl == lines(i));
            if (~isempty(ind_r) )
                curlyLineSegment = [curlyLineSegment  form_u_shape(rightuX(ind_r,:), rightuY(ind_r,:), lineLayer)] ;
            end
        end
        
        % ADDED - AVISHAI for curly gate 2 contact %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if ~isempty(g2c)
            ind_g2c =  find(g2c == lines(i));
            if ~isempty(ind_g2c)
                ind_l=find(leftcurl == lines(i));
                indl=leftcurl(ind_l);
                ind_r=find(rightcurl == lines(i));
                indr=rightcurl(ind_r);
                
                if ~isempty(indl)
                    if g2c(ind_g2c)==indl
                        g2cSegment = [g2cSegment    form_rect(g2cX(ind_g2c,:),g2cY(ind_g2c,:),contactLayer) ];
%                         disp(['Writing left g2c @ ' int2str(indl) ', ind_g2c=' int2str(ind_g2c)]);
                    end
                end
                if ~isempty(indr)
                    if g2c(ind_g2c)==indr
%                         disp(['Writing right g2c @ ' int2str(indr) ', ind_g2c=' int2str(ind_g2c)]);
                        g2cSegment = [g2cSegment    form_rect(g2cX(ind_g2c,:),g2cY(ind_g2c,:),contactLayer) ];
                    end
                end
            end
        end
        
        if ~isempty(c2g)
            ind_c2g =  find(c2g == lines(i));
            if ~isempty(ind_c2g)
                ind_l=find(leftcurl == lines(i));
                indl=leftcurl(ind_l);
                ind_r=find(rightcurl == lines(i));
                indr=rightcurl(ind_r);
                
                if ~isempty(indl)
                    if c2g(ind_c2g)==indl
                        c2gSegment = [c2gSegment    form_rect(c2gX(ind_c2g,:),c2gY(ind_c2g,:),gateLayer) ];
%                         disp(['Writing left g2c @ ' int2str(indl) ', ind_g2c=' int2str(ind_g2c)]);
                    end
                end
                if ~isempty(indr)
                    if c2g(ind_c2g)==indr
%                         disp(['Writing right g2c @ ' int2str(indr) ', ind_g2c=' int2str(ind_g2c)]);
                        c2gSegment = [c2gSegment    form_rect(c2gX(ind_c2g,:),c2gY(ind_c2g,:),gateLayer) ];
                    end
                end
            end
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end    

    if (drawStoppers )
        ind_s =  find(stoppers == lines(i));
        if (~isempty(ind_s) )
            stopperSegment = [stopperSegment    form_rect(stopperX(ind_s,:),stopperY(ind_s,:),stopperLayer) ];
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if (drawAdditionalStoppers )
        warning('No fault detection is implemented in additional-stopper feature. Verify manually.')
        ind_s =  find(additionalStoppers == lines(i));
        if (~isempty(ind_s) )
            stopperSegment = [stopperSegment    form_rect(additionalStopperX(ind_s,:),additionalStopperY(ind_s,:),contactLayer) ];
        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
end

if (drawcurlyStoppers )
        warning('No fault detection is implemented in curly stopper feature. Verify manually.')
        for i = 1:length(curlyStoppers),

            stopperSegment = [stopperSegment    form_rect(curlyStopperX(i,:),curlyStopperY(i,:),contactLayer) ];
        end;
end


if drawCurrentLoop
    if find(gates == lines(i)) % current loop is on gate layer
        loopLayer = gateLayer;
    else % current loop is on contact layer
        loopLayer = contactLayer;
    end
    cantileverSegment = [cantileverSegment form_rect(currentLoop.x,currentLoop.y,loopLayer)];
end
if drawHeatingLoop
    loopLayer = contactLayer;
    for HLL=1:size(heatingLoop.x,2)
        cantileverSegment = [cantileverSegment form_rect(heatingLoop.x{HLL},heatingLoop.y{HLL},loopLayer)];
    end
    
end
% short-bunching lines
if (isempty(find(scheme == '<', 1)) && isempty(find(scheme == '>', 1))) % for backward compatability
    % short-bunch unused lines
    for i=2:length(scheme)-2 % avoid automatically shorting boundary lines, used also as C pads
        if(scheme(i)=='-' && scheme(i+1)=='-')
            if (i+1==length(scheme)-1 || scheme(i+2)~='-') % last in unused sequence
                buttons = [buttons short_buttons(i,i+1, contactLayer, buttonXY,'left','right')];
            else
                buttons = [buttons short_buttons(i,i+1, contactLayer, buttonXY,'left','left')];
            end
        end
    end
else % short lines according to '<' '>' marks in scheme
    escheme = ['-' scheme '-']; % extended schemes for easier handling of first and last lines
    for i=1:length(scheme)
        switch scheme(i)
            case '-'
                if escheme(i)=='>' || escheme(i+2)=='<'
                    buttons = [buttons form_button(i,contactLayer,buttonXY)];
                end
            case '<'
                if (i==1 && scheme(2)=='<') || escheme(i)=='>' % first or following a short-right mark, no short required
                    buttons = [buttons form_button(i,contactLayer,buttonXY)];
                elseif i>1 % short left
                    buttons = [buttons short_buttons(i,i-1, contactLayer, buttonXY,'right','right')];
                end
            case '>'
                if i==length(scheme) % last, no short required
                    if scheme(i-1)=='>'
                        buttons = [buttons form_button(i,contactLayer,buttonXY)];
                    end
                else % short right
                    buttons = [buttons short_buttons(i,i+1, contactLayer, buttonXY,'left','left')];
                end
        end
    end
end


% ADDED - AVISHAI %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (drawExtensions )
    ind_ext =  find(extensions == lines(i));
    if (~isempty(ind_ext) )
        extensionSegment = [extensionSegment form_rect(extensionX(ind_ext,:),extensionY(ind_ext,:),contactLayer) ];
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% SPLIT LINES AT STICHING POINT AND ADD A PATCH ON ANOTHER LAYER+DELTA (IF NECESSARY)

patchSize = 0.400; % size of patch in Y
if(PerformWriteFieldCut)
if ~isnan(stitchY)
    for i = 1:numel(stitchY)
        stubSegment = patch_lines(stubSegment,stitchY(i),patchSize/2,layer('delta'));
        wfanSegment = patch_lines(wfanSegment,stitchY(i),patchSize/2,layer('delta'));
        buttons = patch_lines(buttons,stitchY(i),patchSize/2,layer('delta'));
        cantileverSegment = patch_lines(cantileverSegment,stitchY(i),patchSize/2,layer('delta'));
        stubFanSegment = patch_lines(stubFanSegment,stitchY(i),patchSize/2,layer('delta'));
        curlyLineSegment = patch_lines(curlyLineSegment,stitchY(i),patchSize/2,layer('delta'));
        stopperSegment = patch_lines(stopperSegment,stitchY(i),patchSize/2,layer('delta'));
        extensionSegment = patch_lines(extensionSegment,stitchY(i),patchSize/2,layer('delta'));
    end
end
end
if (doTaper),
    epsilon = 4e-3;
    taperdesc = [lineX(taperLines(2),2) - epsilon, lineX(taperLines(1),1) + epsilon,...
        -taperY(1) + cantiY, -taperY(2) + cantiY, taperRatio];
    
    cantileverSegment = taper_segment(cantileverSegment, taperdesc);
    curlyLineSegment  = taper_segment(curlyLineSegment, taperdesc);
    g2cSegment        = taper_segment(g2cSegment, taperdesc);
    
end;



if advancedTaper,
    [cantileverSegment center] = advanced_taper_segment(cantileverSegment, lines_to_taper...
        , -advanced_taper_descriptor(1) - cantiOffset, -advanced_taper_descriptor(2) - cantiOffset,...
        -advanced_taper_descriptor(3) - cantiOffset, -advanced_taper_descriptor(4) - cantiOffset...
        , advanced_taper_widths, advanced_taper_spacings, advancedTaperWindow);
    if (Tapers >= 2)
        curlyLineSegment = advanced_taper_segment(curlyLineSegment, lines_to_taper_2...
        , -advanced_taper_descriptor(1) - cantiOffset, -advanced_taper_descriptor(2) - cantiOffset,...
        -advanced_taper_descriptor(3) - cantiOffset, -advanced_taper_descriptor(4) - cantiOffset...
        , advanced_taper_widths_2, advanced_taper_spacings_2, advancedTaperWindow_2, center);
    end
    
    if (Tapers == 3),
        cantileverSegment = advanced_taper_segment(cantileverSegment, lines_to_taper_3...
        , -advanced_taper_descriptor_3(1) - cantiOffset, -advanced_taper_descriptor_3(2) - cantiOffset,...
        -advanced_taper_descriptor_3(3) - cantiOffset, -advanced_taper_descriptor_3(4) - cantiOffset...
        , advanced_taper_widths_3, advanced_taper_spacings_3, advancedTaperWindow_3, center);
    end;
end


if exist('jumper_descriptor'),
    jumper_descriptor(3) = - jumper_descriptor(3) - cantiOffset;
    jumper_descriptor(4) = - jumper_descriptor(4) - cantiOffset;
    cantileverSegment = make_jumper(jumper_descriptor, cantileverSegment);
    %gds_write_boundaries(gdsFile, ShiftGDS(cantileverSegment,Shift_x,Shift_y));    
end


if drawCantileverSegment
    disp('Drawing cantilever segment')
    gds_write_boundaries(gdsFile, ShiftGDS(cantileverSegment,Shift_x,Shift_y));
%     gds_write_boundaries(gdsFile, cantileverSegment);
end

if drawStubFanSegment
    disp('Drawing stub fan segment')
    gds_write_boundaries(gdsFile, ShiftGDS(stubFanSegment,Shift_x,Shift_y));
%     gds_write_boundaries(gdsFile, stubFanSegment);
end

if drawStubSegment
    disp('Drawing stub segment')
    gds_write_boundaries(gdsFile, ShiftGDS(stubSegment,Shift_x,Shift_y));
%     gds_write_boundaries(gdsFile, stubSegment);
end

if drawButtons
    disp('Drawing buttons')
    gds_write_boundaries(gdsFile, ShiftGDS(wfanSegment,Shift_x,Shift_y));
    gds_write_boundaries(gdsFile, ShiftGDS(buttons,Shift_x,Shift_y));
%     gds_write_boundaries(gdsFile, wfanSegment);
%     gds_write_boundaries(gdsFile, buttons);
end

etch = [];
if (drawEtch)
    disp('Drawing etch')
    etch = struct('points',[etchX; etchY],'dataType',etchLayer,'layer',etchLayer);
    gds_write_boundaries(gdsFile, ShiftGDS(etch,Shift_x,Shift_y));
%     gds_write_boundaries(gdsFile, etch);
end

if (drawCurls)
    disp('Drawing curly lines')
    gds_write_boundaries(gdsFile, ShiftGDS(curlyLineSegment,Shift_x,Shift_y));
%     gds_write_boundaries(gdsFile, curlyLineSegment);  
    if ~isempty(g2c)
        gds_write_boundaries(gdsFile, ShiftGDS(g2cSegment,Shift_x,Shift_y));
%         gds_write_boundaries(gdsFile, g2cSegment);
    end

    if ~isempty(c2g)
        gds_write_boundaries(gdsFile, ShiftGDS(c2gSegment,Shift_x,Shift_y));
%         gds_write_boundaries(gdsFile, c2gSegment);
    end

    
end
if (drawStoppers)
    disp('Drawing stoppers')
    gds_write_boundaries(gdsFile, ShiftGDS(stopperSegment,Shift_x,Shift_y));    
%     gds_write_boundaries(gdsFile, stopperSegment);    
end

if (drawExtensions)
    disp('Drawing extensions')
    gds_write_boundaries(gdsFile, ShiftGDS(extensionSegment,Shift_x,Shift_y));    
%     gds_write_boundaries(gdsFile, extensionSegment); 
end


%%
Colors='rgbmkyc';
names={'cantileverSegment','stubFanSegment','stubSegment','wfanSegment','buttons','etch','border'};
for k=1:numel(names);
    plot(1,1,Colors(k))
    hold on
end
  for j=1:numel(names)  
      eval(['a={',names{j},'.points}']);

for i=1:numel(a);
   plot(a{i}(1,:),a{i}(2,:),Colors(j)) 
   %hold on
end
  end
hold off
legend(names)
%%
disp('Drawing auxiliary borders and fields')
gds_write_boundaries(gdsFile, ShiftGDS(border,Shift_x,Shift_y));
gds_write_boundaries(gdsFile, ShiftGDS(field,Shift_x,Shift_y));
% gds_write_boundaries(gdsFile, border);
% gds_write_boundaries(gdsFile, field);
ForGDS=[];
Markers=[...
   6.5004300e+02   1.4030600e+03  -6.5004300e+02  -1.1769400e+03;...
   7.7004300e+02   1.5230600e+03  -7.7004300e+02  -1.1769400e+03;...
   8.9004300e+02   1.6430600e+03  -8.9004300e+02  -1.1769400e+03;...
   1.0100430e+03   1.7630600e+03  -1.0100430e+03  -1.1769400e+03;...
   1.1300430e+03   1.8830600e+03  -1.1300430e+03  -1.1769400e+03;...
   1.2500430e+03   2.0030600e+03  -1.2500430e+03  -1.1769400e+03];

for i=1:size(Markers,1)
    ForGDS(end+1).points=[Markers(i,1),Markers(i,3),Markers(i,3),Markers(i,1),Markers(i,1);Markers(i,2),Markers(i,2),Markers(i,4),Markers(i,4),Markers(i,2)];
    ForGDS(end).dataType=0;
    ForGDS(end).layer=91+size(Markers,1)-i;  %write crosses boxes from 91 -> + starting from the most outer one
end
gds_write_boundaries(gdsFile, ShiftGDS(ForGDS,Shift_x,Shift_y));
% gds_write_boundaries(gdsFile, ForGDS);
gds_close_structure(gdsFile);

gds_close_library(gdsFile);
end

function rect = form_single_meander(x,y,line,spacing,width,layer)
l=line; s=spacing; w=width;
rel_co=[0 0 w-l w-l 0 0 w w l l 0; 0 -s-l -s-l -2*s-l -2*s-l -2*s-2*l -2*s-2*l -s -s 0 0];
co=[x*ones(1,size(rel_co,2)); y*ones(1,size(rel_co,2))]+rel_co;
rect = struct('points', co, 'dataType', 3, 'layer', layer);
end


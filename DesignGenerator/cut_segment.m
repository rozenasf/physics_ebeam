function seg_out = cut_segment(segpoints, yval)
    if segpoints(2,1) > yval,
        seg_out = [segpoints(:,1)];
        need_to_close = 0;
    else
        seg_out = [];
        need_to_close = 1;
    end;
    
    for k = 2:size(segpoints,2),
        if point_in_line(yval, [segpoints(2, k-1), segpoints(2, k)]),
            x_val = segpoints(1, k) + (yval - segpoints(2, k))*(segpoints(1, k) ...
                        - segpoints(1, k-1) ) / (segpoints(2, k) - segpoints(2, k-1));
            seg_out = [seg_out, [x_val;yval]];
        end;
        if segpoints(2, k) > yval,
            seg_out = [seg_out, segpoints(:,k)];
        end;
    end;
    if (need_to_close),
        seg_out = [seg_out, seg_out(:,1)];
    end;
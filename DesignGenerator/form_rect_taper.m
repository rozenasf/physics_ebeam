function res = form_rect_taper(x,y,layer, taper_desc)

taper_x = sort(taper_desc(1:2));
taper_y = sort(taper_desc(3:4));
taper_ratio = taper_desc(5);
x = sort(x);
y = sort(y);

if (point_in_line(x(1), taper_x) && point_in_line(x(2), taper_x)),
    switch ( point_in_line(y(1), taper_y)*2 + point_in_line(y(2), taper_y) )
        case 0
            % outside taper
            res = form_rect(x, y, layer);
        case 1
            % crossing from below
            part1 = form_rect(x, [y(1), taper_y(1)], layer);
            part2 = taper_rect(x, [taper_y(1), y(2)], taper_x, taper_y, taper_ratio);
            res = [part1 part2];
        case 2
            part1 = form_rect(x, [taper_y(2), y(2)], layer);
            part2 = taper_rect(x, [y(1), taper_y(2)], taper_x, taper_y, taper_ratio);
            res = [part1 part2];
            % crossing from above
        case 3
            % inside taper
    end
else
    res = form_rect(x, y, layer);
end

end

function b = point_in_line(point, line)
    b = (point >= line(1))*(point <= line(2));
end

function polygon = taper_rect(x,y, layer, taper_x, taper_y, taper_ratio)
    % assumes dealing only with coordinates inside taper
    x1 = t_c(x(1), y(1), taper_x, taper_y, taper_ratio);
    x2 = t_c(x(1), y(1), taper_x, taper_y, taper_ratio);
    x3 = t_c(x(1), y(1), taper_x, taper_y, taper_ratio);
    x4 = t_c(x(1), y(1), taper_x, taper_y, taper_ratio);

    polygon = struct('points', [ x1   x2   x3   x4   x1;...
                               y(1) y(2) y(2) y(1) y(1)]...
                      ,'dataType', 3, 'layer', layer);
end

function x_new = t_c(x,y,taper_x, taper_y, taper_ratio)
    r = 1 - taper_ratio * 2 *abs(y - mean(taper_y))/(taper_y(2) - taper_y(1));
    x_new = mean(taper_x) + r*(x - mean(taper_x));
end